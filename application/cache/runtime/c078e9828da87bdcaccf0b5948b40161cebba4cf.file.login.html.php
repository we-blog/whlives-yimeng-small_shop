<?php /* Smarty version Smarty-3.1.21-dev, created on 2019-08-13 23:02:49
         compiled from "D:\www\whlives-yimeng-master\views\manager\login.html" */ ?>
<?php /*%%SmartyHeaderCode:21505d52d119a4e2e1-13248431%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'c078e9828da87bdcaccf0b5948b40161cebba4cf' => 
    array (
      0 => 'D:\\www\\whlives-yimeng-master\\views\\manager\\login.html',
      1 => 1533788760,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '21505d52d119a4e2e1-13248431',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'redirect_url' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5d52d119abb900_16598047',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5d52d119abb900_16598047')) {function content_5d52d119abb900_16598047($_smarty_tpl) {?><!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
    <meta http-equiv="Cache-Control" content="no-siteapp" />
    <title><?php echo config_item('manager_title');?>
</title>
    <link href="/public/H-ui/css/H-ui.min.css" rel="stylesheet" type="text/css" />
    <link href="/public/H-ui/css/H-ui.admin.css" rel="stylesheet" type="text/css" />
    <link href="/public/H-ui/css/H-ui.login.css" rel="stylesheet" type="text/css" />
    <link href="/public/H-ui/hui-iconfont/iconfont.css" rel="stylesheet" type="text/css">
</head>
<body>
<div class="header"></div>
<div class="loginWraper">
    <div id="loginform" class="loginBox">
        <form class="form form-horizontal" name="login" id="login" method="post" action="<?php echo site_url('/manager/login/login_act');?>
">
            <div class="row cl">
                <label class="form-label col-3"><i class="Hui-iconfont">&#xe60d;</i></label>
                <div class="formControls col-8">
                    <input id="" name="username" type="text" placeholder="账户" class="input-text size-L" datatype="*" nullmsg="请输入用户名！">
                </div>
            </div>
            <div class="row cl">
                <label class="form-label col-3"><i class="Hui-iconfont">&#xe60e;</i></label>
                <div class="formControls col-8">
                    <input id="" name="password" type="password" placeholder="密码" class="input-text size-L" datatype="*" nullmsg="请输入密码！">
                </div>
            </div>
            <div class="row">
                <div class="formControls col-8 col-offset-3">
                    <input name="" type="submit" class="btn btn-success radius size-L" value="&nbsp;登&nbsp;&nbsp;&nbsp;&nbsp;录&nbsp;">
                    <input name="" type="reset" class="btn btn-default radius size-L" value="&nbsp;取&nbsp;&nbsp;&nbsp;&nbsp;消&nbsp;">
                </div>
            </div>
        </form>
    </div>
</div>
<div class="footer"><?php echo get_copyright();?>
</div>
<?php echo '<script'; ?>
 type="text/javascript" src="/public/js/jquery.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="/public/js/validform.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="/public/js/layer/layer.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" >
    //验证表单
    $(function(){
        $.Tipmsg.r=null;
        $("#login").Validform({
            tiptype:function(msg){
                layer.msg(msg);
            },
            tipSweep:true,
            ajaxPost:true,
            callback:function(data){
                if(data.status=="y"){
                    window.location.href="<?php echo $_smarty_tpl->tpl_vars['redirect_url']->value;?>
";
                }
            }
        });
    })
<?php echo '</script'; ?>
>
</body>
</html><?php }} ?>
