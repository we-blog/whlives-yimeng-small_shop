<?php /* Smarty version Smarty-3.1.21-dev, created on 2019-08-13 23:02:33
         compiled from "D:\www\whlives-yimeng-master\views\web\footer.html" */ ?>
<?php /*%%SmartyHeaderCode:50855d52d109cc75c0-02411964%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'a772be5d5718ac45f411c48f1424198e81606b2f' => 
    array (
      0 => 'D:\\www\\whlives-yimeng-master\\views\\web\\footer.html',
      1 => 1533788760,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '50855d52d109cc75c0-02411964',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'cat_list' => 0,
    'key' => 0,
    'a_list' => 0,
    'k' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5d52d109dadd85_53877719',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5d52d109dadd85_53877719')) {function content_5d52d109dadd85_53877719($_smarty_tpl) {?><!-- foot -->
<div class="footwrap">
	<div class="footerwrap">
		<div class="wrapbox clear">
			<?php $_smarty_tpl->tpl_vars['cat_list'] = new Smarty_variable(ym_list('article_cat',array(),4,1,'sortnum asc,id asc'), null, 0);?>
			<?php  $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['key']->_loop = false;
 $_smarty_tpl->tpl_vars['val'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['cat_list']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['key']->key => $_smarty_tpl->tpl_vars['key']->value) {
$_smarty_tpl->tpl_vars['key']->_loop = true;
 $_smarty_tpl->tpl_vars['val']->value = $_smarty_tpl->tpl_vars['key']->key;
?>
			<dl class="footlistbox">
				<dt><a href="javascript:void(0)"><span class="footunderline"><?php echo $_smarty_tpl->tpl_vars['key']->value['name'];?>
</span></a></dt>
				<?php $_smarty_tpl->tpl_vars['a_list'] = new Smarty_variable(ym_list('article',array('where'=>array('cat_id'=>$_smarty_tpl->tpl_vars['key']->value['id'])),4,1,'id asc'), null, 0);?>
				<?php  $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['k']->_loop = false;
 $_smarty_tpl->tpl_vars['v'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['a_list']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['k']->key => $_smarty_tpl->tpl_vars['k']->value) {
$_smarty_tpl->tpl_vars['k']->_loop = true;
 $_smarty_tpl->tpl_vars['v']->value = $_smarty_tpl->tpl_vars['k']->key;
?>
				<dd><a href="<?php echo site_url("/article/view?id=".((string)$_smarty_tpl->tpl_vars['k']->value['id']));?>
"><?php echo $_smarty_tpl->tpl_vars['k']->value['title'];?>
</a></dd>
				<?php } ?>
			</dl>
			<?php } ?>
			<dl class="footlistbox">
				<dt><a href="javascript:void(0)"><span class="footunderline">联系我们</span></a></dt>
				<dd>
					<div class="footLeft">
						<img src="/views/web/skin/images/icon04.png">
					</div>
					<div class="footRight">
						<p>联系电话：<span style="font-size:18px;">400-8888888</span></p>
						(7*24小时客服电话)
					</div>
				</dd>
				<dd>
					<div class="footLeft">
						<img src="/views/web/skin/images/icon05.png">
					</div>
					<div class="footRight">
						<p>在线客服</p>
						(服务时段：8:00 - 24:00)
					</div>
				</dd>
			</dl>
		</div>
	</div>
	<div class="footbox">
		<div class="wrapbox">
			<div class="footboxleft">
				<?php echo get_copyright();?>

			</div>
			<div class="footboxright">
				<div class="footboximg">
					<img src="/views/web/skin/images/erweima.png">
				</div>
				手机版
			</div>
		</div>
	</div>
</div>
<?php echo '<script'; ?>
 language="JavaScript">
	$(function(){
		//购物车基本信息展示
		$.ajax({
			type:"POST",
			url: "/api/cart/cart_count",
			data: "",
			dataType:"json",
			success: function(data){
				if (data.status=='y') {
					if (data.result.sku_count>0) {
						$('#my_cart_count').text(data.result.sku_count);
						$('#my_cart_count').show();
					} else {
						$('#my_cart_count').hide();
					}
				}
			}
		});
	})
<?php echo '</script'; ?>
><?php }} ?>
