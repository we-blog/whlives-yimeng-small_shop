<?php /* Smarty version Smarty-3.1.21-dev, created on 2019-08-13 23:10:18
         compiled from "D:\www\whlives-yimeng-master\views\manager\market\promotion\add.html" */ ?>
<?php /*%%SmartyHeaderCode:167095d52d2dab52ab8-72746160%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '06f85a49642f9fe425635f3393ea395549541d27' => 
    array (
      0 => 'D:\\www\\whlives-yimeng-master\\views\\manager\\market\\promotion\\add.html',
      1 => 1533788760,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '167095d52d2dab52ab8-72746160',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'item' => 0,
    'group_list' => 0,
    'key' => 0,
    'coupons_list' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5d52d2dac641f7_16362185',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5d52d2dac641f7_16362185')) {function content_5d52d2dac641f7_16362185($_smarty_tpl) {?><!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<meta name="renderer" content="webkit|ie-comp|ie-stand">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
<meta http-equiv="Cache-Control" content="no-siteapp" />
    <title><?php echo config_item('manager_title');?>
</title>
    <link href="/public/H-ui/css/H-ui.min.css" rel="stylesheet" type="text/css" />
    <link href="/public/H-ui/css/H-ui.admin.css" rel="stylesheet" type="text/css" />
    <link href="/public/H-ui/hui-iconfont/iconfont.css" rel="stylesheet" type="text/css">
</head>
<body>
<div class="pd-20">
	<form action="<?php echo site_url('/manager/market/promotion/save');?>
" method="post" class="form form-horizontal" id="add">
		<div class="row cl">
			<label class="form-label col-3"><span class="c-red">*</span>活动名称：</label>
			<div class="formControls col-7">
				<input type="text" class="input-text" value="" name="name" datatype="*" nullmsg="请输入活动名称！">
			</div>
		</div>
        <div class="row cl">
            <label class="form-label col-3"><span class="c-red">*</span>开始日期：</label>
            <div class="formControls col-7">
                <input type="text" class="input-text" value="" name="start_time" onclick="laydate({istime: true, format: 'YYYY-MM-DD hh:mm:ss'})" readonly datatype="*" nullmsg="请选择开始日期！">
            </div>
        </div>
        <div class="row cl">
            <label class="form-label col-3"><span class="c-red">*</span>结束日期：</label>
            <div class="formControls col-7">
                <input type="text" class="input-text" value="" name="end_time" onclick="laydate({istime: true, format: 'YYYY-MM-DD hh:mm:ss'})" readonly datatype="*" nullmsg="请选择结束日期！">
            </div>
        </div>
        <div class="row cl">
            <label class="form-label col-3"><span class="c-red">*</span>参与会员组：</label>
            <div class="formControls col-7">
                <input type="checkbox" id="checkbox-0" name="user_group[]" value="0" <?php if (in_array(0,$_smarty_tpl->tpl_vars['item']->value['user_group'])) {?>checked<?php }?>>
                <label for="checkbox-0">全部</label>
                <?php  $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['key']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['group_list']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['key']->key => $_smarty_tpl->tpl_vars['key']->value) {
$_smarty_tpl->tpl_vars['key']->_loop = true;
?>
                <input type="checkbox" id="checkbox-<?php echo $_smarty_tpl->tpl_vars['key']->value['id'];?>
" name="user_group[]" value="<?php echo $_smarty_tpl->tpl_vars['key']->value['id'];?>
" <?php if (in_array($_smarty_tpl->tpl_vars['key']->value['id'],$_smarty_tpl->tpl_vars['item']->value['user_group'])) {?>checked<?php }?>>
                <label for="checkbox-<?php echo $_smarty_tpl->tpl_vars['key']->value['id'];?>
"><?php echo $_smarty_tpl->tpl_vars['key']->value['group_name'];?>
</label>
                <?php } ?>
            </div>
        </div>
        <div class="row cl">
            <label class="form-label col-3"><span class="c-red">*</span>购物金额：</label>
            <div class="formControls col-7">
                <input type="text" class="input-text" value="" name="use_price" datatype="price" nullmsg="请输入订单起用金额！" errormsg="订单起用金额格式错误">
            </div>
        </div>
        <div class="row cl">
            <label class="form-label col-3"><span class="c-red">*</span>活动规则：</label>
            <div class="formControls col-7">
                <select name="type" datatype="*" nullmsg="请选择规则！">
                    <option value="">请选择规则</option>
                    <option value="1">购物金额满{X}减</option>
                    <option value="2">购物金额满{X}优惠</option>
                    <option value="3">购物金额满{X}赠积分</option>
                    <option value="4">购物金额满{X}免运费</option>
                    <option value="5">购物金额满{X}赠优惠券</option>
                </select>
                <span id="type_value_box"></span>

                <!--活动规则-->
                <?php echo '<script'; ?>
 type='text/html' id='type_valueTemplate'>
                    {{if type>=1 && type<=3}}
                    <input type="text" class="input-text" value="" name="type_value" datatype="n" nullmsg="请输入赠送值！" errormsg="请赠送值只能是数字" style="width: 60px;">
                    {{if type==1}}元{{else if type==2}}折1-100%{{else if type==3}}个{{/if}}
                    {{/if}}
                    {{if type==5}}
                    <select name="type_value" style="width: 100px;" datatype="*" nullmsg="请选择优惠券！">
                        <option value="">请选择优惠券</option>
                        <?php  $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['key']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['coupons_list']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['key']->key => $_smarty_tpl->tpl_vars['key']->value) {
$_smarty_tpl->tpl_vars['key']->_loop = true;
?>
                        <option value="<?php echo $_smarty_tpl->tpl_vars['key']->value['id'];?>
" <?php if ($_smarty_tpl->tpl_vars['key']->value['id']==$_smarty_tpl->tpl_vars['item']->value['type_value']) {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['key']->value['name'];?>
</option>
                        <?php } ?>
                    </select>一张
                    {{/if}}
                <?php echo '</script'; ?>
>
            </div>
        </div>
        <div class="row cl">
            <label class="form-label col-3"><span class="c-red">*</span>简介：</label>
            <div class="formControls col-7">
                <textarea name="desc" cols="" rows="" class="textarea"  placeholder="活动简介" datatype="*1-100" dragonfly="true" nullmsg="活动简介不能为空！" onKeyUp="textarealength(this,100)"></textarea>
                <p class="textarea-numberbar"><em class="textarea-length">0</em>/100</p>
            </div>
        </div>
		<div class="row cl">
			<div class="col-10 col-offset-2">
                <input type="hidden" name="id" value="">
				<button onClick="$('#add').submit();" class="btn btn-primary radius" type="submit"><i class="Hui-iconfont">&#xe632;</i> 保存</button>
				<button onClick="layer_close();" class="btn btn-default radius" type="button">&nbsp;&nbsp;取消&nbsp;&nbsp;</button>
			</div>
		</div>
	</form>
</div>
</div>
<?php echo '<script'; ?>
 type="text/javascript" src="/public/js/jquery.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="/public/js/validform.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="/public/js/layer/layer.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="/public/js/form.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="/public/js/artTemplate.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="/public/js/laydate/laydate.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="/public/H-ui/js/H-ui.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="/public/H-ui/js/H-ui.admin.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript">
    $(function(){
        //验证表单
        $.Tipmsg.r=null;
        $("#add").Validform({
            tiptype:function(msg){
                layer.msg(msg);
            },
            tipSweep:true,
            ajaxPost:true,
            callback:function(data){
                if(data.status=="y"){
                    layer_close();
                }
            }
        });

        //全部选择用户组
        $('[id="checkbox-0"]').click(function(){
            if ($(this).prop('checked')==true) {
                $('[name="user_group[]"]').prop('checked', true);
            } else {
                $('[name="user_group[]"]').prop('checked', false);
            }
        })
    })

    //展示规则对应值
    function set_type_value(type){
        var html = template('type_valueTemplate', {'type':type});
        $('#type_value_box').html(html);
    }

    $(function(){
        //选择规则
        $('[name="type"]').change(function(){
            set_type_value($(this).val());
        })

        <?php if ($_smarty_tpl->tpl_vars['item']->value!='') {?>
            //展示规则对应值
            set_type_value(<?php echo $_smarty_tpl->tpl_vars['item']->value['type'];?>
);
        <?php }?>
        //表单回填
        var formObj = new Form();
        formObj.init(<?php echo ch_json_encode($_smarty_tpl->tpl_vars['item']->value);?>
);
    })
<?php echo '</script'; ?>
>
</body>
</html><?php }} ?>
