<?php /* Smarty version Smarty-3.1.21-dev, created on 2019-08-13 23:10:24
         compiled from "D:\www\whlives-yimeng-master\views\manager\market\coupons\list.html" */ ?>
<?php /*%%SmartyHeaderCode:102215d52d2e0053711-40939363%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '6709585b382318cf44f1b47290aaf0440a186c1a' => 
    array (
      0 => 'D:\\www\\whlives-yimeng-master\\views\\manager\\market\\coupons\\list.html',
      1 => 1533788760,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '102215d52d2e0053711-40939363',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'list' => 0,
    'key' => 0,
    'page_count' => 0,
    'search_where' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5d52d2e0184268_48995041',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5d52d2e0184268_48995041')) {function content_5d52d2e0184268_48995041($_smarty_tpl) {?><!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<meta name="renderer" content="webkit|ie-comp|ie-stand">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
<meta http-equiv="Cache-Control" content="no-siteapp" />
	<title><?php echo config_item('manager_title');?>
</title>
	<link href="/public/H-ui/css/H-ui.min.css" rel="stylesheet" type="text/css" />
	<link href="/public/H-ui/css/H-ui.admin.css" rel="stylesheet" type="text/css" />
	<link href="/public/H-ui/hui-iconfont/iconfont.css" rel="stylesheet" type="text/css">
</head>
<body>
<nav class="breadcrumb">
	<i class="Hui-iconfont">&#xe67f;</i> 首页 <span class="c-gray en">&gt;</span> 促销管理 <span class="c-gray en">&gt;</span> 活动列表 <a class="btn btn-success radius r mr-20" style="line-height:1.6em;margin-top:3px" href="javascript:location.replace(location.href);" title="刷新" ><i class="Hui-iconfont">&#xe68f;</i></a>
</nav>
<div class="pd-20" style="padding-top: 0px;">
	<div class="cl pd-5 bg-1 bk-gray mt-20"> <span class="l">
		<a href="javascript:;" class="btn btn-primary radius" onclick="open_iframe('添加活动','<?php echo site_url('/manager/market/coupons/edit');?>
')"><i class="Hui-iconfont">&#xe600;</i> 添加活动</a>
		<a href="javascript:;" onclick="data_del(this,'<?php echo site_url('/manager/market/coupons/delete/');?>
')" class="btn btn-danger radius"><i class="Hui-iconfont">&#xe60b;</i> 批量删除</a>
	</span></div>
	<div class="mt-20">
		<table class="table table-border table-bordered table-bg table-hover table-sort">
			<thead>
				<tr class="text-c">
					<th width="40"><input name="" type="checkbox" value=""></th>
					<th width="50">ID</th>
					<th>活动名称</th>
					<th width="80">优惠券金额</th>
					<th width="80">起用金额</th>
					<th width="120">开始时间</th>
					<th width="120">结束时间</th>
					<th width="150">操作</th>
				</tr>
			</thead>
			<tbody>
			<?php  $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['key']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['list']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['key']->key => $_smarty_tpl->tpl_vars['key']->value) {
$_smarty_tpl->tpl_vars['key']->_loop = true;
?>
				<tr class="text-c">
					<td><input name="id[]" type="checkbox" value="<?php echo $_smarty_tpl->tpl_vars['key']->value['id'];?>
"></td>
					<td><?php echo $_smarty_tpl->tpl_vars['key']->value['id'];?>
</td>
					<td class="text-l"><?php echo $_smarty_tpl->tpl_vars['key']->value['name'];?>
</td>
					<td class="text-l">￥<?php echo $_smarty_tpl->tpl_vars['key']->value['amount'];?>
</td>
					<td class="text-l">￥<?php echo $_smarty_tpl->tpl_vars['key']->value['use_price'];?>
</td>
					<td class="text-c"><?php echo date('Y-m-d H:i:s',$_smarty_tpl->tpl_vars['key']->value['start_time']);?>
</td>
					<td class="text-c"><?php echo date('Y-m-d H:i:s',$_smarty_tpl->tpl_vars['key']->value['end_time']);?>
</td>
					<td class="f-14 td-manage">
						<a style="text-decoration:none" class="ml-5" onClick="generate(<?php echo $_smarty_tpl->tpl_vars['key']->value['id'];?>
)" href="javascript:;" title="生成券"><i class="Hui-iconfont">&#xe600;</i></a>
						<a style="text-decoration:none" class="ml-5" onClick="open_iframe('查看券','<?php echo site_url("/manager/market/coupons/detail?cou_id=".((string)$_smarty_tpl->tpl_vars['key']->value['id']));?>
', '100%')" href="javascript:;" title="查看券"><i class="Hui-iconfont">&#xe665;</i></a>
						<a style="text-decoration:none" class="ml-5" href="<?php echo site_url("/manager/market/coupons/detail/".((string)$_smarty_tpl->tpl_vars['key']->value['id'])."?export=1");?>
" title="导出"><i class="Hui-iconfont">&#xe640;</i></a>
						<a style="text-decoration:none" class="ml-5" onClick="open_iframe('编辑','<?php echo site_url("/manager/market/coupons/edit/".((string)$_smarty_tpl->tpl_vars['key']->value['id']));?>
')" href="javascript:;" title="编辑"><i class="Hui-iconfont">&#xe6df;</i></a>
						<a style="text-decoration:none" class="ml-5" onClick="data_del(this,'<?php echo site_url('/manager/market/coupons/delete/');?>
','<?php echo $_smarty_tpl->tpl_vars['key']->value['id'];?>
')" href="javascript:;" title="删除"><i class="Hui-iconfont">&#xe6e2;</i></a>
					</td>
				</tr>
			<?php } ?>
			</tbody>
		</table>
	</div>
</div>
<!--分页-->
<?php echo page_view('page',$_smarty_tpl->tpl_vars['page_count']->value,search_array_to_link($_smarty_tpl->tpl_vars['search_where']->value));?>

<?php echo '<script'; ?>
 type="text/javascript" src="/public/js/jquery.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="/public/js/layer/layer.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="/public/js/jquery.dataTables.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="/public/H-ui/js/H-ui.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="/public/H-ui/js/H-ui.admin.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 language="JavaScript">
	//生成券
	function generate(id){
		layer.prompt({
			title: '生成张数,每次最多1000张',
			formType: 0 //prompt风格，支持0-2
		}, function(note){
			//提交请求
			$.ajax({
				type:"POST",
				url: "/manager/market/coupons/generate/"+id,
				data: "generate_num="+note,
				dataType:"json",
				success: function(data){
					if (data.status=='y') {
						layer.msg('生成完成');
						setTimeout(function(){
							window.location.reload();
						},1000)
					} else {
						layer.msg(data.info);
					}
				}
			});
		});
	}
<?php echo '</script'; ?>
>
</body>
</html><?php }} ?>
