<?php /* Smarty version Smarty-3.1.21-dev, created on 2019-08-13 23:11:14
         compiled from "D:\www\whlives-yimeng-master\views\manager\tool\adv\position_add.html" */ ?>
<?php /*%%SmartyHeaderCode:320155d52d312bad505-36091729%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '3f503a0bfa223f15c0d551bb71a0fb8903b3750d' => 
    array (
      0 => 'D:\\www\\whlives-yimeng-master\\views\\manager\\tool\\adv\\position_add.html',
      1 => 1533788760,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '320155d52d312bad505-36091729',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'item' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5d52d312bf7894_95204012',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5d52d312bf7894_95204012')) {function content_5d52d312bf7894_95204012($_smarty_tpl) {?><!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<meta name="renderer" content="webkit|ie-comp|ie-stand">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
<meta http-equiv="Cache-Control" content="no-siteapp" />
    <title><?php echo config_item('manager_title');?>
</title>
    <link href="/public/H-ui/css/H-ui.min.css" rel="stylesheet" type="text/css" />
    <link href="/public/H-ui/css/H-ui.admin.css" rel="stylesheet" type="text/css" />
    <link href="/public/H-ui/hui-iconfont/iconfont.css" rel="stylesheet" type="text/css">
</head>
<body>
<div class="pd-20">
	<form action="<?php echo site_url('/manager/tool/adv_position/save');?>
" method="post" class="form form-horizontal" id="add">
		<div class="row cl">
			<label class="form-label col-2"><span class="c-red">*</span>位置名称：</label>
			<div class="formControls col-6">
				<input type="text" class="input-text" value="" name="name" datatype="*" nullmsg="请输入位置名称！">
			</div>
		</div>
        <div class="row cl">
            <label class="form-label col-2"><span class="c-red">*</span>宽ⅹ高：</label>
            <div class="formControls col-8">
                <input type="text" class="input-text" value="0" name="width" datatype="n" nullmsg="请输入宽！" style="width: 80px">ⅹ
                <input type="text" class="input-text" value="0" name="height" datatype="n" nullmsg="请输入高！" style="width: 80px">广告位宽高，0则为不限制
            </div>
        </div>
        <div class="row cl">
            <label class="form-label col-2"><span class="c-red">*</span>展示方式：</label>
            <div class="formControls col-5 skin-minimal">
                <div class="radio-box">
                    <input type="radio" id="play_type-1" name="play_type" value="1" checked>
                    <label for="play_type-1">单个</label>
                </div>
                <div class="radio-box">
                    <input type="radio" id="play_type-2" name="play_type" value="2">
                    <label for="play_type-2">列表</label>
                </div>
                <div class="radio-box">
                    <input type="radio" id="play_type-3" name="play_type" value="3">
                    <label for="play_type-3">随机</label>
                </div>
            </div>
        </div>
        <div class="row cl">
            <label class="form-label col-2"><span class="c-red">*</span>是否开启：</label>
            <div class="formControls col-5 skin-minimal">
                <div class="radio-box">
                    <input type="radio" id="type-1" name="status" value="0" checked>
                    <label for="type-1">是</label>
                </div>
                <div class="radio-box">
                    <input type="radio" id="type-2" name="status" value="1">
                    <label for="type-2">否</label>
                </div>
            </div>
        </div>
		<div class="row cl">
			<div class="col-10 col-offset-2">
                <input type="hidden" name="id" value="">
				<button onClick="$('#add').submit();" class="btn btn-primary radius" type="submit"><i class="Hui-iconfont">&#xe632;</i> 保存</button>
				<button onClick="layer_close();" class="btn btn-default radius" type="button">&nbsp;&nbsp;取消&nbsp;&nbsp;</button>
			</div>
		</div>
	</form>
</div>
</div>
<?php echo '<script'; ?>
 type="text/javascript" src="/public/js/jquery.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="/public/js/validform.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="/public/js/layer/layer.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="/public/js/form.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="/public/H-ui/js/H-ui.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="/public/H-ui/js/H-ui.admin.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript">
    $(function(){
        //验证表单
        $.Tipmsg.r=null;
        $("#add").Validform({
            tiptype:function(msg){
                layer.msg(msg);
            },
            tipSweep:true,
            ajaxPost:true,
            callback:function(data){
                if(data.status=="y"){
                    layer_close();
                }
            }
        });
        //表单回填
        var formObj = new Form();
        formObj.init(<?php echo ch_json_encode($_smarty_tpl->tpl_vars['item']->value);?>
);
    })
<?php echo '</script'; ?>
>
</body>
</html><?php }} ?>
