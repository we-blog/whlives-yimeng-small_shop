<?php /* Smarty version Smarty-3.1.21-dev, created on 2019-08-13 23:14:14
         compiled from "D:\www\whlives-yimeng-master\views\manager\goods\brand\add.html" */ ?>
<?php /*%%SmartyHeaderCode:271165d52d3c642c7b7-30485500%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '0cb1b8e7d04044e3782b531268eb12f6d57fbaf5' => 
    array (
      0 => 'D:\\www\\whlives-yimeng-master\\views\\manager\\goods\\brand\\add.html',
      1 => 1533788760,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '271165d52d3c642c7b7-30485500',
  'function' => 
  array (
    'type' => 
    array (
      'parameter' => 
      array (
        'level' => 0,
      ),
      'compiled' => '',
    ),
  ),
  'variables' => 
  array (
    'item' => 0,
    'data' => 0,
    'key' => 0,
    'level' => 0,
    'cat_list' => 0,
  ),
  'has_nocache_code' => 0,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5d52d3c6545bf5_95297094',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5d52d3c6545bf5_95297094')) {function content_5d52d3c6545bf5_95297094($_smarty_tpl) {?><!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<meta name="renderer" content="webkit|ie-comp|ie-stand">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
<meta http-equiv="Cache-Control" content="no-siteapp" />
    <title><?php echo config_item('manager_title');?>
</title>
    <link href="/public/H-ui/css/H-ui.min.css" rel="stylesheet" type="text/css" />
    <link href="/public/H-ui/css/H-ui.admin.css" rel="stylesheet" type="text/css" />
    <link href="/public/H-ui/hui-iconfont/iconfont.css" rel="stylesheet" type="text/css">
</head>
<body>
<div class="pd-20">
	<form action="<?php echo site_url('/manager/goods/brand/save');?>
" method="post" class="form form-horizontal" id="add">
		<div class="row cl">
			<label class="form-label col-3"><span class="c-red">*</span>品牌名称：</label>
			<div class="formControls col-5">
				<input type="text" class="input-text" value="" name="name" datatype="*" nullmsg="请输入品牌名称！">
			</div>
		</div>
        <div class="row cl">
            <label class="form-label col-3"><span class="c-red">*</span>品牌logo：</label>
            <div class="formControls col-6">
                <a href="<?php echo $_smarty_tpl->tpl_vars['item']->value['logo'];?>
" target="_blank"><img src="<?php echo $_smarty_tpl->tpl_vars['item']->value['logo'];?>
" width="50" <?php if ($_smarty_tpl->tpl_vars['item']->value['logo']=='') {?>style="display: none;"<?php }?>></a>
                <input type="hidden" value="" name="logo">
                <input type="file" name="logo_pic" id="logo_pic" onchange="upload_file(this);" style="width: 150px;">
            </div>
        </div>
        <div class="row cl">
            <label class="form-label col-3"><span class="c-red">*</span>所属分类：</label>
            <div class="formControls col-5">
                <span class="select-box" style="height: 150px;">
				<select name="cat_id[]" class="select" multiple style="height: 140px;">
                    <option value="">请选择分类(可多选)</option>
                    <!--分类模板-->
                    <?php if (!function_exists('smarty_template_function_type')) {
    function smarty_template_function_type($_smarty_tpl,$params) {
    $saved_tpl_vars = $_smarty_tpl->tpl_vars;
    foreach ($_smarty_tpl->smarty->template_functions['type']['parameter'] as $key => $value) {$_smarty_tpl->tpl_vars[$key] = new Smarty_variable($value);};
    foreach ($params as $key => $value) {$_smarty_tpl->tpl_vars[$key] = new Smarty_variable($value);}?>
                    <?php  $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['key']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['data']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['key']->key => $_smarty_tpl->tpl_vars['key']->value) {
$_smarty_tpl->tpl_vars['key']->_loop = true;
?>
                    <option value="<?php echo $_smarty_tpl->tpl_vars['key']->value['id'];?>
" <?php if (in_array($_smarty_tpl->tpl_vars['key']->value['id'],$_smarty_tpl->tpl_vars['item']->value['cat_id'])) {?>selected<?php }?>><?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['loop'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['loop']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['name'] = 'loop';
$_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['level']->value) ? count($_loop) : max(0, (int) $_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['total']);
?>├<?php endfor; endif;
echo $_smarty_tpl->tpl_vars['key']->value['name'];?>
</option>
                    <?php smarty_template_function_type($_smarty_tpl,array('data'=>$_smarty_tpl->tpl_vars['key']->value['down'],'level'=>$_smarty_tpl->tpl_vars['level']->value+1));?>

                    <?php } ?>
                    <?php $_smarty_tpl->tpl_vars = $saved_tpl_vars;
foreach (Smarty::$global_tpl_vars as $key => $value) if(!isset($_smarty_tpl->tpl_vars[$key])) $_smarty_tpl->tpl_vars[$key] = $value;}}?>

                    <!--调用模板函数-->
                    <?php smarty_template_function_type($_smarty_tpl,array('data'=>$_smarty_tpl->tpl_vars['cat_list']->value));?>

                </select>
				</span>
            </div>
        </div>
		<div class="row cl">
			<label class="form-label col-3">排序值：</label>
			<div class="formControls col-5">
				<input type="text" class="input-text" value="99" name="sortnum" datatype="n" nullmsg="请输入排序值！" errormsg="排序值只能是数字">
			</div>
		</div>
		<div class="row cl">
			<div class="col-10 col-offset-2">
                <input type="hidden" name="id" value="">
				<button onClick="$('#add').submit();" class="btn btn-primary radius" type="submit"><i class="Hui-iconfont">&#xe632;</i> 保存</button>
				<button onClick="layer_close();" class="btn btn-default radius" type="button">&nbsp;&nbsp;取消&nbsp;&nbsp;</button>
			</div>
		</div>
	</form>
</div>
</div>
<?php echo '<script'; ?>
 type="text/javascript" src="/public/js/jquery.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="/public/js/validform.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="/public/js/layer/layer.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="/public/js/form.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="/public/H-ui/js/H-ui.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="/public/H-ui/js/H-ui.admin.js"><?php echo '</script'; ?>
>
<?php echo ajax_upload();?>

<?php echo '<script'; ?>
 type="text/javascript">
    $(function(){
        //验证表单
        $.Tipmsg.r=null;
        $("#add").Validform({
            tiptype:function(msg){
                layer.msg(msg);
            },
            tipSweep:true,
            ajaxPost:true,
            callback:function(data){
                if(data.status=="y"){
                    layer_close();
                }
            }
        });
        //表单回填
        var formObj = new Form();
        formObj.init(<?php echo ch_json_encode($_smarty_tpl->tpl_vars['item']->value);?>
);
    })
    //添加图片展示
    function show_upload(spec_pic, url){
        $("#"+spec_pic).parent().find('img').attr('src',url);
        $("#"+spec_pic).parent().find('img').show();
        $("#"+spec_pic).parent().find('a').attr('href',url);
        $("#"+spec_pic).parent().find('[type="hidden"]').val(url);
    }
<?php echo '</script'; ?>
>
</body>
</html><?php }} ?>
