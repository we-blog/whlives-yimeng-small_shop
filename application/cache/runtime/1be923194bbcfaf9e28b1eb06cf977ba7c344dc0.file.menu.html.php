<?php /* Smarty version Smarty-3.1.21-dev, created on 2019-08-13 23:03:05
         compiled from "D:\www\whlives-yimeng-master\views\manager\index\menu.html" */ ?>
<?php /*%%SmartyHeaderCode:17195d52d1290a1252-50284742%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '1be923194bbcfaf9e28b1eb06cf977ba7c344dc0' => 
    array (
      0 => 'D:\\www\\whlives-yimeng-master\\views\\manager\\index\\menu.html',
      1 => 1533788760,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '17195d52d1290a1252-50284742',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'menu_list' => 0,
    'val' => 0,
    'key' => 0,
    'admin_data' => 0,
    'k' => 0,
    'i' => 0,
    'v' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5d52d1291c6211_20447519',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5d52d1291c6211_20447519')) {function content_5d52d1291c6211_20447519($_smarty_tpl) {?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title><?php echo config_item('manager_title');?>
</title>
    <link href="/public/H-ui/css/style.css" rel="stylesheet" type="text/css">
    <link href="/public/H-ui/hui-iconfont/iconfont.css" rel="stylesheet" type="text/css">
    <?php echo '<script'; ?>
 type="text/javascript" src="/public/js/jquery.js"><?php echo '</script'; ?>
>
</head>
<body class="left_menu_body">
    <?php  $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['key']->_loop = false;
 $_smarty_tpl->tpl_vars['val'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['menu_list']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['key']->key => $_smarty_tpl->tpl_vars['key']->value) {
$_smarty_tpl->tpl_vars['key']->_loop = true;
 $_smarty_tpl->tpl_vars['val']->value = $_smarty_tpl->tpl_vars['key']->key;
?>
    <div class="left_menu">
        <div class="left_menu_parent"><a><?php echo $_smarty_tpl->tpl_vars['val']->value;?>
</a></div>
        <div class="left_menu_down">
            <?php $_smarty_tpl->tpl_vars['i'] = new Smarty_variable(0, null, 0);?>
            <?php  $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['k']->_loop = false;
 $_smarty_tpl->tpl_vars['v'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['key']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['k']->key => $_smarty_tpl->tpl_vars['k']->value) {
$_smarty_tpl->tpl_vars['k']->_loop = true;
 $_smarty_tpl->tpl_vars['v']->value = $_smarty_tpl->tpl_vars['k']->key;
?>
                <!--普通管理员-->
                <?php if (stripos($_smarty_tpl->tpl_vars['admin_data']->value['role'],trim($_smarty_tpl->tpl_vars['k']->value,'/manager'))!==false) {?>
                <li class="<?php if ($_smarty_tpl->tpl_vars['i']->value==0) {?>no_border_top<?php }?>">
                    <a target="main" href="<?php echo site_url($_smarty_tpl->tpl_vars['k']->value);?>
"> <?php echo $_smarty_tpl->tpl_vars['v']->value;?>
</a>
                </li>
                <?php }?>
                <!--超级管理员-->
                <?php if ($_smarty_tpl->tpl_vars['admin_data']->value['role_id']==0) {?>
                <li class="<?php if ($_smarty_tpl->tpl_vars['i']->value==0) {?>no_border_top<?php }?>">
                    <a target="main" href="<?php echo site_url($_smarty_tpl->tpl_vars['k']->value);?>
"> <?php echo $_smarty_tpl->tpl_vars['v']->value;?>
</a>
                </li>
                <?php }?>
                <?php $_smarty_tpl->tpl_vars['i'] = new Smarty_variable($_smarty_tpl->tpl_vars['i']->value+1, null, 0);?>
            <?php } ?>
        </div>
    </div>
    <?php } ?>

    <?php echo '<script'; ?>
 type="application/javascript">
        $(function() {
            //左侧菜单收缩展开
            $(".left_menu_parent").click(
                function () {
                    $(this).parent().find('.left_menu_down').toggle();
                }
            );

            //删除没有下级栏目的菜单
            $(".left_menu_down").each(
                    function () {
                        if($(this).children('li').length==0)
                        {
                            $(this).parent().remove();
                        }
                    }
            );
        });
    <?php echo '</script'; ?>
>
</body>
</html><?php }} ?>
