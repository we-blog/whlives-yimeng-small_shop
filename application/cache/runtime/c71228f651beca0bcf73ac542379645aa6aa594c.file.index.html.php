<?php /* Smarty version Smarty-3.1.21-dev, created on 2019-08-13 23:17:10
         compiled from "D:\www\whlives-yimeng-master\views\mobile\index.html" */ ?>
<?php /*%%SmartyHeaderCode:11875d52d476b78090-23478381%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'c71228f651beca0bcf73ac542379645aa6aa594c' => 
    array (
      0 => 'D:\\www\\whlives-yimeng-master\\views\\mobile\\index.html',
      1 => 1533788760,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '11875d52d476b78090-23478381',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'cat_list' => 0,
    'key' => 0,
    'goods_list' => 0,
    'k' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5d52d476cfeaf6_78490779',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5d52d476cfeaf6_78490779')) {function content_5d52d476cfeaf6_78490779($_smarty_tpl) {?><!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title><?php echo config_item('website_title');?>
</title>
    <meta content="initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport">
    <meta content="yes" name="apple-mobile-web-app-capable">
    <meta content="black" name="apple-mobile-web-app-status-bar-style">
    <meta content="telephone=no" name="format-detection">
    <link rel="stylesheet" href="/views/mobile/skin/css/swiper.min.css">
    <link rel="stylesheet" href="/views/mobile/skin/css/style.css">
</head>
<body>
<!-- banner start -->
<section class="banner">
    <div class="swiper-container">
        <div class="swiper-wrapper">
            <?php echo show_adv(1,'<div class="swiper-slide">','</div>');?>

        </div>
        <div class="swiper-pagination"></div>
    </div>
</section>
<!-- banner end -->
<section class="gridsitem">
    <!--顶部分类 start-->
    <?php $_smarty_tpl->tpl_vars['cat_list'] = new Smarty_variable(ym_list('goods_category',array('where'=>array('reid'=>0)),4,1,'sortnum asc,id asc'), null, 0);?>
    <?php  $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['key']->_loop = false;
 $_smarty_tpl->tpl_vars['val'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['cat_list']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['key']->key => $_smarty_tpl->tpl_vars['key']->value) {
$_smarty_tpl->tpl_vars['key']->_loop = true;
 $_smarty_tpl->tpl_vars['val']->value = $_smarty_tpl->tpl_vars['key']->key;
?>
    <a href="<?php ob_start();
echo get_web_type();
$_tmp1=ob_get_clean();?><?php echo site_url("/".$_tmp1."/category?cat_id=".((string)$_smarty_tpl->tpl_vars['key']->value['id']));?>
" class="gridsitem_a1">
        <div class="gridsitem_icon2">
            <img src="<?php echo $_smarty_tpl->tpl_vars['key']->value['image'];?>
">
        </div>
        <p class="grids_label cat_color_<?php echo $_smarty_tpl->tpl_vars['key']->value['id'];?>
"><?php echo $_smarty_tpl->tpl_vars['key']->value['name'];?>
</p>
    </a>
    <?php } ?>
    <!--顶部分类 end-->
    <a href="<?php ob_start();
echo get_web_type();
$_tmp2=ob_get_clean();?><?php echo site_url("/".$_tmp2."/category");?>
" class="gridsitem_a1">
        <div class="gridsitem_icon2">
            <img src="/views/mobile/skin/images/icon11.png">
        </div>
        <p class="grids_label">更多</p>
    </a>
</section>
<!--gridsitem end  -->
<section class="content">
    <div class="midline">
    </div>

    <section class="sortwrap">
        <!--单个分类模板start-->
        <?php  $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['key']->_loop = false;
 $_smarty_tpl->tpl_vars['val'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['cat_list']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['key']->key => $_smarty_tpl->tpl_vars['key']->value) {
$_smarty_tpl->tpl_vars['key']->_loop = true;
 $_smarty_tpl->tpl_vars['val']->value = $_smarty_tpl->tpl_vars['key']->key;
?>
        <section class="sortbox">
            <!-- 分类标题 start-->
            <div class="borderbox" onclick="window.location.href='<?php ob_start();
echo get_web_type();
$_tmp3=ob_get_clean();?><?php echo site_url("/".$_tmp3."/goods/goods_list?cat_id=".((string)$_smarty_tpl->tpl_vars['key']->value['id']));?>
'">
                <div class="borderboxL">
                    <img src="/views/mobile/skin/images/icon12.png">
                </div>
                <div class="borderboxM">
                    <?php echo $_smarty_tpl->tpl_vars['key']->value['name'];?>

                </div>
                <div class="borderboxR">
                </div>
            </div>
            <!-- 分类标题 end -->
            <!-- 分类内容 start -->
            <?php $_smarty_tpl->tpl_vars['goods_list'] = new Smarty_variable(ym_goods_list(array('cat_id'=>$_smarty_tpl->tpl_vars['key']->value['id']),3,1), null, 0);?>
            <?php  $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['k']->_loop = false;
 $_smarty_tpl->tpl_vars['v'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['goods_list']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['k']->key => $_smarty_tpl->tpl_vars['k']->value) {
$_smarty_tpl->tpl_vars['k']->_loop = true;
 $_smarty_tpl->tpl_vars['v']->value = $_smarty_tpl->tpl_vars['k']->key;
?>
            <div class="sortitembox">
                <a href="<?php ob_start();
echo get_web_type();
$_tmp4=ob_get_clean();?><?php echo site_url("/".$_tmp4."/goods/product/".((string)$_smarty_tpl->tpl_vars['k']->value['id']));?>
" class="gridsitem_a2">
                    <div class="sortImgbox">
                        <img src="<?php echo image_thumb($_smarty_tpl->tpl_vars['k']->value['image'],200,200);?>
">
                    </div>
                    <p class="pro_name"><?php echo $_smarty_tpl->tpl_vars['k']->value['name'];?>
</p>
                    <p class="pro_price">￥<?php echo $_smarty_tpl->tpl_vars['k']->value['sell_price'];?>
</p>
                </a>
            </div>
            <?php } ?>
            <!-- 分类内容 end-->
        </section>
        <?php } ?>
    </section>
</section>
<?php echo '<script'; ?>
 type="text/javascript" src="/public/js/jquery.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="/public/js/artTemplate.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="/views/mobile/skin/js/swiper.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="/views/mobile/skin/js/public.js"><?php echo '</script'; ?>
>
<?php echo $_smarty_tpl->getSubTemplate ("mobile/footer.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<?php echo '<script'; ?>
>
    //图片滑动
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',
        paginationClickable: true,
        autoplay: 1000,
        spaceBetween: 0,
    });
<?php echo '</script'; ?>
>
</body>
</html>




<?php }} ?>
