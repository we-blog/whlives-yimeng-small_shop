<?php /* Smarty version Smarty-3.1.21-dev, created on 2019-08-13 23:18:46
         compiled from "D:\www\whlives-yimeng-master\views\manager\member\user\withdraw_list.html" */ ?>
<?php /*%%SmartyHeaderCode:122355d52d4d6353e53-58359255%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'd9db3623527d7ac142c25a16b886ec55ee6ac2a8' => 
    array (
      0 => 'D:\\www\\whlives-yimeng-master\\views\\manager\\member\\user\\withdraw_list.html',
      1 => 1533788760,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '122355d52d4d6353e53-58359255',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'status' => 0,
    'v' => 0,
    'k' => 0,
    'list' => 0,
    'key' => 0,
    'type' => 0,
    'page_count' => 0,
    'search_where' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5d52d4d64caeb8_63945235',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5d52d4d64caeb8_63945235')) {function content_5d52d4d64caeb8_63945235($_smarty_tpl) {?><!DOCTYPE HTML>
<html>
<head>
	<meta charset="utf-8">
	<meta name="renderer" content="webkit|ie-comp|ie-stand">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
	<meta http-equiv="Cache-Control" content="no-siteapp" />
	<title><?php echo config_item('manager_title');?>
</title>
	<link href="/public/H-ui/css/H-ui.min.css" rel="stylesheet" type="text/css" />
	<link href="/public/H-ui/css/H-ui.admin.css" rel="stylesheet" type="text/css" />
	<link href="/public/H-ui/hui-iconfont/iconfont.css" rel="stylesheet" type="text/css">
</head>
<body>
<nav class="breadcrumb">
	<i class="Hui-iconfont">&#xe67f;</i> 首页 <span class="c-gray en">&gt;</span> 会员管理 <span class="c-gray en">&gt;</span> 会员提现列表 <a class="btn btn-success radius r mr-20" style="line-height:1.6em;margin-top:3px" href="javascript:location.replace(location.href);" title="刷新" ><i class="Hui-iconfont">&#xe68f;</i></a>
</nav>
<div class="pd-20" style="padding-top: 0px;">
	<div class="text-c pt-20">
		<form action="<?php echo site_url('/manager/member/user_withdraw/');?>
" method="post" class="form form-horizontal" id="search" name="search">
			<input type="text" name="username" placeholder="用户名" style="width:150px" class="input-text">
			<span class="select-box" style="width:150px">
				<select name="status" class="select">
					<option value="">选择状态</option>
					<?php  $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['k']->_loop = false;
 $_smarty_tpl->tpl_vars['v'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['status']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['k']->key => $_smarty_tpl->tpl_vars['k']->value) {
$_smarty_tpl->tpl_vars['k']->_loop = true;
 $_smarty_tpl->tpl_vars['v']->value = $_smarty_tpl->tpl_vars['k']->key;
?>
					<option value="<?php echo $_smarty_tpl->tpl_vars['v']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['k']->value;?>
</option>
					<?php } ?>
				</select>
			</span>
			<button class="btn btn-success" type="submit"><i class="Hui-iconfont">&#xe665;</i> 搜索</button>
		</form>
	</div>
	<div class="mt-20">
		<table class="table table-border table-bordered table-bg table-hover table-sort">
			<thead>
			<tr class="text-c">
				<th width="40"><input name="" type="checkbox" value=""></th>
				<th width="50">ID</th>
				<th>会员名</th>
				<th width="50">提现金额</th>
				<th width="50">提现方式</th>
				<th width="50">开户名</th>
				<th width="170">提现账号</th>
				<th width="80">提交时间</th>
				<th width="80">处理时间</th>
				<th width="140">操作</th>
			</tr>
			</thead>
			<tbody>
			<?php  $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['key']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['list']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['key']->key => $_smarty_tpl->tpl_vars['key']->value) {
$_smarty_tpl->tpl_vars['key']->_loop = true;
?>
			<tr class="text-c">
				<td><input name="id[]" type="checkbox" value="<?php echo $_smarty_tpl->tpl_vars['key']->value['id'];?>
"></td>
				<td><?php echo $_smarty_tpl->tpl_vars['key']->value['id'];?>
</td>
				<td class="text-l"><?php echo $_smarty_tpl->tpl_vars['key']->value['username'];?>
</td>
				<td class="text-l">￥<?php echo $_smarty_tpl->tpl_vars['key']->value['amount'];?>
</td>
				<td><?php echo $_smarty_tpl->tpl_vars['type']->value[$_smarty_tpl->tpl_vars['key']->value['type']];?>
</td>
				<td class="text-l"><?php echo $_smarty_tpl->tpl_vars['key']->value['name'];?>
</td>
				<td class="text-l">
					<?php if ($_smarty_tpl->tpl_vars['key']->value['type']==1) {?>
					银行:<?php echo $_smarty_tpl->tpl_vars['key']->value['bank_name'];?>
<br>
					<?php }?>
					账号:<?php echo $_smarty_tpl->tpl_vars['key']->value['pay_number'];?>

				</td>
				<td><?php echo date('Y-m-d',$_smarty_tpl->tpl_vars['key']->value['addtime']);?>
</td>
				<td><?php if ($_smarty_tpl->tpl_vars['key']->value['endtime']!='') {
echo date('Y-m-d',$_smarty_tpl->tpl_vars['key']->value['endtime']);
}?></td>
				<td class="f-14 td-manage" style="line-height: 30px;">
					<a style="text-decoration:none" class="ml-5" onClick="open_iframe('备注','<?php echo site_url("/manager/member/user_withdraw/edit/".((string)$_smarty_tpl->tpl_vars['key']->value['id']));?>
')" href="javascript:;" title="备注"><i class="Hui-iconfont">&#xe6df;</i></a>
					<?php if ($_smarty_tpl->tpl_vars['key']->value['status']==0) {?>
					<a href="javascript:;" onClick="update_status(<?php echo $_smarty_tpl->tpl_vars['key']->value['id'];?>
,1)" class="btn btn-primary radius">审核通过</a>
					<a href="javascript:;" onClick="update_status(<?php echo $_smarty_tpl->tpl_vars['key']->value['id'];?>
,2)" class="btn btn-danger radius">拒绝并退还资金</a>
					<a href="javascript:;" onClick="update_status(<?php echo $_smarty_tpl->tpl_vars['key']->value['id'];?>
,3)" class="btn btn-danger radius">拒绝不退还资金</a>
					<?php } else { ?>
					<span class="label label-success radius"><?php echo $_smarty_tpl->tpl_vars['status']->value[$_smarty_tpl->tpl_vars['key']->value['status']];?>
</span>
					<?php }?>
				</td>
			</tr>
			<?php } ?>
			</tbody>
		</table>
	</div>
</div>
<!--分页-->
<?php echo page_view('page',$_smarty_tpl->tpl_vars['page_count']->value,search_array_to_link($_smarty_tpl->tpl_vars['search_where']->value));?>

<?php echo '<script'; ?>
 type="text/javascript" src="/public/js/jquery.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="/public/js/layer/layer.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="/public/js/jquery.dataTables.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="/public/js/form.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="/public/H-ui/js/H-ui.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="/public/H-ui/js/H-ui.admin.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 language="JavaScript">
	$(function(){
		//表单回填
		var formObj = new Form();
		formObj.init(<?php echo ch_json_encode($_smarty_tpl->tpl_vars['search_where']->value);?>
);
	})

	/*更新信息状态
	 * @ids会员id 对象
	 * @status 状态
	 * */
	function update_status(ids,status){
		if(ids==''){
			layer.msg('没有选择任何数据!');
			return false;
		}
		layer.confirm('确认要执行吗？',function(index){
			$.ajax({
				type:"POST",
				url: "<?php echo site_url('/manager/member/user_withdraw/update_status');?>
",
				data:"id="+ids+"&status="+status,
				dataType:"json",
				success: function(data){
					if(data.status!='y'){
						layer.msg(data.info);
					}else{
						window.location.reload();
					}
				}
			});
		});
	}
<?php echo '</script'; ?>
>
</body>
</html><?php }} ?>
