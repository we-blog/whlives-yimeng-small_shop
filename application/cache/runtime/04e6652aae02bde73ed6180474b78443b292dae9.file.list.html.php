<?php /* Smarty version Smarty-3.1.21-dev, created on 2019-08-13 23:14:22
         compiled from "D:\www\whlives-yimeng-master\views\manager\goods\category\list.html" */ ?>
<?php /*%%SmartyHeaderCode:127255d52d3cebb4294-81825272%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '04e6652aae02bde73ed6180474b78443b292dae9' => 
    array (
      0 => 'D:\\www\\whlives-yimeng-master\\views\\manager\\goods\\category\\list.html',
      1 => 1533788760,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '127255d52d3cebb4294-81825272',
  'function' => 
  array (
    'type' => 
    array (
      'parameter' => 
      array (
        'level' => 0,
      ),
      'compiled' => '',
    ),
  ),
  'variables' => 
  array (
    'data' => 0,
    'key' => 0,
    'level' => 0,
    'list' => 0,
  ),
  'has_nocache_code' => 0,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5d52d3cecb9e46_63408918',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5d52d3cecb9e46_63408918')) {function content_5d52d3cecb9e46_63408918($_smarty_tpl) {?><!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<meta name="renderer" content="webkit|ie-comp|ie-stand">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
<meta http-equiv="Cache-Control" content="no-siteapp" />
	<title><?php echo config_item('manager_title');?>
</title>
	<link href="/public/H-ui/css/H-ui.min.css" rel="stylesheet" type="text/css" />
	<link href="/public/H-ui/css/H-ui.admin.css" rel="stylesheet" type="text/css" />
	<link href="/public/H-ui/hui-iconfont/iconfont.css" rel="stylesheet" type="text/css">
</head>
<body>
<nav class="breadcrumb">
	<i class="Hui-iconfont">&#xe67f;</i> 首页 <span class="c-gray en">&gt;</span> 商品管理 <span class="c-gray en">&gt;</span> 分类列表 <a class="btn btn-success radius r mr-20" style="line-height:1.6em;margin-top:3px" href="javascript:location.replace(location.href);" title="刷新" ><i class="Hui-iconfont">&#xe68f;</i></a>
</nav>
<div class="pd-20" style="padding-top: 0px;">
	<div class="cl pd-5 bg-1 bk-gray mt-20"> <span class="l">
		<a href="javascript:;" class="btn btn-primary radius" onclick="open_iframe('添加顶级分类','<?php echo site_url('/manager/goods/category/add');?>
')"><i class="Hui-iconfont">&#xe600;</i> 添加顶级分类</a>
	</span></div>
	<div class="mt-20">
		<table class="table table-border table-bordered table-bg table-hover table-sort">
			<thead>
				<tr class="text-c">
					<th width="50">ID</th>
					<th>分类名称</th>
					<th width="60">排序</th>
					<th width="120">操作</th>
				</tr>
			</thead>
			<tbody>
			<!--分类模板-->
			<?php if (!function_exists('smarty_template_function_type')) {
    function smarty_template_function_type($_smarty_tpl,$params) {
    $saved_tpl_vars = $_smarty_tpl->tpl_vars;
    foreach ($_smarty_tpl->smarty->template_functions['type']['parameter'] as $key => $value) {$_smarty_tpl->tpl_vars[$key] = new Smarty_variable($value);};
    foreach ($params as $key => $value) {$_smarty_tpl->tpl_vars[$key] = new Smarty_variable($value);}?>
				<?php  $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['key']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['data']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['key']->key => $_smarty_tpl->tpl_vars['key']->value) {
$_smarty_tpl->tpl_vars['key']->_loop = true;
?>
				<tr class="text-c">
					<td><?php echo $_smarty_tpl->tpl_vars['key']->value['id'];?>
</td>
					<td class="text-l">
						<span style="margin-left: <?php echo $_smarty_tpl->tpl_vars['level']->value*15;?>
px;"></span><?php if ($_smarty_tpl->tpl_vars['key']->value['image']!='') {?><a><img width="30" height="30" class="album-img" src="<?php echo $_smarty_tpl->tpl_vars['key']->value['image'];?>
"></a><?php }
echo $_smarty_tpl->tpl_vars['key']->value['name'];?>

						<?php if ($_smarty_tpl->tpl_vars['key']->value['flag']=='1') {?><span class="c-red">推荐</span><?php }?>
					</td>
					<td><?php echo $_smarty_tpl->tpl_vars['key']->value['sortnum'];?>
</td>
					<td class="f-14 td-manage">
						<a style="text-decoration:none" class="ml-5" onClick="open_iframe('添加子分类','<?php echo site_url("/manager/goods/category/add/".((string)$_smarty_tpl->tpl_vars['key']->value['id']));?>
')" href="javascript:;" title="添加子分类"><i class="Hui-iconfont">&#xe600;</i></a>
						<a style="text-decoration:none" class="ml-5" onClick="open_iframe('编辑','<?php echo site_url("/manager/goods/category/edit/".((string)$_smarty_tpl->tpl_vars['key']->value['id']));?>
')" href="javascript:;" title="编辑"><i class="Hui-iconfont">&#xe6df;</i></a>
						<a style="text-decoration:none" class="ml-5" onClick="data_del(this,'<?php echo site_url('/manager/goods/category/delete/');?>
','<?php echo $_smarty_tpl->tpl_vars['key']->value['id'];?>
')" href="javascript:;" title="删除"><i class="Hui-iconfont">&#xe6e2;</i></a>
					</td>
				</tr>
				<?php smarty_template_function_type($_smarty_tpl,array('data'=>$_smarty_tpl->tpl_vars['key']->value['down'],'level'=>$_smarty_tpl->tpl_vars['level']->value+1));?>

				<?php } ?>
			<?php $_smarty_tpl->tpl_vars = $saved_tpl_vars;
foreach (Smarty::$global_tpl_vars as $key => $value) if(!isset($_smarty_tpl->tpl_vars[$key])) $_smarty_tpl->tpl_vars[$key] = $value;}}?>

			<!--调用模板函数-->
			<?php smarty_template_function_type($_smarty_tpl,array('data'=>$_smarty_tpl->tpl_vars['list']->value));?>

			</tbody>
		</table>
	</div>
</div>
<?php echo '<script'; ?>
 type="text/javascript" src="/public/js/jquery.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="/public/js/layer/layer.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="/public/js/jquery.dataTables.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="/public/H-ui/js/H-ui.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="/public/H-ui/js/H-ui.admin.js"><?php echo '</script'; ?>
>
</body>
</html><?php }} ?>
