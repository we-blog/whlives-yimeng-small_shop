<?php /* Smarty version Smarty-3.1.21-dev, created on 2019-08-13 23:13:42
         compiled from "D:\www\whlives-yimeng-master\views\manager\goods\goods\list.html" */ ?>
<?php /*%%SmartyHeaderCode:291205d52d3a658cba1-26179837%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '2feb8b67f7602c25f364682ddc0c64d39ada460e' => 
    array (
      0 => 'D:\\www\\whlives-yimeng-master\\views\\manager\\goods\\goods\\list.html',
      1 => 1533788760,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '291205d52d3a658cba1-26179837',
  'function' => 
  array (
    'type' => 
    array (
      'parameter' => 
      array (
        'level' => 0,
      ),
      'compiled' => '',
    ),
  ),
  'variables' => 
  array (
    'data' => 0,
    'key' => 0,
    'level' => 0,
    'cat_list' => 0,
    'brand_list' => 0,
    'k' => 0,
    'shop_list' => 0,
    'goods_status' => 0,
    'v' => 0,
    'search_where' => 0,
    'list' => 0,
    'page_count' => 0,
  ),
  'has_nocache_code' => 0,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5d52d3a68d8862_77855584',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5d52d3a68d8862_77855584')) {function content_5d52d3a68d8862_77855584($_smarty_tpl) {?><!DOCTYPE HTML>
<html>
<head>
	<meta charset="utf-8">
	<meta name="renderer" content="webkit|ie-comp|ie-stand">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
	<meta http-equiv="Cache-Control" content="no-siteapp" />
	<title><?php echo config_item('manager_title');?>
</title>
	<link href="/public/H-ui/css/H-ui.min.css" rel="stylesheet" type="text/css" />
	<link href="/public/H-ui/css/H-ui.admin.css" rel="stylesheet" type="text/css" />
	<link href="/public/H-ui/hui-iconfont/iconfont.css" rel="stylesheet" type="text/css">
</head>
<body>
<nav class="breadcrumb">
	<i class="Hui-iconfont">&#xe67f;</i> 首页 <span class="c-gray en">&gt;</span> 商品管理 <span class="c-gray en">&gt;</span> 商品列表 <a class="btn btn-success radius r mr-20" style="line-height:1.6em;margin-top:3px" href="javascript:location.replace(location.href);" title="刷新" ><i class="Hui-iconfont">&#xe68f;</i></a>
</nav>
<div class="pd-20" style="padding-top: 0px;">
	<div class="text-c pt-20">
		<form action="<?php echo site_url('/manager/goods/goods/');?>
" method="post" class="form form-horizontal" id="search" name="search">
			<input type="text" name="name" placeholder="商品标题" style="width:150px" class="input-text">
			<span class="select-box" style="width:150px">
				<select name="cat_id" class="select">
					<option value="">商品分类</option>
					<!--分类模板-->
					<?php if (!function_exists('smarty_template_function_type')) {
    function smarty_template_function_type($_smarty_tpl,$params) {
    $saved_tpl_vars = $_smarty_tpl->tpl_vars;
    foreach ($_smarty_tpl->smarty->template_functions['type']['parameter'] as $key => $value) {$_smarty_tpl->tpl_vars[$key] = new Smarty_variable($value);};
    foreach ($params as $key => $value) {$_smarty_tpl->tpl_vars[$key] = new Smarty_variable($value);}?>
					<?php  $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['key']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['data']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['key']->key => $_smarty_tpl->tpl_vars['key']->value) {
$_smarty_tpl->tpl_vars['key']->_loop = true;
?>
					<option value="<?php echo $_smarty_tpl->tpl_vars['key']->value['id'];?>
"><?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['loop'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['loop']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['name'] = 'loop';
$_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['level']->value) ? count($_loop) : max(0, (int) $_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['total']);
?>├<?php endfor; endif;
echo $_smarty_tpl->tpl_vars['key']->value['name'];?>
</option>
					<?php smarty_template_function_type($_smarty_tpl,array('data'=>$_smarty_tpl->tpl_vars['key']->value['down'],'level'=>$_smarty_tpl->tpl_vars['level']->value+1));?>

					<?php } ?>
					<?php $_smarty_tpl->tpl_vars = $saved_tpl_vars;
foreach (Smarty::$global_tpl_vars as $key => $value) if(!isset($_smarty_tpl->tpl_vars[$key])) $_smarty_tpl->tpl_vars[$key] = $value;}}?>

					<!--调用模板函数-->
					<?php smarty_template_function_type($_smarty_tpl,array('data'=>$_smarty_tpl->tpl_vars['cat_list']->value));?>

				</select>
			</span>
			<span class="select-box" style="width:100px">
				<select name="brand_id" class="select">
					<option value="">选择品牌</option>
					<?php  $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['k']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['brand_list']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['k']->key => $_smarty_tpl->tpl_vars['k']->value) {
$_smarty_tpl->tpl_vars['k']->_loop = true;
?>
					<option value="<?php echo $_smarty_tpl->tpl_vars['k']->value['id'];?>
"><?php echo $_smarty_tpl->tpl_vars['k']->value['name'];?>
</option>
					<?php } ?>
				</select>
			</span>
			<span class="select-box" style="width:100px">
				<select name="shop_id" class="select">
					<option value="">来源店铺</option>
					<?php  $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['k']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['shop_list']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['k']->key => $_smarty_tpl->tpl_vars['k']->value) {
$_smarty_tpl->tpl_vars['k']->_loop = true;
?>
					<option value="<?php echo $_smarty_tpl->tpl_vars['k']->value['m_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['k']->value['shop_name'];?>
</option>
					<?php } ?>
				</select>
			</span>
			<span class="select-box" style="width:100px">
				<select name="flag_type" class="select">
					<option value="">推荐状态</option>
					<option value="is_flag">推荐</option>
					<option value="is_new">新品</option>
					<option value="is_hot">最热</option>
				</select>
			</span>
			<span class="select-box" style="width:100px">
				<select name="status" class="select">
					<option value="">选择状态</option>
					<?php  $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['k']->_loop = false;
 $_smarty_tpl->tpl_vars['v'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['goods_status']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['k']->key => $_smarty_tpl->tpl_vars['k']->value) {
$_smarty_tpl->tpl_vars['k']->_loop = true;
 $_smarty_tpl->tpl_vars['v']->value = $_smarty_tpl->tpl_vars['k']->key;
?>
					<option value="<?php echo $_smarty_tpl->tpl_vars['v']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['k']->value;?>
</option>
					<?php } ?>
				</select>
			</span>
			<button class="btn btn-success" type="submit"><i class="Hui-iconfont">&#xe665;</i> 搜索</button>
		</form>
	</div>
	<div class="cl pd-5 bg-1 bk-gray mt-20"> <span class="l">
		<?php if ($_smarty_tpl->tpl_vars['search_where']->value['status']==1) {?>
		<a href="<?php echo site_url('/manager/goods/goods/');?>
" class="btn btn-primary radius"><i class="Hui-iconfont">&#xe678;</i> 返回列表</a>
		<a href="javascript:;" onclick="data_del(this,'<?php echo site_url('/manager/goods/goods/delete/');?>
')" class="btn btn-danger radius"><i class="Hui-iconfont">&#xe60b;</i> 彻底删除</a>
		<a href="javascript:;" onclick="data_del(this,'<?php echo site_url('/manager/goods/goods/reduction_recycle/');?>
')" class="btn btn-primary radius"><i class="Hui-iconfont">&#xe66b;</i> 还原</a>
		<?php } else { ?>
		<a href="javascript:;" onclick="open_iframe('添加商品','<?php echo site_url('/manager/goods/goods/edit');?>
','100%')" class="btn btn-primary radius"><i class="Hui-iconfont">&#xe600;</i> 添加商品</a>
		<a href="javascript:;" onclick="data_del(this,'<?php echo site_url('/manager/goods/goods/update_status/');?>
?status=0')" class="btn btn-primary radius">上架</a>
		<a href="javascript:;" onclick="data_del(this,'<?php echo site_url('/manager/goods/goods/update_status/');?>
?status=2')" class="btn btn-danger radius">下架</a>
		<a href="javascript:;" onclick="data_del(this,'<?php echo site_url('/manager/goods/goods/update_status/');?>
?status=3')" class="btn btn-primary radius">待审</a>
		<a href="javascript:;" onclick="data_del(this,'<?php echo site_url('/manager/goods/goods/update_status/');?>
?status=4')" class="btn btn-danger radius">审核拒绝</a>
		<a href="javascript:;" onclick="data_del(this,'<?php echo site_url('/manager/goods/goods/update_flag/');?>
?type=is_flag&value=1')" class="btn btn-primary radius">推荐</a>
		<a href="javascript:;" onclick="data_del(this,'<?php echo site_url('/manager/goods/goods/update_flag/');?>
?type=is_flag&value=0')" class="btn btn-danger radius">取消推荐</a>
		<a href="javascript:;" onclick="data_del(this,'<?php echo site_url('/manager/goods/goods/update_flag/');?>
?type=is_hot&value=1')" class="btn btn-primary radius">最热</a>
		<a href="javascript:;" onclick="data_del(this,'<?php echo site_url('/manager/goods/goods/update_flag/');?>
?type=is_hot&value=0')" class="btn btn-danger radius">取消最热</a>
		<a href="javascript:;" onclick="data_del(this,'<?php echo site_url('/manager/goods/goods/update_flag/');?>
?type=is_new&value=1')" class="btn btn-primary radius">新品</a>
		<a href="javascript:;" onclick="data_del(this,'<?php echo site_url('/manager/goods/goods/update_flag/');?>
?type=is_new&value=0')" class="btn btn-danger radius">取消新品</a>
		<a href="javascript:;" onclick="data_del(this,'<?php echo site_url('/manager/goods/goods/delete_recycle/');?>
')" class="btn btn-danger radius"><i class="Hui-iconfont">&#xe60b;</i> 批量删除</a>
		<a href="<?php echo site_url('/manager/goods/goods/');?>
?status=1" class="btn btn-primary radius"><i class="Hui-iconfont">&#xe6e2;</i> 回收站</a>
		<?php }?>
	</span></div>
	<div class="mt-20">
		<table class="table table-border table-bordered table-bg table-hover table-sort">
			<thead>
			<tr class="text-c">
				<th width="30"><input name="" type="checkbox" value=""></th>
				<th width="50">ID</th>
				<th>商品名称</th>
				<th width="80">分类</th>
				<th width="50">库存</th>
				<th width="50">市场价格</th>
				<th width="50">销售价格</th>
				<th width="50">排序</th>
				<?php if ($_smarty_tpl->tpl_vars['search_where']->value['status']==0) {?><th width="100">状态</th><?php }?>
				<th width="80">操作</th>
			</tr>
			</thead>
			<tbody>
			<?php  $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['key']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['list']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['key']->key => $_smarty_tpl->tpl_vars['key']->value) {
$_smarty_tpl->tpl_vars['key']->_loop = true;
?>
			<tr class="text-c">
				<td><input name="id[]" type="checkbox" value="<?php echo $_smarty_tpl->tpl_vars['key']->value['id'];?>
"></td>
				<td><?php echo $_smarty_tpl->tpl_vars['key']->value['id'];?>
</td>
				<td class="text-l">
					<a target="_blank" href="<?php echo site_url("/web/goods/product/".((string)$_smarty_tpl->tpl_vars['key']->value['id']));?>
"><img width="30" height="30" class="album-img" src="<?php echo $_smarty_tpl->tpl_vars['key']->value['image'];?>
"></a><?php echo $_smarty_tpl->tpl_vars['key']->value['name'];?>

					<?php if ($_smarty_tpl->tpl_vars['key']->value['is_hot']) {?><span class="label label-success radius">最热</span><?php }?>
					<?php if ($_smarty_tpl->tpl_vars['key']->value['is_new']) {?><span class="label label-success radius">新品</span><?php }?>
					<?php if ($_smarty_tpl->tpl_vars['key']->value['is_flag']) {?><span class="label label-success radius">推荐</span><?php }?>
				</td>
				<td class="text-l"><?php echo $_smarty_tpl->tpl_vars['key']->value['cat_name'];?>
</td>
				<td class="text-l"><?php echo $_smarty_tpl->tpl_vars['key']->value['store_nums'];?>
</td>
				<td class="text-l">￥<?php echo $_smarty_tpl->tpl_vars['key']->value['market_price'];?>
</td>
				<td class="text-l">￥<?php echo $_smarty_tpl->tpl_vars['key']->value['sell_price'];?>
</td>
				<td class="text-l"><?php echo $_smarty_tpl->tpl_vars['key']->value['sortnum'];?>
</td>
				<?php if ($_smarty_tpl->tpl_vars['search_where']->value['status']==0) {?>
				<td>
					<span class="select-box">
						<select name="status" class="select" onchange="update_status('<?php echo $_smarty_tpl->tpl_vars['key']->value['id'];?>
',this.value);">
							<?php  $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['k']->_loop = false;
 $_smarty_tpl->tpl_vars['v'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['goods_status']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['k']->key => $_smarty_tpl->tpl_vars['k']->value) {
$_smarty_tpl->tpl_vars['k']->_loop = true;
 $_smarty_tpl->tpl_vars['v']->value = $_smarty_tpl->tpl_vars['k']->key;
?>
							<option value="<?php echo $_smarty_tpl->tpl_vars['v']->value;?>
" <?php if ($_smarty_tpl->tpl_vars['key']->value['status']==$_smarty_tpl->tpl_vars['v']->value) {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['k']->value;?>
</option>
							<?php } ?>
						</select>
					</span>
				</td>
				<?php }?>
				<td class="f-14 td-manage text-l">
					<a style="text-decoration:none" class="ml-5" onClick="open_iframe('编辑','<?php echo site_url("/manager/goods/goods/edit/".((string)$_smarty_tpl->tpl_vars['key']->value['id']));?>
','100%')" href="javascript:;" title="编辑"><i class="Hui-iconfont">&#xe6df;</i></a>
					<?php if ($_smarty_tpl->tpl_vars['search_where']->value['status']==1) {?>
					<a style="text-decoration:none" class="ml-5" onClick="data_del(this,'<?php echo site_url('/manager/goods/goods/delete/');?>
','<?php echo $_smarty_tpl->tpl_vars['key']->value['id'];?>
')" href="javascript:;" title="彻底删除"><i class="Hui-iconfont">&#xe6e2;</i></a>
					<?php } else { ?>
					<a style="text-decoration:none" class="ml-5" onClick="data_del(this,'<?php echo site_url('/manager/goods/goods/delete_recycle/');?>
','<?php echo $_smarty_tpl->tpl_vars['key']->value['id'];?>
')" href="javascript:;" title="删除到回收站"><i class="Hui-iconfont">&#xe6e2;</i></a>
					<?php }?>
					<?php if ($_smarty_tpl->tpl_vars['key']->value['shop_id']>1) {?>
					<a style="text-decoration:none" class="ml-5" onClick="open_iframe('查看商家','<?php echo site_url("/manager/member/shop/view/".((string)$_smarty_tpl->tpl_vars['key']->value['shop_id']));?>
','100%')" href="javascript:;" title="查看商家"><i class="Hui-iconfont">&#xe66a;</i></a>
					<?php }?>
				</td>
			</tr>
			<?php } ?>
			</tbody>
		</table>
	</div>
</div>
<!--分页-->
<?php echo page_view('page',$_smarty_tpl->tpl_vars['page_count']->value,search_array_to_link($_smarty_tpl->tpl_vars['search_where']->value));?>

<?php echo '<script'; ?>
 type="text/javascript" src="/public/js/jquery.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="/public/js/layer/layer.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="/public/js/jquery.dataTables.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="/public/js/form.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="/public/H-ui/js/H-ui.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="/public/H-ui/js/H-ui.admin.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 language="JavaScript">
	$(function(){
		//表单回填
		var formObj = new Form();
		formObj.init(<?php echo ch_json_encode($_smarty_tpl->tpl_vars['search_where']->value);?>
);
	})

	/*更新信息状态
	 * @ids商品id 对象
	 * @status 状态
	 * */
	function update_status(ids,status){
		if(ids==''){
			layer.msg('没有选择任何数据!');
			return false;
		}
		$.ajax({
			type:"POST",
			url: "<?php echo site_url('/manager/goods/goods/update_status');?>
",
			data:"id="+ids+"&status="+status,
			dataType:"json",
			success: function(data){
				if(data.status!='y'){
					layer.msg(data.info);
				}
			}
		});
	}
<?php echo '</script'; ?>
>
</body>
</html><?php }} ?>
