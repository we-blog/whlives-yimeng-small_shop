<?php /* Smarty version Smarty-3.1.21-dev, created on 2019-08-13 23:14:23
         compiled from "D:\www\whlives-yimeng-master\views\manager\goods\category\add.html" */ ?>
<?php /*%%SmartyHeaderCode:745d52d3cfed91e4-83374614%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '8e130a15733c962bf6cab74169b9d01e59e09915' => 
    array (
      0 => 'D:\\www\\whlives-yimeng-master\\views\\manager\\goods\\category\\add.html',
      1 => 1533788760,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '745d52d3cfed91e4-83374614',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'item' => 0,
    'reid' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5d52d3d001bb03_30561204',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5d52d3d001bb03_30561204')) {function content_5d52d3d001bb03_30561204($_smarty_tpl) {?><!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<meta name="renderer" content="webkit|ie-comp|ie-stand">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
<meta http-equiv="Cache-Control" content="no-siteapp" />
    <title><?php echo config_item('manager_title');?>
</title>
    <link href="/public/H-ui/css/H-ui.min.css" rel="stylesheet" type="text/css" />
    <link href="/public/H-ui/css/H-ui.admin.css" rel="stylesheet" type="text/css" />
    <link href="/public/H-ui/hui-iconfont/iconfont.css" rel="stylesheet" type="text/css">
</head>
<body>
<div class="pd-20">
	<form action="<?php echo site_url('/manager/goods/category/save');?>
" method="post" class="form form-horizontal" id="add">
		<div class="row cl">
			<label class="form-label col-3"><span class="c-red">*</span>分类名称：</label>
			<div class="formControls col-5">
				<input type="text" class="input-text" value="" name="name" datatype="*" nullmsg="请输入分类名称！">
			</div>
		</div>
        <div class="row cl">
            <label class="form-label col-3"><span class="c-red">*</span>图片：</label>
            <div class="formControls col-6">
                <a href="<?php echo $_smarty_tpl->tpl_vars['item']->value['image'];?>
" target="_blank"><img src="<?php echo $_smarty_tpl->tpl_vars['item']->value['image'];?>
" width="50" <?php if ($_smarty_tpl->tpl_vars['item']->value['image']=='') {?>style="display: none;"<?php }?>></a>
                <input type="hidden" value="" name="image">
                <input type="file" name="image_pic" id="image_pic" onchange="upload_file(this);" style="width: 150px;">
            </div>
        </div>
		<div class="row cl">
			<label class="form-label col-3">排序值：</label>
			<div class="formControls col-5">
				<input type="text" class="input-text" value="99" name="sortnum" datatype="n" nullmsg="请输入排序值！" errormsg="排序值只能是数字">
			</div>
		</div>
        <div class="row cl">
            <label class="form-label col-3">是否推荐：</label>
            <div class="formControls col-5">
                <div class="radio-box">
                    <input type="radio" id="flag-1" name="flag" value="0" checked>
                    <label for="flag-1">否</label>
                </div>
                <div class="radio-box">
                    <input type="radio" id="flag-2" name="flag" value="1">
                    <label for="flag-2">是</label>
                </div>
            </div>
        </div>
		<div class="row cl">
			<div class="col-10 col-offset-2">
                <input type="hidden" name="reid" value="<?php echo $_smarty_tpl->tpl_vars['reid']->value;?>
">
                <input type="hidden" name="id" value="">
				<button onClick="$('#add').submit();" class="btn btn-primary radius" type="submit"><i class="Hui-iconfont">&#xe632;</i> 保存</button>
				<button onClick="layer_close();" class="btn btn-default radius" type="button">&nbsp;&nbsp;取消&nbsp;&nbsp;</button>
			</div>
		</div>
	</form>
</div>
</div>
<?php echo '<script'; ?>
 type="text/javascript" src="/public/js/jquery.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="/public/js/validform.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="/public/js/layer/layer.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="/public/js/form.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="/public/H-ui/js/H-ui.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="/public/H-ui/js/H-ui.admin.js"><?php echo '</script'; ?>
>
<?php echo ajax_upload();?>

<?php echo '<script'; ?>
 type="text/javascript">
    $(function(){
        //验证表单
        $.Tipmsg.r=null;
        $("#add").Validform({
            tiptype:function(msg){
                layer.msg(msg);
            },
            tipSweep:true,
            ajaxPost:true,
            callback:function(data){
                if(data.status=="y"){
                    layer_close();
                }
            }
        });
        //表单回填
        var formObj = new Form();
        formObj.init(<?php echo ch_json_encode($_smarty_tpl->tpl_vars['item']->value);?>
);
    })
    //添加图片展示
    function show_upload(spec_pic, url){
        $("#"+spec_pic).parent().find('img').attr('src',url);
        $("#"+spec_pic).parent().find('img').show();
        $("#"+spec_pic).parent().find('a').attr('href',url);
        $("#"+spec_pic).parent().find('[type="hidden"]').val(url);
    }
<?php echo '</script'; ?>
>
</body>
</html><?php }} ?>
