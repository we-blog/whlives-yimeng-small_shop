<?php /* Smarty version Smarty-3.1.21-dev, created on 2019-08-13 23:11:40
         compiled from "D:\www\whlives-yimeng-master\views\manager\order\order\list.html" */ ?>
<?php /*%%SmartyHeaderCode:4125d52d32cda33e0-72014077%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'ae40c0cfea5901e216b431f05b9d11cca4e740eb' => 
    array (
      0 => 'D:\\www\\whlives-yimeng-master\\views\\manager\\order\\order\\list.html',
      1 => 1533788760,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '4125d52d32cda33e0-72014077',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'shop_list' => 0,
    'k' => 0,
    'search_where' => 0,
    'list' => 0,
    'key' => 0,
    'payment_list' => 0,
    'delivery_list' => 0,
    'page_count' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5d52d32d09f3e0_11277927',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5d52d32d09f3e0_11277927')) {function content_5d52d32d09f3e0_11277927($_smarty_tpl) {?><!DOCTYPE HTML>
<html>
<head>
	<meta charset="utf-8">
	<meta name="renderer" content="webkit|ie-comp|ie-stand">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
	<meta http-equiv="Cache-Control" content="no-siteapp" />
	<title><?php echo config_item('manager_title');?>
</title>
	<link href="/public/H-ui/css/H-ui.min.css" rel="stylesheet" type="text/css" />
	<link href="/public/H-ui/css/H-ui.admin.css" rel="stylesheet" type="text/css" />
	<link href="/public/H-ui/hui-iconfont/iconfont.css" rel="stylesheet" type="text/css">
</head>
<body>
<nav class="breadcrumb">
	<i class="Hui-iconfont">&#xe67f;</i> 首页 <span class="c-gray en">&gt;</span> 订单管理 <span class="c-gray en">&gt;</span> 订单列表 <a class="btn btn-success radius r mr-20" style="line-height:1.6em;margin-top:3px" href="javascript:location.replace(location.href);" title="刷新" ><i class="Hui-iconfont">&#xe68f;</i></a>
</nav>
<div class="pd-20" style="padding-top: 0px;">
	<div class="text-c pt-20">
		<form action="<?php echo site_url('/manager/order/order/');?>
" method="post" class="form form-horizontal" id="search" name="search">
			<span class="select-box" style="width:150px">
				<select name="shop_id" class="select">
					<option value="">来源店铺</option>
					<?php  $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['k']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['shop_list']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['k']->key => $_smarty_tpl->tpl_vars['k']->value) {
$_smarty_tpl->tpl_vars['k']->_loop = true;
?>
					<option value="<?php echo $_smarty_tpl->tpl_vars['k']->value['m_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['k']->value['shop_name'];?>
</option>
					<?php } ?>
				</select>
			</span>
			<span class="select-box" style="width:150px">
				<select name="payment_status" class="select">
					<option value="">支付状态</option>
					<option value="0">未支付</option>
					<option value="1">已支付</option>
				</select>
			</span>
			<span class="select-box" style="width:150px">
				<select name="status" class="select">
					<option value="">状态</option>
					<option value="1">待支付</option>
					<option value="2">待发货</option>
					<option value="3">待确认</option>
					<option value="4">待评价</option>
					<option value="5">已评价</option>
					<option value="6">退款</option>
					<option value="7">部分退款</option>
					<option value="8">交易取消</option>
					<option value="9">订单作废</option>
					<option value="10">退款中</option>
				</select>
			</span>
			<span class="select-box" style="width:150px">
				<select name="keyword_where" class="select">
					<option value="">选择条件</option>
					<option value="o.full_name">收货人姓名</option>
					<option value="o.tel">收货人电话</option>
					<option value="o.order_no">订单号</option>
					<option value="m.username">用户名</option>
					<option value="o.id">订单ID</option>
				</select>
			</span>
			<input type="text" name="keyword" style="width:150px" class="input-text">
			<button class="btn btn-success" type="submit"><i class="Hui-iconfont">&#xe665;</i> 搜索</button>
			<button onclick="window.location.href='<?php echo search_array_to_link($_smarty_tpl->tpl_vars['search_where']->value);?>
&export=1'" class="btn btn-primary" type="button"><i class="Hui-iconfont">&#xe640;</i> 导出</button>
		</form>
	</div>
	<div class="cl pd-5 bg-1 bk-gray mt-20"> <span class="l">
		<?php if ($_smarty_tpl->tpl_vars['search_where']->value['is_del']==1) {?>
		<a href="<?php echo site_url('/manager/order/order/');?>
" class="btn btn-primary radius"><i class="Hui-iconfont">&#xe678;</i> 返回列表</a>
		<a href="javascript:;" onclick="data_del(this,'<?php echo site_url('/manager/order/order/delete/');?>
')" class="btn btn-danger radius"><i class="Hui-iconfont">&#xe60b;</i> 彻底删除</a>
		<a href="javascript:;" onclick="data_del(this,'<?php echo site_url('/manager/order/order/reduction_recycle/');?>
')" class="btn btn-primary radius"><i class="Hui-iconfont">&#xe66b;</i> 还原</a>
		<?php } else { ?>
		<a href="javascript:;" onclick="batch_print_distribution();" class="btn btn-primary radius"><i class="Hui-iconfont">&#xe652;</i> 批量打印配货单</a>
		<a href="javascript:;" onclick="batch_print_express();" class="btn btn-primary radius"><i class="Hui-iconfont">&#xe652;</i> 批量打印快递单</a>
		<a href="javascript:;" onclick="data_del(this,'<?php echo site_url('/manager/order/order/delete_recycle/');?>
')" class="btn btn-danger radius"><i class="Hui-iconfont">&#xe60b;</i> 批量删除</a>
		<a href="<?php echo site_url('/manager/order/order/');?>
?is_del=1" class="btn btn-primary radius"><i class="Hui-iconfont">&#xe6e2;</i> 回收站</a>
		<?php }?>
	</span></div>
	<div class="mt-20">
		<table class="table table-border table-bordered table-bg table-hover table-sort">
			<thead>
			<tr class="text-c">
				<th width="30"><input name="" type="checkbox" value=""></th>
				<th>订单号</th>
				<th width="70">收货人</th>
				<th width="50">支付状态</th>
				<th width="50">订单状态</th>
				<th width="60">打印</th>
				<th width="75">支付方式</th>
				<th width="75">配送方式</th>
				<th width="70">用户名</th>
				<th width="70">下单时间</th>
				<th width="90">操作</th>
			</tr>
			</thead>
			<tbody>
			<?php  $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['key']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['list']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['key']->key => $_smarty_tpl->tpl_vars['key']->value) {
$_smarty_tpl->tpl_vars['key']->_loop = true;
?>
			<tr class="text-c">
				<td><input name="id[]" type="checkbox" value="<?php echo $_smarty_tpl->tpl_vars['key']->value['id'];?>
"></td>
				<td class="text-l" title="ID:<?php echo $_smarty_tpl->tpl_vars['key']->value['id'];?>
"><?php echo $_smarty_tpl->tpl_vars['key']->value['order_no'];?>
</td>
				<td class="text-l"><?php echo $_smarty_tpl->tpl_vars['key']->value['full_name'];?>
</td>
				<td class="text-c">
					<?php if ($_smarty_tpl->tpl_vars['key']->value['payment_status']==0) {?>
					<span class="label label-warning radius">未支付</span>
					<?php } elseif ($_smarty_tpl->tpl_vars['key']->value['payment_status']==1) {?>
					<span class="label label-success radius">已支付</span>
					<?php }?>
				</td>
				<td class="text-c"><?php echo get_order_status_text($_smarty_tpl->tpl_vars['key']->value);?>
</td>
				<td class="text-c">
					<span class="label label-warning radius"><a target="_blank" style="color: #fff;" href="<?php echo site_url("/manager/order/order/print_distribution/");?>
?order_id=<?php echo $_smarty_tpl->tpl_vars['key']->value['id'];?>
">配货单</a></span>
					<span class="label label-success radius" style="cursor: pointer;" onClick="open_iframe('选择快递公司','<?php echo site_url("/manager/order/order/print_express_select/");?>
?order_id=<?php echo $_smarty_tpl->tpl_vars['key']->value['id'];?>
')">快递单</span>
				</td>
				<td class="text-l">
					<?php if ($_smarty_tpl->tpl_vars['key']->value['payment_id']==1) {?>
					货到付款
					<?php } else { ?>
					<?php echo $_smarty_tpl->tpl_vars['payment_list']->value[$_smarty_tpl->tpl_vars['key']->value['payment_id']]['name'];?>

					<?php }?></td>
				<td class="text-l"><?php echo $_smarty_tpl->tpl_vars['delivery_list']->value[$_smarty_tpl->tpl_vars['key']->value['delivery_id']]['name'];?>
</td>
				<td class="text-l"><?php echo $_smarty_tpl->tpl_vars['key']->value['username'];?>
</td>
				<td class="text-l"><?php echo date('Y-m-d H:i:s',$_smarty_tpl->tpl_vars['key']->value['addtime']);?>
</td>
				<td class="f-14 td-manage text-l">
					<a style="text-decoration:none" class="ml-5" onClick="open_iframe('订单详情','<?php echo site_url("/manager/order/order/view/".((string)$_smarty_tpl->tpl_vars['key']->value['id']));?>
','100%')" href="javascript:;" title="订单详情"><i class="Hui-iconfont">&#xe665;</i></a>
					<?php if ($_smarty_tpl->tpl_vars['key']->value['status']==1) {?>
					<a style="text-decoration:none" class="ml-5" onClick="open_iframe('编辑改价','<?php echo site_url("/manager/order/order/edit/".((string)$_smarty_tpl->tpl_vars['key']->value['id']));?>
')" href="javascript:;" title="编辑改价"><i class="Hui-iconfont">&#xe6df;</i></a>
					<?php }?>
					<?php if ($_smarty_tpl->tpl_vars['search_where']->value['is_del']==1) {?>
					<a style="text-decoration:none" class="ml-5" onClick="data_del(this,'<?php echo site_url('/manager/order/order/delete/');?>
','<?php echo $_smarty_tpl->tpl_vars['key']->value['id'];?>
')" href="javascript:;" title="彻底删除"><i class="Hui-iconfont">&#xe6e2;</i></a>
					<?php } else { ?>
					<a style="text-decoration:none" class="ml-5" onClick="data_del(this,'<?php echo site_url('/manager/order/order/delete_recycle/');?>
','<?php echo $_smarty_tpl->tpl_vars['key']->value['id'];?>
')" href="javascript:;" title="删除到回收站"><i class="Hui-iconfont">&#xe6e2;</i></a>
					<?php }?>
					<?php if ($_smarty_tpl->tpl_vars['key']->value['shop_id']>1) {?>
					<a style="text-decoration:none" class="ml-5" onClick="open_iframe('查看商家','<?php echo site_url("/manager/member/shop/view/".((string)$_smarty_tpl->tpl_vars['key']->value['shop_id']));?>
','100%')" href="javascript:;" title="查看商家"><i class="Hui-iconfont">&#xe66a;</i></a>
					<?php }?>
				</td>
			</tr>
			<?php } ?>
			</tbody>
		</table>
	</div>
</div>
<!--分页-->
<?php echo page_view('page',$_smarty_tpl->tpl_vars['page_count']->value,search_array_to_link($_smarty_tpl->tpl_vars['search_where']->value));?>

<?php echo '<script'; ?>
 type="text/javascript" src="/public/js/jquery.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="/public/js/layer/layer.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="/public/js/jquery.dataTables.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="/public/js/form.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="/public/H-ui/js/H-ui.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="/public/H-ui/js/H-ui.admin.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 language="JavaScript">
	//批量打印配货单
	function batch_print_distribution(){
		var ids = new Array();
		$('[name="id[]"]:checked').each(
			function(i){
				//获取多选框的值
				var idVal = $('[name="id[]"]:checked:eq('+i+')').val();
				ids.push(idVal);
			}
		)
		if(ids==''){
			layer.msg('没有选择任何数据!');
			return false;
		}
		window.open('<?php echo site_url("/manager/order/order/print_distribution/");?>
?order_id='+ids.join(','));
	}
	//批量打印快递单
	function batch_print_express(){
		var ids = new Array();
		$('[name="id[]"]:checked').each(
			function(i){
				//获取多选框的值
				var idVal = $('[name="id[]"]:checked:eq('+i+')').val();
				ids.push(idVal);
			}
		)
		open_iframe('订单详情','<?php echo site_url("/manager/order/order/print_express_select/");?>
?order_id='+ids.join(','))
	}
	$(function(){
		//表单回填
		var formObj = new Form();
		formObj.init(<?php echo ch_json_encode($_smarty_tpl->tpl_vars['search_where']->value);?>
);
	})
<?php echo '</script'; ?>
>
</body>
</html><?php }} ?>
