<?php /* Smarty version Smarty-3.1.21-dev, created on 2019-08-13 23:03:04
         compiled from "D:\www\whlives-yimeng-master\views\manager\index\main.html" */ ?>
<?php /*%%SmartyHeaderCode:315705d52d128a3e009-70918547%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '85e0d362a6b2da30d0e569488be0618dc0fb55ae' => 
    array (
      0 => 'D:\\www\\whlives-yimeng-master\\views\\manager\\index\\main.html',
      1 => 1533788760,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '315705d52d128a3e009-70918547',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'admin_data' => 0,
    'is_install' => 0,
    'select_buy' => 0,
    'php_version' => 0,
    'server_soft' => 0,
    'file_size' => 0,
    'http_host' => 0,
    'local_version' => 0,
    'version' => 0,
    'count' => 0,
    'list' => 0,
    'key' => 0,
    'delivery_list' => 0,
    'payment_list' => 0,
    'search_where' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5d52d128cb2f12_45161738',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5d52d128cb2f12_45161738')) {function content_5d52d128cb2f12_45161738($_smarty_tpl) {?><!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
    <meta http-equiv="Cache-Control" content="no-siteapp" />
    <title><?php echo config_item('manager_title');?>
</title>
    <link href="/public/H-ui/css/H-ui.min.css" rel="stylesheet" type="text/css" />
    <link href="/public/H-ui/css/H-ui.admin.css" rel="stylesheet" type="text/css" />
    <link href="/public/H-ui/hui-iconfont/iconfont.css" rel="stylesheet" type="text/css">
</head>
<body>
<div class="pd-20" style="padding-top:20px;">
    <p class="f-20 text-success">欢迎<?php echo $_smarty_tpl->tpl_vars['admin_data']->value['username'];?>
</p>
    <?php if ($_smarty_tpl->tpl_vars['is_install']->value==1) {?><p class="f-14 text-success c-red">你的install目录还没有删除,有安全隐患,请尽快删除</p><?php }?>
    <p class="f-14 text-success c-red"><?php if ($_smarty_tpl->tpl_vars['select_buy']->value['status']=='n') {
echo $_smarty_tpl->tpl_vars['select_buy']->value['info'];
}?></p>
    <table style="width: 49%; float: left;" class="table table-border table-bordered table-bg">
        <thead>
        <tr>
            <th colspan="2" scope="col">系统信息</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td width="100">PHP版本 </td>
            <td><?php echo $_smarty_tpl->tpl_vars['php_version']->value;?>
</td>
        </tr>
        <tr>
            <td>服务器解析引擎 </td>
            <td><?php echo $_smarty_tpl->tpl_vars['server_soft']->value;?>
</td>
        </tr>
        <tr>
            <td>服务器上传限制</td>
            <td><?php echo $_smarty_tpl->tpl_vars['file_size']->value;?>
</td>
        </tr>
        <tr>
            <td>服务器当前时间 </td>
            <td><?php echo date('Y-m-d H:i:s',time());?>
</td>
        </tr>
        <tr>
            <td>服务器域名</td>
            <td><?php echo $_smarty_tpl->tpl_vars['http_host']->value;?>
</td>
        </tr>
        <tr>
            <td>当前版本</td>
            <td><?php echo $_smarty_tpl->tpl_vars['local_version']->value;?>
 最新版本<?php echo $_smarty_tpl->tpl_vars['version']->value;?>
 (<a href="http://www.smallshop.me/article/22.html" target="_blank">更新日志</a>)</td>
        </tr>
        <tr>
            <td>联系我们</td>
            <td><a href="http://www.smallshop.me" target="_blank">官网</a> <a href="http://wpa.qq.com/msgrd?v=3&uin=87127173&site=qq&menu=yes" target="_blank">技术支持</a></td>
        </tr>
        <tr>
            <td>是否商业授权</td>
            <td><?php if ($_smarty_tpl->tpl_vars['select_buy']->value['status']=='y') {
echo $_smarty_tpl->tpl_vars['select_buy']->value['info'];
} else { ?>未授权<?php }?></td>
        </tr>
        </tbody>
    </table>
    <table style="width: 49%; float: right;" class="table table-border table-bordered table-bg">
        <thead>
        <tr>
            <th colspan="2" scope="col">基础统计</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td width="100">商家数量</td>
            <td><a href="<?php echo site_url('/manager/member/shop/index');?>
"><?php echo $_smarty_tpl->tpl_vars['count']->value['member_shop']-1;?>
家</a></td>
        </tr>
        <tr>
            <td>注册用户</td>
            <td><a href="<?php echo site_url('/manager/member/user/index');?>
"><?php echo $_smarty_tpl->tpl_vars['count']->value['member']-1;?>
个</a></td>
        </tr>
        <tr>
            <td>产品数量</td>
            <td><a href="<?php echo site_url('/manager/goods/goods/index');?>
"><?php echo $_smarty_tpl->tpl_vars['count']->value['goods'];?>
个</a></td>
        </tr>
        <tr>
            <td>待审商品</td>
            <td><a href="<?php echo site_url('/manager/goods/goods/index?status=3');?>
"><?php echo $_smarty_tpl->tpl_vars['count']->value['examine_goods'];?>
个</a></td>
        </tr>
        <tr>
            <td>库存预警</td>
            <td><a href="<?php echo site_url('/manager/goods/goods/index?goods_store_nums=1');?>
"><?php echo $_smarty_tpl->tpl_vars['count']->value['goods_store_nums'];?>
个</a></td>
        </tr>
        <tr>
            <td>订单数量</td>
            <td><a href="<?php echo site_url('/manager/order/order/index');?>
"><?php echo $_smarty_tpl->tpl_vars['count']->value['order'];?>
个</a></td>
        </tr>
        <tr>
            <td>未发货订单</td>
            <td><a href="<?php echo site_url('/manager/order/order/index?status=2');?>
"><?php echo $_smarty_tpl->tpl_vars['count']->value['send_order'];?>
个</a></td>
        </tr>
        <tr>
            <td>退款申请</td>
            <td><a href="<?php echo site_url('/manager/order/refund_doc/pending');?>
"><?php echo $_smarty_tpl->tpl_vars['count']->value['order_refund_doc'];?>
个</a></td>
        </tr>
        </tbody>
    </table>
    <table class="tabl table-bg"></table>
    <table class="table table-border table-bordered table-bg table-hover table-sort mt-20">
        <thead>
        <tr>
            <th colspan="10" scope="col">最新订单</th>
        </tr>
        </thead>
        <thead>
        <tr class="text-c">
            <th>订单号</th>
            <th width="80">收货人</th>
            <th width="50">支付状态</th>
            <th width="60">订单状态</th>
            <th width="110">打印</th>
            <th width="70">配送方式</th>
            <th width="70">支付方式</th>
            <th width="70">用户名</th>
            <th width="70">下单时间</th>
            <th width="80">操作</th>
        </tr>
        </thead>
        <tbody>
        <?php  $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['key']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['list']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['key']->key => $_smarty_tpl->tpl_vars['key']->value) {
$_smarty_tpl->tpl_vars['key']->_loop = true;
?>
        <tr class="text-c">
            <td class="text-l"><?php echo $_smarty_tpl->tpl_vars['key']->value['order_no'];?>
</td>
            <td class="text-l"><?php echo $_smarty_tpl->tpl_vars['key']->value['full_name'];?>
</td>
            <td class="text-c">
                <?php if ($_smarty_tpl->tpl_vars['key']->value['payment_status']==0) {?>
                <span class="label label-warning radius">未支付</span>
                <?php } elseif ($_smarty_tpl->tpl_vars['key']->value['payment_status']==1) {?>
                <span class="label label-success radius">已支付</span>
                <?php }?>
            </td>
            <td class="text-c"><?php echo get_order_status_text($_smarty_tpl->tpl_vars['key']->value);?>
</td>
            <td class="text-c">
                <span class="label label-warning radius"><a target="_blank" style="color: #fff;" href="<?php echo site_url("/manager/order/order/shop_templet/".((string)$_smarty_tpl->tpl_vars['key']->value['id']));?>
">配货单</a></span>
                <span class="label label-success radius" style="cursor: pointer;" onClick="open_iframe('选择快递公司','<?php echo site_url("/manager/order/order/express_templet_select/".((string)$_smarty_tpl->tpl_vars['key']->value['id']));?>
')">快递单</span>
            </td>
            <td class="text-l"><?php echo $_smarty_tpl->tpl_vars['delivery_list']->value[$_smarty_tpl->tpl_vars['key']->value['delivery_id']]['name'];?>
</td>
            <td class="text-l">
                <?php if ($_smarty_tpl->tpl_vars['key']->value['payment_id']==1) {?>
                货到付款
                <?php } else { ?>
                <?php echo $_smarty_tpl->tpl_vars['payment_list']->value[$_smarty_tpl->tpl_vars['key']->value['payment_id']]['name'];?>

                <?php }?>
            </td>
            <td class="text-l"><?php echo $_smarty_tpl->tpl_vars['key']->value['username'];?>
</td>
            <td class="text-l"><?php echo date('Y-m-d H:i:s',$_smarty_tpl->tpl_vars['key']->value['addtime']);?>
</td>
            <td class="f-14 td-manage">
                <a style="text-decoration:none" class="ml-5" onClick="open_iframe('订单详情','<?php echo site_url("/manager/order/order/view/".((string)$_smarty_tpl->tpl_vars['key']->value['id']));?>
','100%')" href="javascript:;" title="订单详情"><i class="Hui-iconfont">&#xe665;</i></a>
                <?php if ($_smarty_tpl->tpl_vars['key']->value['status']==1) {?>
                <a style="text-decoration:none" class="ml-5" onClick="open_iframe('编辑改价','<?php echo site_url("/manager/order/order/edit/".((string)$_smarty_tpl->tpl_vars['key']->value['id']));?>
')" href="javascript:;" title="编辑改价"><i class="Hui-iconfont">&#xe6df;</i></a>
                <?php }?>
                <?php if ($_smarty_tpl->tpl_vars['search_where']->value['is_del']==1) {?>
                <a style="text-decoration:none" class="ml-5" onClick="data_del(this,'<?php echo site_url('/manager/order/order/delete/');?>
','<?php echo $_smarty_tpl->tpl_vars['key']->value['id'];?>
')" href="javascript:;" title="彻底删除"><i class="Hui-iconfont">&#xe6e2;</i></a>
                <?php } else { ?>
                <a style="text-decoration:none" class="ml-5" onClick="data_del(this,'<?php echo site_url('/manager/order/order/delete_recycle/');?>
','<?php echo $_smarty_tpl->tpl_vars['key']->value['id'];?>
')" href="javascript:;" title="删除到回收站"><i class="Hui-iconfont">&#xe6e2;</i></a>
                <?php }?>
            </td>
        </tr>
        <?php } ?>
        </tbody>
    </table>
</div>
<?php echo '<script'; ?>
 type="text/javascript" src="/public/js/jquery.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="/public/js/layer/layer.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="/public/js/jquery.dataTables.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="/public/js/form.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="/public/H-ui/js/H-ui.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="/public/H-ui/js/H-ui.admin.js"><?php echo '</script'; ?>
>
</body>
</html><?php }} ?>
