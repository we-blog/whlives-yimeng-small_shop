<?php /* Smarty version Smarty-3.1.21-dev, created on 2019-08-13 23:17:17
         compiled from "D:\www\whlives-yimeng-master\views\mobile\category\index.html" */ ?>
<?php /*%%SmartyHeaderCode:5295d52d47d8aeb77-45963262%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'eb46a0ecea0a5bd6d6a5e2f059e0090e5cd5b1df' => 
    array (
      0 => 'D:\\www\\whlives-yimeng-master\\views\\mobile\\category\\index.html',
      1 => 1533788760,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '5295d52d47d8aeb77-45963262',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5d52d47d962693_83819313',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5d52d47d962693_83819313')) {function content_5d52d47d962693_83819313($_smarty_tpl) {?><!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title><?php echo config_item('website_title');?>
</title>
    <meta content="initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport">
    <meta content="yes" name="apple-mobile-web-app-capable">
    <meta content="black" name="apple-mobile-web-app-status-bar-style">
    <meta content="telephone=no" name="format-detection">
    <link rel="stylesheet" href="/views/mobile/skin/css/swiper.min.css">
    <link rel="stylesheet" href="/views/mobile/skin/css/style.css">
</head>
<body>
<!-- search start -->
<section class="searchwrap flexbox">
    <div class="logbox flexbox">
        <img src="/views/mobile/skin/images/logo.png">
    </div>
    <div class="searchbox flexbox">
        <input type="text" class="searchstyle" placeholder="搜索商品">
        <div class="searchbtnbox">
            <img src="/views/mobile/skin/images/search_btn.png">
        </div>
    </div>
</section>
<!-- search end -->
<section class="contentbox ">
    <!-- leftnav start -->
    <aside class="nav" id="left_category_list">
        <a href="javascript:void(0)" data-value="flag" class="nav_a activea">推荐</a>
    </aside>
    <!--左侧分类模板start-->
    <?php echo '<script'; ?>
 type='text/html' id='left_category_list_Template'>
        {{each result}}
        <a href="javascript:void(0)" data-value="{{$value.id}}" class="nav_a">{{$value.name}}</a>
        {{/each}}
    <?php echo '</script'; ?>
>
    <!--左侧分类模板end-->
    <!-- leftnav end -->
    <!--分类列表 start  -->
    <section class="prolist" id="category_list">
    </section>
    <!--分类模板start-->
    <?php echo '<script'; ?>
 type='text/html' id='category_list_Template'>
        <dl>
        {{each result}}
        {{if $value.down != ''}}
            <dt>{{$value.name}}</dt>
            <dd>
                {{each $value.down as v}}
                <a href="<?php ob_start();
echo get_web_type();
$_tmp1=ob_get_clean();?><?php echo site_url("/".$_tmp1."/goods/goods_list?cat_id=");?>
{{v.id}}" class="gridsitem_a2">
                    <div class="sortImgwrap">
                        <img src="{{v.image}}">
                    </div>
                    <p class="gridsitem_label">{{v.name}}</p>
                </a>
                {{/each}}
            </dd>
        </dl><dl>
        {{else}}
            <a href="<?php ob_start();
echo get_web_type();
$_tmp2=ob_get_clean();?><?php echo site_url("/".$_tmp2."/goods/goods_list?cat_id=");?>
{{$value.id}}" class="gridsitem_a2">
                <div class="sortImgwrap">
                    <img src="{{$value.image}}">
                </div>
                <p class="gridsitem_label">{{$value.name}}</p>
            </a>
            {{if $index+1%3==0}}</dl><dl>{{/if}}
        {{/if}}
        {{/each}}
        </dl>
    <?php echo '</script'; ?>
>
    <!--分类模板end-->
    <!--分类列表 end -->
</section>
<?php echo '<script'; ?>
 type="text/javascript" src="/public/js/jquery.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="/public/js/artTemplate.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="/views/mobile/skin/js/swiper.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="/views/mobile/skin/js/public.js"><?php echo '</script'; ?>
>
<?php echo $_smarty_tpl->getSubTemplate ("mobile/footer.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<?php echo '<script'; ?>
>
    $(function () {
        //左侧分类展示
        category('left_category_list');
        //默认推荐的
        category('category_list', 'flag');

        //切换菜单
        $('#left_category_list').on('click', 'a', function () {
            $('#left_category_list a').removeClass('activea');
            $(this).addClass('activea');
            category('category_list', $(this).attr('data-value'), 1);
        });
    })

    //分类请求
    function category(div_id, cat_id, level) {
        $.ajax({
            type: "POST",
            url: "<?php echo site_url('/api/goods/category/');?>
",
            data: "reid=" + cat_id + '&level=' + level,
            dataType: "json",
            success: function (data) {
                if (data.status == 'y') {
                    var html = template(div_id + '_Template', data);
                    if (div_id == 'left_category_list') {
                        $('#' + div_id).append(html);
                    } else {
                        $('#' + div_id).html(html);
                    }
                }
            }
        });
    }
<?php echo '</script'; ?>
>
<?php echo '<script'; ?>
>
    window.onload = function () {
        // 设置左侧导航栏高度
        var headerHeight = $(".searchwrap").height();
        var footerHeight = $(".navbar").height();
        var navHeight = (window.innerHeight - headerHeight - footerHeight) + 'px';
        // var assortHeight  = (window.innerHeight - headerHeight - footerHeight-20) +'px' ;
        $('.nav').css("height", navHeight);
    }
<?php echo '</script'; ?>
>
<?php if (get_client()=='weixin') {?>
<?php echo $_smarty_tpl->getSubTemplate ("mobile/wechat_share.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<?php echo '<script'; ?>
 language="JavaScript">
    wx.ready(function () {
        var share_title = "<?php echo config_item('website_title');?>
";
        var share_desc = "";
        var share_link = "<?php echo site_url('/<{get_web_type()}>');?>
";
        var share_imgurl = "<?php echo config_item('base_url');
echo config_item('website_logo');?>
";
        wx_share(share_title, share_desc, share_link, share_imgurl);
    });
<?php echo '</script'; ?>
>
<?php }?>
</body>
</html>




<?php }} ?>
