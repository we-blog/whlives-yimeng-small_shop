<?php /* Smarty version Smarty-3.1.21-dev, created on 2019-08-13 23:11:46
         compiled from "D:\www\whlives-yimeng-master\views\manager\order\address\list.html" */ ?>
<?php /*%%SmartyHeaderCode:228175d52d33261eb18-89214114%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'd0fc9a0b1af126284dbcc49dff6f265abaf58fc3' => 
    array (
      0 => 'D:\\www\\whlives-yimeng-master\\views\\manager\\order\\address\\list.html',
      1 => 1533788760,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '228175d52d33261eb18-89214114',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'search_where' => 0,
    'list' => 0,
    'key' => 0,
    'page_count' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5d52d33274b7d1_90036411',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5d52d33274b7d1_90036411')) {function content_5d52d33274b7d1_90036411($_smarty_tpl) {?><!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<meta name="renderer" content="webkit|ie-comp|ie-stand">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
<meta http-equiv="Cache-Control" content="no-siteapp" />
	<title><?php echo config_item('manager_title');?>
</title>
	<link href="/public/H-ui/css/H-ui.min.css" rel="stylesheet" type="text/css" />
	<link href="/public/H-ui/css/H-ui.admin.css" rel="stylesheet" type="text/css" />
	<link href="/public/H-ui/hui-iconfont/iconfont.css" rel="stylesheet" type="text/css">
</head>
<body>
<nav class="breadcrumb">
	<i class="Hui-iconfont">&#xe67f;</i> 首页 <span class="c-gray en">&gt;</span> 订单管理 <span class="c-gray en">&gt;</span> 发货地址管理 <a class="btn btn-success radius r mr-20" style="line-height:1.6em;margin-top:3px" href="javascript:location.replace(location.href);" title="刷新" ><i class="Hui-iconfont">&#xe68f;</i></a>
</nav>
<div class="pd-20" style="padding-top: 0px;">
	<div class="cl pd-5 bg-1 bk-gray mt-20"> <span class="l">
		<?php if ($_smarty_tpl->tpl_vars['search_where']->value['status']==1) {?>
		<a href="<?php echo site_url('/manager/order/address/');?>
" class="btn btn-primary radius"><i class="Hui-iconfont">&#xe678;</i> 返回列表</a>
		<a href="javascript:;" onclick="data_del(this,'<?php echo site_url('/manager/order/address/delete/');?>
','')" class="btn btn-danger radius"><i class="Hui-iconfont">&#xe60b;</i> 彻底删除</a>
		<a href="javascript:;" onclick="data_del(this,'<?php echo site_url('/manager/order/address/reduction_recycle/');?>
','')" class="btn btn-primary radius"><i class="Hui-iconfont">&#xe66b;</i> 还原</a>
		<?php } else { ?>
		<a href="javascript:;" class="btn btn-primary radius" onclick="open_iframe('添加发货地址','<?php echo site_url('/manager/order/address/edit');?>
','100%')"><i class="Hui-iconfont">&#xe600;</i> 添加发货地址</a>
		<a href="javascript:;" onclick="data_del(this,'<?php echo site_url('/manager/order/address/delete_recycle/');?>
')" class="btn btn-danger radius"><i class="Hui-iconfont">&#xe60b;</i> 批量删除</a>
		<a href="<?php echo site_url('/manager/order/address/');?>
?status=1" class="btn btn-primary radius"><i class="Hui-iconfont">&#xe6e2;</i> 回收站</a>
		<?php }?>
	</span></div>
	<div class="mt-20">
		<table class="table table-border table-bordered table-bg table-hover table-sort">
			<thead>
				<tr class="text-c">
					<th width="40"><input name="" type="checkbox" value=""></th>
					<th>姓名</th>
					<th>电话</th>
					<th>地址</th>
					<th>默认</th>
					<th width="80">操作</th>
				</tr>
			</thead>
			<tbody>
			<?php  $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['key']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['list']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['key']->key => $_smarty_tpl->tpl_vars['key']->value) {
$_smarty_tpl->tpl_vars['key']->_loop = true;
?>
				<tr class="text-c">
					<td><input name="id[]" type="checkbox" value="<?php echo $_smarty_tpl->tpl_vars['key']->value['id'];?>
"></td>
					<td class="text-l"><?php echo $_smarty_tpl->tpl_vars['key']->value['full_name'];?>
</td>
					<td class="text-l"><?php echo $_smarty_tpl->tpl_vars['key']->value['tel'];?>
</td>
					<td class="text-l"><?php echo get_area_name(array($_smarty_tpl->tpl_vars['key']->value['prov'],$_smarty_tpl->tpl_vars['key']->value['city'],$_smarty_tpl->tpl_vars['key']->value['area']));
echo $_smarty_tpl->tpl_vars['key']->value['address'];?>
</td>
					<td>
						<?php if ($_smarty_tpl->tpl_vars['key']->value['is_default']==1) {?>
						<span class="label label-success radius">默认</span>
						<?php } else { ?>
						<span class="label label-warning radius">非默认</span>
						<?php }?>
					</td>
					<td class="f-14 td-manage">
						<a style="text-decoration:none" class="ml-5" onClick="open_iframe('编辑','<?php echo site_url("/manager/order/address/edit/".((string)$_smarty_tpl->tpl_vars['key']->value['id']));?>
','100%')" href="javascript:;" title="编辑"><i class="Hui-iconfont">&#xe6df;</i></a>
						<a style="text-decoration:none" class="ml-5" onClick="data_del(this,'<?php echo site_url('/manager/order/address/delete/');?>
','<?php echo $_smarty_tpl->tpl_vars['key']->value['id'];?>
')" href="javascript:;" title="彻底删除"><i class="Hui-iconfont">&#xe6e2;</i></a>
					</td>
				</tr>
			<?php } ?>
			</tbody>
		</table>
	</div>
</div>
<!--分页-->
<?php echo page_view('page',$_smarty_tpl->tpl_vars['page_count']->value,search_array_to_link($_smarty_tpl->tpl_vars['search_where']->value));?>

<?php echo '<script'; ?>
 type="text/javascript" src="/public/js/jquery.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="/public/js/layer/layer.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="/public/js/jquery.dataTables.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="/public/H-ui/js/H-ui.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="/public/H-ui/js/H-ui.admin.js"><?php echo '</script'; ?>
>
</body>
</html><?php }} ?>
