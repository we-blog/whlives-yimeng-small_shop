<?php /* Smarty version Smarty-3.1.21-dev, created on 2019-08-13 22:54:42
         compiled from "D:\www\whlives-yimeng-master\views\web\index.html" */ ?>
<?php /*%%SmartyHeaderCode:90135d52cf320e08c3-54367234%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '203ac5e1b7c6509c1a2940f9d26842a73759c736' => 
    array (
      0 => 'D:\\www\\whlives-yimeng-master\\views\\web\\index.html',
      1 => 1533788760,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '90135d52cf320e08c3-54367234',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'goods_list' => 0,
    'g' => 0,
    'cat_list' => 0,
    'val' => 0,
    'key' => 0,
    'k' => 0,
    'v' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5d52cf32e2efe0_22200556',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5d52cf32e2efe0_22200556')) {function content_5d52cf32e2efe0_22200556($_smarty_tpl) {?><!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
	<title><?php echo config_item('website_title');?>
</title>
	<meta content="<?php echo config_item('website_keywords');?>
" name="Keywords">
	<meta content="<?php echo config_item('website_desc');?>
" name="description"/>
	<?php echo '<script'; ?>
 type="text/javascript" src="/public/js/jquery.js"><?php echo '</script'; ?>
>
	<link rel="stylesheet" type="text/css" href="/views/web/skin/css/style.css" />
</head>
<body>
	<div class="wrap">
		<?php echo $_smarty_tpl->getSubTemplate ("web/header.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

		<!-- banner -->
		<div class="bannerbox">
			<?php echo show_adv(2);?>

		</div>
		<!-- 广告 -->
		<div class="wrapbox">
			<ul class="aditems clear">
				<?php echo show_adv(3,'<li>','</li>');?>

			</ul>
		</div>
		<!-- 产品列表 -->
		<div class="indexprowrap">
			<div class="indexprobox">
				<div class="wrapbox">
					<div class="indexpro_titlebox clear">
						<div class="indexpro_title">
							<div class="indexpro_T">
								<a href="JavaScript:void(0)">推荐</a>
							</div>
						</div>
					</div>
					<div class="indexpro_listwrap">
						<div class="indexpro_listbox" style="display: block;">
							<ul class="indexpro_list clear">
								<?php $_smarty_tpl->tpl_vars['goods_list'] = new Smarty_variable(ym_goods_list(array('is_flag'=>1),4,1), null, 0);?>
								<?php  $_smarty_tpl->tpl_vars['g'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['g']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['goods_list']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['g']->key => $_smarty_tpl->tpl_vars['g']->value) {
$_smarty_tpl->tpl_vars['g']->_loop = true;
?>
								<li>
									<a href="<?php echo site_url("/goods/product/".((string)$_smarty_tpl->tpl_vars['g']->value['id']));?>
">
									<div class="indexpro_listimg">
										<img src="<?php echo image_thumb($_smarty_tpl->tpl_vars['g']->value['image'],240,240);?>
" width="240" height="240">
									</div>
									<div class="indexpro_listTitlebox">
										<?php echo $_smarty_tpl->tpl_vars['g']->value['name'];?>

									</div>
									<div class="indexpro_listpbox">
										<div class="indexpro_listpleft">
											市场价：<span>￥<?php echo $_smarty_tpl->tpl_vars['g']->value['market_price'];?>
</span>
										</div>
										<div class="indexpro_listpRight">
											销售价：￥<?php echo $_smarty_tpl->tpl_vars['g']->value['sell_price'];?>

										</div>
									</div>
									</a>
								</li>
								<?php } ?>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="indexprobox">
				<div class="wrapbox">
					<div class="indexpro_titlebox clear">
						<div class="indexpro_title">
							<div class="indexpro_T">
								<a href="JavaScript:void(0)">最新</a>
							</div>
						</div>
					</div>
					<div class="indexpro_listwrap">
						<div class="indexpro_listbox" style="display: block;">
							<ul class="indexpro_list clear">
								<?php $_smarty_tpl->tpl_vars['goods_list'] = new Smarty_variable(ym_goods_list(array('is_new'=>1),4,1), null, 0);?>
								<?php  $_smarty_tpl->tpl_vars['g'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['g']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['goods_list']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['g']->key => $_smarty_tpl->tpl_vars['g']->value) {
$_smarty_tpl->tpl_vars['g']->_loop = true;
?>
								<li>
									<a href="<?php echo site_url("/goods/product/".((string)$_smarty_tpl->tpl_vars['g']->value['id']));?>
">
									<div class="indexpro_listimg">
										<img src="<?php echo image_thumb($_smarty_tpl->tpl_vars['g']->value['image'],240,240);?>
" width="240" height="240">
									</div>
									<div class="indexpro_listTitlebox">
										<?php echo $_smarty_tpl->tpl_vars['g']->value['name'];?>

									</div>
									<div class="indexpro_listpbox">
										<div class="indexpro_listpleft">
											市场价：<span>￥<?php echo $_smarty_tpl->tpl_vars['g']->value['market_price'];?>
</span>
										</div>
										<div class="indexpro_listpRight">
											销售价：￥<?php echo $_smarty_tpl->tpl_vars['g']->value['sell_price'];?>

										</div>
									</div>
									</a>
								</li>
								<?php } ?>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="indexprobox">
				<div class="wrapbox">
					<div class="indexpro_titlebox clear">
						<div class="indexpro_title">
							<div class="indexpro_T">
								<a href="JavaScript:void(0)">最热</a>
							</div>
						</div>
					</div>
					<div class="indexpro_listwrap">
						<div class="indexpro_listbox" style="display: block;">
							<ul class="indexpro_list clear">
								<?php $_smarty_tpl->tpl_vars['goods_list'] = new Smarty_variable(ym_goods_list(array('is_hot'=>1),4,1), null, 0);?>
								<?php  $_smarty_tpl->tpl_vars['g'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['g']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['goods_list']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['g']->key => $_smarty_tpl->tpl_vars['g']->value) {
$_smarty_tpl->tpl_vars['g']->_loop = true;
?>
								<li>
									<a href="<?php echo site_url("/goods/product/".((string)$_smarty_tpl->tpl_vars['g']->value['id']));?>
">
									<div class="indexpro_listimg">
										<img src="<?php echo image_thumb($_smarty_tpl->tpl_vars['g']->value['image'],240,240);?>
" width="240" height="240">
									</div>
									<div class="indexpro_listTitlebox">
										<?php echo $_smarty_tpl->tpl_vars['g']->value['name'];?>

									</div>
									<div class="indexpro_listpbox">
										<div class="indexpro_listpleft">
											市场价：<span>￥<?php echo $_smarty_tpl->tpl_vars['g']->value['market_price'];?>
</span>
										</div>
										<div class="indexpro_listpRight">
											销售价：￥<?php echo $_smarty_tpl->tpl_vars['g']->value['sell_price'];?>

										</div>
									</div>
									</a>
								</li>
								<?php } ?>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<!-- 单层 -->
			<?php $_smarty_tpl->tpl_vars['cat_list'] = new Smarty_variable(ym_list('goods_category',array('where'=>array('reid'=>0)),10,1,'sortnum asc,id asc'), null, 0);?>
			<?php  $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['key']->_loop = false;
 $_smarty_tpl->tpl_vars['val'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['cat_list']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['key']->key => $_smarty_tpl->tpl_vars['key']->value) {
$_smarty_tpl->tpl_vars['key']->_loop = true;
 $_smarty_tpl->tpl_vars['val']->value = $_smarty_tpl->tpl_vars['key']->key;
?>
			<div class="indexprobox">
				<div class="wrapbox">
					<div class="indexpro_titlebox clear">
						<div class="indexpro_title">
							<div class="indexpro_icon">
								<?php echo $_smarty_tpl->tpl_vars['val']->value+1;?>
F
							</div>
							<div class="indexpro_T">
								<a href="<?php echo site_url("/goods/goods_list?cat_id=".((string)$_smarty_tpl->tpl_vars['key']->value['id']));?>
"><?php echo $_smarty_tpl->tpl_vars['key']->value['name'];?>
</a>
							</div>
						</div>
						<div class="indexpro_navbox">
							<ul class="indexpro_navitem">
								<?php $_smarty_tpl->tpl_vars['cat_list'] = new Smarty_variable(ym_list('goods_category',array('where'=>array('reid'=>$_smarty_tpl->tpl_vars['key']->value['id'])),10,1,'sortnum asc,id asc'), null, 0);?>
								<?php  $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['k']->_loop = false;
 $_smarty_tpl->tpl_vars['v'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['cat_list']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['k']->key => $_smarty_tpl->tpl_vars['k']->value) {
$_smarty_tpl->tpl_vars['k']->_loop = true;
 $_smarty_tpl->tpl_vars['v']->value = $_smarty_tpl->tpl_vars['k']->key;
?>
								<li data-class="<?php echo $_smarty_tpl->tpl_vars['val']->value+1;?>
"><a href="<?php echo site_url("/goods/goods_list?cat_id=".((string)$_smarty_tpl->tpl_vars['k']->value['id']));?>
"><?php echo $_smarty_tpl->tpl_vars['k']->value['name'];?>
</a></li>
								<?php } ?>
							</ul>
						</div>
					</div>
					<div class="indexpro_listwrap">
						<div class="indexpro_listbox" id="<?php echo $_smarty_tpl->tpl_vars['val']->value+1;
echo $_smarty_tpl->tpl_vars['v']->value;?>
" style="display: block;">
							<ul class="indexpro_list clear">
								<?php $_smarty_tpl->tpl_vars['goods_list'] = new Smarty_variable(ym_goods_list(array('cat_id'=>$_smarty_tpl->tpl_vars['key']->value['id']),4,1), null, 0);?>
								<?php  $_smarty_tpl->tpl_vars['g'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['g']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['goods_list']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['g']->key => $_smarty_tpl->tpl_vars['g']->value) {
$_smarty_tpl->tpl_vars['g']->_loop = true;
?>
								<li>
									<a href="<?php echo site_url("/goods/product/".((string)$_smarty_tpl->tpl_vars['g']->value['id']));?>
">
										<div class="indexpro_listimg">
											<img src="<?php echo image_thumb($_smarty_tpl->tpl_vars['g']->value['image'],240,240);?>
" width="240" height="240">
										</div>
										<div class="indexpro_listTitlebox">
											<?php echo $_smarty_tpl->tpl_vars['g']->value['name'];?>

										</div>
										<div class="indexpro_listpbox">
											<div class="indexpro_listpleft">
												市场价：<span>￥<?php echo $_smarty_tpl->tpl_vars['g']->value['market_price'];?>
</span>
											</div>
											<div class="indexpro_listpRight">
												销售价：￥<?php echo $_smarty_tpl->tpl_vars['g']->value['sell_price'];?>

											</div>
										</div>
									</a>
								</li>
								<?php } ?>
							</ul>
						</div>
					</div>

				</div>
			</div>
			<?php } ?>
		</div>
		<?php echo $_smarty_tpl->getSubTemplate ("web/footer.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

	</div>
</body>
</html><?php }} ?>
