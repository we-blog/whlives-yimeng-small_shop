<?php /* Smarty version Smarty-3.1.21-dev, created on 2019-08-13 22:54:42
         compiled from "D:\www\whlives-yimeng-master\views\web\header.html" */ ?>
<?php /*%%SmartyHeaderCode:211445d52cf32e2b168-46032096%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '984396acc49170526f2a64fcbf9211e2aa50aceb' => 
    array (
      0 => 'D:\\www\\whlives-yimeng-master\\views\\web\\header.html',
      1 => 1533788760,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '211445d52cf32e2b168-46032096',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'redirect_url' => 0,
    'login_member' => 0,
    'cat_list' => 0,
    'key' => 0,
    'cat_list_down' => 0,
    'k' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5d52cf3352e656_00770963',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5d52cf3352e656_00770963')) {function content_5d52cf3352e656_00770963($_smarty_tpl) {?><!-- top -->
<div class="topwrap">
	<div class="wrapbox clear">
		<div class="topL">
			您好！欢迎光临<?php echo config_item('website_title');?>
!
			<?php if (get_m_id()=='') {?>
			<a href="<?php ob_start();
echo urlencode($_smarty_tpl->tpl_vars['redirect_url']->value);
$_tmp1=ob_get_clean();?><?php echo site_url("/welcome/login?redirect_url=".$_tmp1);?>
">
				[登录]
			</a>
			<a href="<?php ob_start();
echo urlencode($_smarty_tpl->tpl_vars['redirect_url']->value);
$_tmp2=ob_get_clean();?><?php echo site_url("/welcome/reg?redirect_url=".$_tmp2);?>
">
				[免费注册]
			</a>
			<?php } else { ?>
			<?php $_smarty_tpl->tpl_vars['login_member'] = new Smarty_variable(get_member_data(get_m_id()), null, 0);?>
			<?php echo $_smarty_tpl->tpl_vars['login_member']->value['username'];?>

			<a href="<?php echo site_url("/welcome/loginout");?>
">
			[退出]
			</a>
			<?php }?>
		</div>
		<div class="topR">
			<a href="<?php echo site_url("/member");?>
" class="top_a">
				会员中心
			</a>
			<a href="<?php echo site_url("/member/order");?>
" class="top_a">
				我的订单
			</a>
			<a href="javascript:void(0)" class="top_a">
				客服电话：<span class="tel">400-888-8888</span>
			</a>
		</div>
	</div>
</div>
<!-- search -->
<div class="wrapbox">
	<div class="searchwrap clear">
		<div class="searchwrapL">
			<a href="<?php echo site_url("/welcome");?>
"><img src="/views/web/skin/images/logo.png"></a>
		</div>
		<div class="searchwrapM">
			<form name="search" id="search" method="post" action="<?php echo site_url("/goods/goods_list");?>
" onsubmit="return key_search();" style="width:100%;">
			<div class="searchbox">
				<div class="searchboxL">
					<input type="text" class="searchinput" name="keyword" placeholder="搜索关键字">
				</div>
				<div class="searchboxR">
					<input type="submit" value="搜索" class="searchbtn">
				</div>
			</div>
			</form>
			<?php echo '<script'; ?>
 language="JavaScript">
				function key_search(){
					var keyword = $('#search [name="keyword"]').val();
					if (keyword=='') {
						layer.open({content: '关键字不能为空',time: 1});
						return false;
					} else {
						$('#search').submit();
					}
				}
			<?php echo '</script'; ?>
>
			<div class="keywordwrap">
				<p class="keyword">关键词：</p>
				<ul class="keyworditem">
					<?php echo show_adv(5,'<li>','</li>');?>

				</ul>
			</div>
		</div>
		<div class="searchwrapR">
			<div class="searchcart">
				<div class="searchcart_dot" id="my_cart_count">0</div>
				<a href="<?php echo site_url("/cart");?>
">去购物车结算</a>
			</div>
		</div>
	</div>
</div>
<!-- nav -->
<div class="navwrap">
	<div id="navmenuwrap" class="wrapbox" onmouseover="$('.wrapbox').addClass('on')" onmouseout="$('.wrapbox').removeClass('on')">
		<div class="navmenu">
			全部商品分类
		</div>
		<div class="navsortbox">
			<div id ="navsort" >
				<?php $_smarty_tpl->tpl_vars['cat_list'] = new Smarty_variable(ym_list('goods_category',array('where'=>array('reid'=>0)),10,1,'sortnum asc,id asc'), null, 0);?>
				<?php  $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['key']->_loop = false;
 $_smarty_tpl->tpl_vars['val'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['cat_list']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['key']->key => $_smarty_tpl->tpl_vars['key']->value) {
$_smarty_tpl->tpl_vars['key']->_loop = true;
 $_smarty_tpl->tpl_vars['val']->value = $_smarty_tpl->tpl_vars['key']->key;
?>
				<div class="sortitem" onmouseover="this.className='sortitem on'" onmouseleave="this.className='sortitem'">
					<a href="<?php echo site_url("/goods/goods_list?cat_id=".((string)$_smarty_tpl->tpl_vars['key']->value['id']));?>
" class="sort_a"><?php echo $_smarty_tpl->tpl_vars['key']->value['name'];?>
</a>
					<div class="sortlist">
						<ul class="sortlistitem">
							<?php $_smarty_tpl->tpl_vars['cat_list_down'] = new Smarty_variable(ym_list('goods_category',array('where'=>array('reid'=>$_smarty_tpl->tpl_vars['key']->value['id'])),'','','sortnum asc,id asc'), null, 0);?>
							<?php  $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['k']->_loop = false;
 $_smarty_tpl->tpl_vars['v'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['cat_list_down']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['k']->key => $_smarty_tpl->tpl_vars['k']->value) {
$_smarty_tpl->tpl_vars['k']->_loop = true;
 $_smarty_tpl->tpl_vars['v']->value = $_smarty_tpl->tpl_vars['k']->key;
?>
							<li>
								<a href="<?php echo site_url("/goods/goods_list?cat_id=".((string)$_smarty_tpl->tpl_vars['k']->value['id']));?>
">
									<div class="sortlistimg">
										<img src="<?php echo $_smarty_tpl->tpl_vars['k']->value['image'];?>
">
									</div>
									<div class="sortlistText">
										<?php echo $_smarty_tpl->tpl_vars['k']->value['name'];?>

									</div>
								</a>
							</li>
							<?php } ?>
						</ul>
					</div>
				</div>
				<?php } ?>
			</div>
		</div>

	</div>
</div><?php }} ?>
