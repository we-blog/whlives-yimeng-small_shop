<?php /* Smarty version Smarty-3.1.21-dev, created on 2019-08-30 22:14:13
         compiled from "D:\www\whlives-yimeng-master\views\web\user\login.html" */ ?>
<?php /*%%SmartyHeaderCode:295185d692f3507b704-57852197%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '30eb6f02d03289f1a4b8ef27b5ff6ccf1887e62d' => 
    array (
      0 => 'D:\\www\\whlives-yimeng-master\\views\\web\\user\\login.html',
      1 => 1533788760,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '295185d692f3507b704-57852197',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'redirect_url' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5d692f35127536_58077787',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5d692f35127536_58077787')) {function content_5d692f35127536_58077787($_smarty_tpl) {?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title><?php echo config_item('website_title');?>
</title>
    <link rel="stylesheet" type="text/css" href="/views/web/skin/css/style.css"/>
    <?php echo '<script'; ?>
 type="text/javascript" src="/public/js/jquery.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 type="text/javascript" src="/public/js/validform.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 type="text/javascript" src="/public/js/layer/layer.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 type="text/javascript" src="/views/mobile/skin/js/public.js"><?php echo '</script'; ?>
>
</head>
<body>
<div class="wrap">
    <?php echo $_smarty_tpl->getSubTemplate ("web/header.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

    <!-- 内容 -->
    <div class="wrapbox">
        <div class="scrapnav">
            您的当前位置：<a href="<?php echo site_url("/welcome");?>
">首页</a>><a href="<?php echo site_url("/member");?>
">会员中心</a>><a href="javascript:void(0)">登录</a>
        </div>
        <div class="contenwrap">
            <div class="publictitle">
                登录
            </div>
            <div class="loginbox clear">
                <form name="login" id="login" method="post" action="<?php echo site_url('/api/user/login/user_login');?>
">
                    <div class="loginboxleft">
                        <div class="loginboxleft_box clear">
                            <div class="loginleft">
                                手机：
                            </div>
                            <div class="loginright">
                                <input type="text" name="username" class="inputText" placeholder="手机" datatype="m" nullmsg="请输入手机号码" errormsg="手机格式不正确">
                            </div>
                        </div>
                        <div class="loginboxleft_box clear">
                            <div class="loginleft">
                                密码：
                            </div>
                            <div class="loginright">
                                <input type="password" name="password" class="inputText" placeholder="请输入密码" datatype="*" nullmsg="请输入密码">
                            </div>
                        </div>
                        <div class="loginboxleft_box clear">
                            <div class="loginleft">
                            </div>
                            <div class="loginright">
                                <input type="submit" class="inputText login_btn" value="登录">
                            </div>
                        </div>
                        <div class="loginboxleft_box forget">
                            <a href="<?php ob_start();
echo urlencode($_smarty_tpl->tpl_vars['redirect_url']->value);
$_tmp1=ob_get_clean();?><?php echo site_url("/welcome/find_password?redirect_url=".$_tmp1);?>
">忘记密码?</a>
                        </div>
                    </div>
                </form>
                <div class="loginboxright">
                    <div class="loginboxright_img">
                        <img src="/views/web/skin/images/loginright.png">
                    </div>
                    <p>您还不是会员吗？</p>
                    <p> 现在免费<a href="<?php ob_start();
echo urlencode($_smarty_tpl->tpl_vars['redirect_url']->value);
$_tmp2=ob_get_clean();?><?php echo site_url("/welcome/reg?redirect_url=".$_tmp2);?>
">注册会员</a>，不出家门就能享受更多优惠哦~！</p>
                </div>
            </div>
        </div>

    </div>
    <?php echo $_smarty_tpl->getSubTemplate ("web/footer.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

</div>
<?php echo '<script'; ?>
 type="text/javascript" >
    //验证表单
    $(function(){
        $.Tipmsg.r=null;
        $("#login").Validform({
            tiptype:function(msg){
                layer.msg(msg);
            },
            tipSweep:true,
            ajaxPost:true,
            callback:function(data){
                if(data.status=="y"){
                    window.location.href="<?php echo $_smarty_tpl->tpl_vars['redirect_url']->value;?>
";
                }
            }
        });
    })
<?php echo '</script'; ?>
>
</body>
</html><?php }} ?>
