<?php /* Smarty version Smarty-3.1.21-dev, created on 2019-08-13 23:12:29
         compiled from "D:\www\whlives-yimeng-master\views\manager\member\shop\add.html" */ ?>
<?php /*%%SmartyHeaderCode:71225d52d35d42c755-94610033%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '2b562a662f333af4a77241700396bcc9e0c3abf5' => 
    array (
      0 => 'D:\\www\\whlives-yimeng-master\\views\\manager\\member\\shop\\add.html',
      1 => 1533788760,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '71225d52d35d42c755-94610033',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'item' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5d52d35d564fa9_49566169',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5d52d35d564fa9_49566169')) {function content_5d52d35d564fa9_49566169($_smarty_tpl) {?><!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
    <meta http-equiv="Cache-Control" content="no-siteapp" />
    <title><?php echo config_item('manager_title');?>
</title>
    <link href="/public/H-ui/css/H-ui.min.css" rel="stylesheet" type="text/css" />
    <link href="/public/H-ui/css/H-ui.admin.css" rel="stylesheet" type="text/css" />
    <link href="/public/H-ui/hui-iconfont/iconfont.css" rel="stylesheet" type="text/css">
</head>
<body>
<div>
    <form action="<?php echo site_url('/manager/member/shop/save');?>
" method="post" class="form form-horizontal" id="add" enctype="multipart/form-data">
        <div class="row cl">
            <label class="form-label col-2"><span class="c-red">*</span>店铺名称：</label>
            <div class="formControls col-3">
                <input type="text" class="input-text" value="" name="shop_name" datatype="*" nullmsg="请输入店铺名称！">
            </div>
        </div>
        <div class="row cl">
            <label class="form-label col-2"><span class="c-red">*</span>店铺logo：</label>
            <div class="formControls col-3">
                <a href="<?php echo $_smarty_tpl->tpl_vars['item']->value['logo'];?>
" target="_blank"><img src="<?php echo $_smarty_tpl->tpl_vars['item']->value['logo'];?>
" width="50" <?php if ($_smarty_tpl->tpl_vars['item']->value['logo']=='') {?>style="display: none;"<?php }?>></a>
                <input type="hidden" value="" name="logo" datatype="*" nullmsg="请上传店铺logo！">
                <input type="file" name="logo_pic" id="logo_pic" onchange="upload_file(this);" style="width: 150px;">
            </div>
        </div>
        <div class="row cl">
            <label class="form-label col-2"><span class="c-red">*</span>电话：</label>
            <div class="formControls col-3">
                <input type="text" class="input-text" value="" name="tel" datatype="*" nullmsg="请输入电话！">
            </div>
        </div>
        <div class="row cl">
            <label class="form-label col-2"><span class="c-red">*</span>邮箱：</label>
            <div class="formControls col-3">
                <input type="text" class="input-text" value="" name="email" datatype="e" errormsg="邮箱格式错误！">
            </div>
        </div>
        <div class="row cl">
            <label class="form-label col-2">第三方客服链接：</label>
            <div class="formControls col-3">
                <input type="text" class="input-text" value="" name="customer_url" datatype="empty|url" nullmsg="第三方客服链接！">
            </div>
        </div>
        <div class="row cl">
            <label class="form-label col-2"><span class="c-red">*</span>营业执照：</label>
            <div class="formControls col-3">
                <a href="<?php echo $_smarty_tpl->tpl_vars['item']->value['business_license'];?>
" target="_blank"><img src="<?php echo $_smarty_tpl->tpl_vars['item']->value['business_license'];?>
" width="50" <?php if ($_smarty_tpl->tpl_vars['item']->value['business_license']=='') {?>style="display: none;"<?php }?>></a>
                <input type="hidden" value="" name="business_license" datatype="*" nullmsg="请上传营业执照">
                <input type="file" name="business_license_pic" id="business_license_pic" onchange="upload_file(this);" style="width: 150px;">
            </div>
        </div>
        <div class="row cl">
            <label class="form-label col-2"><span class="c-red">*</span>省市区：</label>
            <div class="formControls col-5">
                <span class="select-box" style="width: 31%;">
				<select name="prov" class="select" onchange="area_list('city',this.value);" datatype="*" nullmsg="请选择省！">
                </select>
				</span>
                <span class="select-box" style="width: 31%;">
				<select name="city" class="select" onchange="area_list('area',this.value);" datatype="*" nullmsg="请选择市！">
                    <option>请选择市</option>
                </select>
				</span>
                <span class="select-box" style="width: 31%;">
				<select name="area" class="select" datatype="*" nullmsg="请选择区！">
                    <option>请选择区</option>
                </select>
				</span>
            </div>
        </div>
        <div class="row cl">
            <label class="form-label col-2"><span class="c-red">*</span>地址：</label>
            <div class="formControls col-3">
                <input type="text" class="input-text" value="" name="address" datatype="*" nullmsg="请输入地址！">
            </div>
        </div>
        <div class="row cl">
            <label class="form-label col-2"><span class="c-red">*</span>店铺简介：</label>
            <div class="formControls col-6">
                <textarea name="desc" cols="" rows="" class="textarea"  placeholder="店铺简介" datatype="*1-100" dragonfly="true" nullmsg="店铺简介不能为空！" onKeyUp="textarealength(this,100)"></textarea>
                <p class="textarea-numberbar"><em class="textarea-length">0</em>/100</p>
            </div>
        </div>
        <div class="row cl">
            <div class="col-10 col-offset-2">
                <input type="hidden" name="m_id">
                <button onClick="$('#add').submit();" class="btn btn-primary radius" type="submit"><i class="Hui-iconfont">&#xe632;</i> 保存</button>
                <button onClick="layer_close();" class="btn btn-default radius" type="button">&nbsp;&nbsp;取消&nbsp;&nbsp;</button>
            </div>
        </div>
    </form>
</div>
</div>
<?php echo '<script'; ?>
 type="text/javascript" src="/public/js/jquery.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="/public/js/validform.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="/public/js/layer/layer.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="/public/js/form.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="/public/H-ui/js/H-ui.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="/public/H-ui/js/H-ui.admin.js"><?php echo '</script'; ?>
>
<?php echo ajax_upload();?>

<?php echo '<script'; ?>
 type="text/javascript">
    $(function(){
        //验证表单
        $.Tipmsg.r=null;
        $("#add").Validform({
            tiptype:function(msg){
                layer.msg(msg);
            },
            tipSweep:true,
            ajaxPost:true,
            callback:function(data){
                if(data.status=="y"){
                    layer.msg('保存成功');
                    setTimeout('layer_close()',1500);
                }
            }
        });

        //省市区回填
        area_list('prov', 0 <?php if ($_smarty_tpl->tpl_vars['item']->value['prov']!='') {?>,<?php echo $_smarty_tpl->tpl_vars['item']->value['prov'];
}?>);//省份选择
        <?php if ($_smarty_tpl->tpl_vars['item']->value['city']!=''&&$_smarty_tpl->tpl_vars['item']->value['prov']!='') {?>
            area_list('city', <?php echo $_smarty_tpl->tpl_vars['item']->value['prov'];?>
, <?php echo $_smarty_tpl->tpl_vars['item']->value['city'];?>
);//市
        <?php }?>
        <?php if ($_smarty_tpl->tpl_vars['item']->value['area']!=''&&$_smarty_tpl->tpl_vars['item']->value['city']!='') {?>
            area_list('area', <?php echo $_smarty_tpl->tpl_vars['item']->value['city'];?>
, <?php echo $_smarty_tpl->tpl_vars['item']->value['area'];?>
);//区
        <?php }?>
        //表单回填
        var formObj = new Form();
        formObj.init(<?php echo ch_json_encode($_smarty_tpl->tpl_vars['item']->value);?>
);
    })
    //添加图片展示
    function show_upload(spec_pic, url){
        $("#"+spec_pic).parent().find('img').attr('src',url);
        $("#"+spec_pic).parent().find('img').show();
        $("#"+spec_pic).parent().find('a').attr('href',url);
        $("#"+spec_pic).parent().find('[type="hidden"]').val(url);
    }
    //地区加载area_type 选择框名称 parent_id 上级id default_id默认id
    function area_list(area_type, parent_id, default_id){
        $.ajax({
            type:"POST",
            url: '<?php echo site_url("/api/area/get_list");?>
',
            data: 'parent_id='+parent_id+'&default_id='+default_id,
            dataType:"html",
            success: function(data){
                $('[name="'+area_type+'"]').html(data);
            }
        });
    }
<?php echo '</script'; ?>
>
</body>
</html><?php }} ?>
