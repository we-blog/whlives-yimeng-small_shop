<?php /* Smarty version Smarty-3.1.21-dev, created on 2019-08-30 22:10:35
         compiled from "D:\www\whlives-yimeng-master\views\manager\report\statistics\consumption_sum.html" */ ?>
<?php /*%%SmartyHeaderCode:25295d692e5b269ba6-86843707%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '340c64ec42c0f18ee4eeb0982ce5d747510fb7cd' => 
    array (
      0 => 'D:\\www\\whlives-yimeng-master\\views\\manager\\report\\statistics\\consumption_sum.html',
      1 => 1533788760,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '25295d692e5b269ba6-86843707',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'search_where' => 0,
    'list' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5d692e5b2e6bc1_24431662',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5d692e5b2e6bc1_24431662')) {function content_5d692e5b2e6bc1_24431662($_smarty_tpl) {?><!DOCTYPE HTML>
<html>
<head>
	<meta charset="utf-8">
	<meta name="renderer" content="webkit|ie-comp|ie-stand">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
	<meta http-equiv="Cache-Control" content="no-siteapp" />
	<title><?php echo config_item('manager_title');?>
</title>
	<link href="/public/H-ui/css/H-ui.min.css" rel="stylesheet" type="text/css" />
	<link href="/public/H-ui/css/H-ui.admin.css" rel="stylesheet" type="text/css" />
	<link href="/public/H-ui/hui-iconfont/iconfont.css" rel="stylesheet" type="text/css">
</head>
<body>
<nav class="breadcrumb">
	<i class="Hui-iconfont">&#xe67f;</i> 首页 <span class="c-gray en">&gt;</span> 数据统计 <span class="c-gray en">&gt;</span> 每日销售额统计 <a class="btn btn-success radius r mr-20" style="line-height:1.6em;margin-top:3px" href="javascript:location.replace(location.href);" title="刷新" ><i class="Hui-iconfont">&#xe68f;</i></a>
</nav>
<div class="pd-20" style="padding-top: 0px;">
	<div class="text-c pt-20">
		<form action="<?php echo site_url('/manager/report/consumption_sum/');?>
" method="post" class="form form-horizontal" id="search" name="search">
			<input type="text" class="input-text" placeholder="开始时间" name="start_time" onclick="laydate()" readonly style="width: 100px">-
			<input type="text" class="input-text" placeholder="结束时间" name="end_time" onclick="laydate()" readonly style="width: 100px">
			<button class="btn btn-success" type="submit"><i class="Hui-iconfont">&#xe665;</i> 搜索</button>
			<button onclick="window.location.href='<?php echo search_array_to_link($_smarty_tpl->tpl_vars['search_where']->value);?>
&export=1'" class="btn btn-primary" type="button"><i class="Hui-iconfont">&#xe640;</i> 导出</button>
		</form>
	</div>
	<div class="text-c pt-20" id="container">

	</div>
</div>
<?php echo '<script'; ?>
 type="text/javascript" src="/public/js/jquery.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="/public/H-ui/js/H-ui.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="/public/H-ui/js/H-ui.admin.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="/public/js/laydate/laydate.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="/public/js/highcharts/highcharts.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="/public/js/form.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 language="JavaScript">
	$(function(){
		//表单回填
		var formObj = new Form();
		formObj.init(<?php echo ch_json_encode($_smarty_tpl->tpl_vars['search_where']->value);?>
);
	})
<?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 language="JavaScript">
	$(function () {
		$('#container').highcharts({
			title: {
				text: '每日销售额统计',
				x: -20 //center
			},
			xAxis: {
				categories: <?php echo ch_json_encode($_smarty_tpl->tpl_vars['list']->value['categories']);?>

			},
			yAxis: {
				title: {
					text: '单位(元)'
				},
				plotLines: [{
					value: 0,
					width: 1,
					color: '#808080'
				}]
			},
			tooltip: {
				valueSuffix: '元'
			},
			series: [{
				name: '销售额',
				data: [<?php echo join(',',$_smarty_tpl->tpl_vars['list']->value['value']);?>
]
			}]
		});
	});
<?php echo '</script'; ?>
>
</body>
</html><?php }} ?>
