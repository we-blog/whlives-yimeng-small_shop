<?php /* Smarty version Smarty-3.1.21-dev, created on 2019-08-13 23:14:30
         compiled from "D:\www\whlives-yimeng-master\views\manager\goods\goods\add.html" */ ?>
<?php /*%%SmartyHeaderCode:234835d52d3d670a912-31551571%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '95d284f8bb548c9b035d0b6f460a868f2b069546' => 
    array (
      0 => 'D:\\www\\whlives-yimeng-master\\views\\manager\\goods\\goods\\add.html',
      1 => 1533788760,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '234835d52d3d670a912-31551571',
  'function' => 
  array (
    'type' => 
    array (
      'parameter' => 
      array (
        'level' => 0,
      ),
      'compiled' => '',
    ),
  ),
  'variables' => 
  array (
    'data' => 0,
    'key' => 0,
    'level' => 0,
    'cat_list' => 0,
    'shop_list' => 0,
    'k' => 0,
    'brand_list' => 0,
    'item' => 0,
    'val' => 0,
  ),
  'has_nocache_code' => 0,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5d52d3d69c1eb6_97757263',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5d52d3d69c1eb6_97757263')) {function content_5d52d3d69c1eb6_97757263($_smarty_tpl) {?><!DOCTYPE HTML>
<html>
<head>
	<meta charset="utf-8">
	<meta name="renderer" content="webkit|ie-comp|ie-stand">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
	<meta http-equiv="Cache-Control" content="no-siteapp" />
	<title><?php echo config_item('manager_title');?>
</title>
	<link href="/public/H-ui/css/H-ui.min.css" rel="stylesheet" type="text/css" />
	<link href="/public/H-ui/css/H-ui.admin.css" rel="stylesheet" type="text/css" />
	<link href="/public/H-ui/hui-iconfont/iconfont.css" rel="stylesheet" type="text/css">
	<link href="/public/H-ui/css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
<div class="pd-20">
	<form action="<?php echo site_url('/manager/goods/goods/save');?>
" method="post" class="form form-horizontal" id="add" enctype="multipart/form-data">
		<input type="hidden" name="id" value="">
		<div class="row cl">
			<label class="form-label col-2"><span class="c-red">*</span>商品标题：</label>
			<div class="formControls col-10">
				<input type="text" class="input-text" value="" name="name" datatype="*1-50" nullmsg="商品标题不能为空">
			</div>
		</div>
		<div class="row cl">
			<label class="form-label col-2"><span class="c-red">*</span>分类栏目：</label>
			<div class="formControls col-2"> <span class="select-box">
				<select name="cat_id" class="select" datatype="*" nullmsg="请选择分类" onchange="cat_id_select(this.value)">
					<option value="">请选择分类</option>
					<!--分类模板-->
					<?php if (!function_exists('smarty_template_function_type')) {
    function smarty_template_function_type($_smarty_tpl,$params) {
    $saved_tpl_vars = $_smarty_tpl->tpl_vars;
    foreach ($_smarty_tpl->smarty->template_functions['type']['parameter'] as $key => $value) {$_smarty_tpl->tpl_vars[$key] = new Smarty_variable($value);};
    foreach ($params as $key => $value) {$_smarty_tpl->tpl_vars[$key] = new Smarty_variable($value);}?>
					<?php  $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['key']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['data']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['key']->key => $_smarty_tpl->tpl_vars['key']->value) {
$_smarty_tpl->tpl_vars['key']->_loop = true;
?>
						<option value="<?php echo $_smarty_tpl->tpl_vars['key']->value['id'];?>
"><?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['loop'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['loop']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['name'] = 'loop';
$_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['level']->value) ? count($_loop) : max(0, (int) $_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['total']);
?>├<?php endfor; endif;
echo $_smarty_tpl->tpl_vars['key']->value['name'];?>
</option>
					<?php smarty_template_function_type($_smarty_tpl,array('data'=>$_smarty_tpl->tpl_vars['key']->value['down'],'level'=>$_smarty_tpl->tpl_vars['level']->value+1));?>

					<?php } ?>
					<?php $_smarty_tpl->tpl_vars = $saved_tpl_vars;
foreach (Smarty::$global_tpl_vars as $key => $value) if(!isset($_smarty_tpl->tpl_vars[$key])) $_smarty_tpl->tpl_vars[$key] = $value;}}?>

					<!--调用模板函数-->
					<?php smarty_template_function_type($_smarty_tpl,array('data'=>$_smarty_tpl->tpl_vars['cat_list']->value));?>

				</select>
				</span>
			</div>
			<label class="form-label col-1"><span class="c-red">*</span>商户：</label>
			<div class="formControls col-2"> <span class="select-box">
				<select name="shop_id" class="select">
					<?php  $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['k']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['shop_list']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['k']->key => $_smarty_tpl->tpl_vars['k']->value) {
$_smarty_tpl->tpl_vars['k']->_loop = true;
?>
					<option value="<?php echo $_smarty_tpl->tpl_vars['k']->value['m_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['k']->value['shop_name'];?>
</option>
					<?php } ?>
				</select>
				</span>
			</div>
			<label class="form-label col-1">品牌：</label>
			<div class="formControls col-2"> <span class="select-box">
				<select name="brand_id" class="select">
					<option value="0">请选择品牌</option>
					<?php  $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['k']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['brand_list']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['k']->key => $_smarty_tpl->tpl_vars['k']->value) {
$_smarty_tpl->tpl_vars['k']->_loop = true;
?>
					<option value="<?php echo $_smarty_tpl->tpl_vars['k']->value['id'];?>
"><?php echo $_smarty_tpl->tpl_vars['k']->value['name'];?>
</option>
					<?php } ?>
				</select>
				</span>
			</div>
		</div>
		<div class="row cl">
			<label class="form-label col-2"><span class="c-red">*</span>是否上架：</label>
			<div class="formControls col-2 skin-minimal">
				<div class="radio-box">
					<input type="radio" id="type-0" name="status" value="0" checked>
					<label for="type-0">上架</label>
				</div>
				<div class="radio-box">
					<input type="radio" id="type-2" name="status" value="2">
					<label for="type-2">下架</label>
				</div>
				<div class="radio-box" style="display: none">
					<input type="radio" id="type-3" name="status" value="3">
					<label for="type-3">待审</label>
				</div>
				<div class="radio-box" style="display: none">
					<input type="radio" id="type-4" name="status" value="4">
					<label for="type-4">审核拒绝</label>
				</div>
			</div>
			<label class="form-label col-1"><span class="c-red">*</span>单位：</label>
			<div class="formControls col-2">
				<input type="text" name="unit" value="件" class="input-text" datatype="*" nullmsg="计量单位不能为空">

			</div>
			<label class="form-label col-1"><span class="c-red">*</span>排序：</label>
			<div class="formControls col-2">
				<input type="text" name="sortnum" value="99" class="input-text" datatype="n" nullmsg="排序不能为空" errormsg="排序只能是数字">

			</div>
		</div>
		<div class="row cl">
			<div class="col-10 col-offset-2">
				<button onClick="$('#add').submit();" class="btn btn-primary radius" type="submit"><i class="Hui-iconfont">&#xe632;</i> 保存并提交审核</button>
			</div>
		</div>
		<div class="row cl">
			<label class="form-label col-2">商品SKU：</label>
			<div class="formControls col-10">
				<table style="width: auto;" class="table table-border table-bordered table-bg table-hover table-sort" id="spec_list_th">
					<thead>
					<tr class="text-c">
						<th width="100">商品货号</th>
						<th width="70">库存</th>
						<th width="70">市场价格</th>
						<th width="70">销售价格</th>
						<th width="70">重量(克)</th>
						<th width="70">起订量</th>
						<th width="40">操作</th>
					</tr>
					</thead>
					<tbody id="spec_list">

					</tbody>
				</table>
				<!--规格名称模板-->
				<?php echo '<script'; ?>
 type='text/html' id='spec_th_valueTemplate'>
					{{each}}
					<th width="{{if $value.type==1}}70{{else}}170{{/if}}">
						<input type="text" class="input-text" value="{{if $value.spec_num}}{{if $value.spec_num==1}}颜色{{else if $value.spec_num==2}}尺寸{{else if $value.spec_num==3}}型号{{else if $value.spec_num==4}}规格{{/if}}{{else}}{{$value.name}}{{/if}}" name="spec_name[]" style="width: 50px;" datatype="*" nullmsg="规格名称不能为空">
						<input type="hidden" value="{{$value.type}}" name="spec_name_type[]">
						<a><i class="Hui-iconfont">&#xe6a6;</i></a>
					</th>
					{{/each}}
				<?php echo '</script'; ?>
>
				<!--动态规格值模板-->
				<?php echo '<script'; ?>
 type='text/html' id='spec_valueTemplate'>
					{{each}}
					<td>
						{{if $value.type==1}}
						<!--文本-->
						<input type="text" class="input-text" value="{{$value.value}}" name="spec_value[{{$value.spec_num}}][]" style="width: 80px;" datatype="*" nullmsg="规格值不能为空">
						<input type="hidden" value="{{$value.note}}" name="spec_value_note[{{$value.spec_num}}][]">
						{{else if $value.type==2}}
						<!--图片-->
						<a><img src="{{$value.value}}" width="30"></a>
						<input type="text" class="input-text" value="{{$value.note}}" name="spec_value_note[{{$value.spec_num}}][]" style="width: 60px;" datatype="*" nullmsg="图片规格备注不能为空" placeholder="备注">
						<input type="hidden" value="{{$value.value}}" name="spec_value[{{$value.spec_num}}][]" datatype="*" nullmsg="图片规格值不能为空">
						<input type="file" id="spec_value_img_{{$value.spec_num}}_{{$value.sv}}" name="spec_value_img_{{$value.spec_num}}_{{$value.sv}}" onchange="upload_file(this);" style="width: 60px;">
						{{/if}}
					</td>
					{{/each}}
				<?php echo '</script'; ?>
>
				<!--规格列表模板-->
				<?php echo '<script'; ?>
 type='text/html' id='spec_listTemplate'>
					{{each}}
					<tr class="text-c">
						<td>
							<input type="hidden" name="sku_id[]" value="{{$value.id}}">
							<input type="text" class="input-text" value="{{$value.sku_no}}" name="sku_no[]" style="width: 120px;" datatype="*" nullmsg="商品货号不能为空">
						</td>
						<td>
							<input type="text" class="input-text" value="{{$value.store_nums}}" name="store_nums[]" style="width: 60px;" datatype="n" nullmsg="库存不能为空" errormsg="库存只能是数字">
						</td>
						<td>
							<input type="text" class="input-text" value="{{$value.market_price}}" name="market_price[]" style="width: 60px;" datatype="price" nullmsg="市场价格不能为空" errormsg="市场价格格式错误">
						</td>
						<td>
							<input type="text" class="input-text" value="{{$value.sell_price}}" name="sell_price[]" style="width: 60px;" datatype="price" nullmsg="销售价格不能为空" errormsg="销售价格格式错误">
						</td>
						<td>
							<input type="text" class="input-text" value="{{$value.weight}}" name="weight[]" style="width: 60px;" datatype="n" nullmsg="重量不能为空" errormsg="重量只能是数字">
						</td>
						<td>
							<input type="text" class="input-text" value="{{if $value.minimum}}{{$value.minimum}}{{else}}1{{/if}}" name="minimum[]" style="width: 60px;" datatype="n" nullmsg="起订量不能为空" errormsg="起订量只能是数字">
						</td>
						<td class="f-14 spec_delete">{{if $index>0}}<i id="delete_tr" class="Hui-iconfont">&#xe609;</i>{{/if}}</td>
					</tr>
					{{/each}}
				<?php echo '</script'; ?>
>
			</div>
		</div>
		<div class="row cl">
			<label class="form-label col-2"></label>
			<div class="formControls col-10">
				<button onClick="add_spec();" class="btn btn-primary radius" type="button">添加商品SKU</button>
				<button onClick="add_spec_th(1)" class="btn btn-primary radius" type="button">添加文字规格</button>
				<button onClick="add_spec_th(2)" class="btn btn-primary radius" type="button">添加图片规格</button>
			</div>
		</div>
		<div class="row cl">
			<label class="form-label col-2"><span class="c-red">*</span>模型：</label>
			<div class="formControls col-6"> <span class="select-box" style="width: 150px;">
				<select name="model_id" class="select">
					<option value="">选择模型</option>
				</select>
				</span>
				可以加入商品扩展属性，比如：型号，年代，款式...
			</div>
		</div>
		<div class="row cl" id="model_select" style="display: none">
			<label class="form-label col-2">扩展属性：</label>
			<div class="formControls col-8">
				<table class="table table-border table-bordered table-bg table-hover table-sort">
					<thead id="goods_model">

					</thead>
				</table>
				<!--商品模型模板-->
				<?php echo '<script'; ?>
 type='text/html' id='goods_modelTemplate'>
					{{each}}
					<tr class="text-c">
						<th width="100">{{$value.name}}</th>
						<td class="text-l">

							<!--单选框-->
								{{if $value.type==1}}
								{{each $value.value as spec_value i}}
								<div class="radio-box">
									<input {{if $value.checked==spec_value}}checked{{/if}} type="radio" id="radio-{{$value.id}}{{i}}"  name="attr_id_{{$value.id}}" value="{{spec_value}}">
									<label for="radio-{{$value.id}}{{i}}">{{spec_value}}</label>
								</div>
								{{/each}}
							<!--复选框-->
								{{else if $value.type==2}}
								{{each $value.value as spec_value i}}
								<div class="radio-box">
									<input type="checkbox" id="checkbox-{{$value.id}}{{i}}" name="attr_id_{{$value.id}}[]" value="{{spec_value}}" {{each $value.checked as checked j}}{{if spec_value==checked}}checked{{/if}}{{/each}}>
									<label for="checkbox-{{$value.id}}{{i}}">{{spec_value}}</label>
								</div>
								{{/each}}
							<!--下拉框-->
								{{else if $value.type==3}}
								<span class="select-box" style="width: 150px;">
									<select name="attr_id_{{$value.id}}" class="select">
										{{each $value.value as spec_value i}}
										<option value="{{spec_value}}" {{if $value.checked==spec_value}}selected{{/if}}>{{spec_value}}</option>
										{{/each}}
									</select>
								</span>
							<!--输入框-->
								{{else if $value.type==4}}
								<input type="text" class="input-text" value="{{$value.checked}}" name="attr_id_{{$value.id}}" style="width: 200px;">
								{{/if}}
						</td>
					</tr>
					{{/each}}
				<?php echo '</script'; ?>
>
			</div>
		</div>

		<div class="row cl">
			<label class="form-label col-2">商品图片：</label>
			<div class="formControls col-10">
				<button class="btn btn-primary radius" type="button" id="select_goods_pic">选择图片</button>
			</div>
		</div>
		<div class="row cl">
			<label class="form-label col-2"></label>
			<input type="hidden" name="image" value="">
			<div class="formControls col-10" id="goods_list_pic">
				<!--商品图片-->
			</div>
			<!--商品图片模板-->
			<?php echo '<script'; ?>
 type='text/html' id='goods_picTemplate'>
				{{each}}
				<li class="f-l mr-10 text-c l-24 f-12">
					<input type="hidden" name="image_list[]" value="{{$value.url}}">
					<img src="{{$value.url}}" width="100" height="100" class="{{if $value.url=='<?php echo $_smarty_tpl->tpl_vars['item']->value['image'];?>
'}}goods_img_current{{else}}goods_img_border{{/if}}"><br>
					<a class="c-primary">删除</a>
				</li>
				{{/each}}
			<?php echo '</script'; ?>
>
		</div>
		<div class="row cl">
			<label class="form-label col-2">详细内容：</label>
			<div class="formControls col-10">
				<?php echo '<script'; ?>
 type="text/javascript" src="/public/kindeditor/kindeditor.js"><?php echo '</script'; ?>
>
				<?php echo load_editer('desc',$_smarty_tpl->tpl_vars['item']->value['desc']);?>

			</div>
		</div>
		<div class="row cl">
			<div class="col-10 col-offset-2">
				<button onClick="$('#add').submit();" class="btn btn-primary radius" type="submit"><i class="Hui-iconfont">&#xe632;</i> 保存并提交审核</button>
				<button onClick="layer_close();" class="btn btn-default radius" type="button">&nbsp;&nbsp;取消&nbsp;&nbsp;</button>
			</div>
		</div>
	</form>
</div>
</div>

<?php echo '<script'; ?>
 type="text/javascript" src="/public/js/jquery.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="/public/js/validform.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="/public/js/layer/layer.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="/public/js/form.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="/public/js/artTemplate.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="/public/H-ui/js/H-ui.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="/public/H-ui/js/H-ui.admin.js"><?php echo '</script'; ?>
>
<?php echo ajax_upload();?>

<?php echo plupload('select_goods_pic',array('num'=>5));?>

<?php echo '<script'; ?>
 language="JavaScript">
	var spec_num = 0;
	$(function() {
		//验证表单
		$.Tipmsg.r = null;
		$("#add").Validform({
			tiptype: function (msg) {
				layer.msg(msg);
			},
			tipSweep: true,
			ajaxPost: true,
			callback: function (data) {
				if (data.status == "y") {
					layer_close();
				}
			}
		});
	});

	//选择分类加载品牌和模型
	function cat_id_select(cat_id) {
		//品牌
		$.ajax({
			type:"POST",
			url: '<?php echo site_url("/manager/goods/goods/cat_brand/");?>
',
			data: 'cat_id='+cat_id,
			dataType:"json",
			success: function(data){
				var brand_select_html = '<option value="0">请选择品牌</option>';
				var brand_selected = '';
				var brand_id = '<?php echo $_smarty_tpl->tpl_vars['item']->value['brand_id'];?>
';
				if (data.status == 'y') {
					$.each(data.result, function(i,v){
						if (v.id == brand_id) {
							brand_selected = 'selected';
						} else {
							brand_selected = '';
						}
						brand_select_html += '<option value="'+ v.id+'" '+brand_selected+'>'+ v.name+'</option>';
					})
				}
				$('[name="brand_id"]').html(brand_select_html);
			}
		});

		//模型
		$.ajax({
			type:"POST",
			url: '<?php echo site_url("/manager/goods/goods/cat_model/");?>
',
			data: 'cat_id='+cat_id,
			dataType:"json",
			success: function(data){
				var model_select_html = '<option value="0">请选择模型</option>';
				var model_selected = '';
				var model_id = '<?php echo $_smarty_tpl->tpl_vars['item']->value['model_id'];?>
';
				if (data.status == 'y') {
					$.each(data.result, function(i,v){
						if (v.id == model_id) {
							model_selected = 'selected';
							model_select(v.id);//设置默认模型
						} else {
							model_selected = '';
						}
						model_select_html += '<option value="'+ v.id+'" '+model_selected+'>'+ v.name+'</option>';
					})
				}
				$('[name="model_id"]').html(model_select_html);
			}
		});
	}

	//上传商品图片展示
	function show_plupload(data){
		var html = template('goods_picTemplate',[data]);
		$('#goods_list_pic').append(html);
	}

	//添加规格名称
	function  add_spec_th(spec_type){
		spec_num++;
		var all_spec_num = $("#spec_list_th th").length-5;//已经添加的规格个数
		if(all_spec_num>4){
			layer.msg('最多只能添加4个属性');
			return false;
		}
		var th_data = [{"type":spec_type,"spec_num":spec_num}];
		var spec_th_value = template('spec_th_valueTemplate', th_data);//规格标题
		$('#spec_list_th').find('th').eq(all_spec_num).before(spec_th_value);//规格标题
		var sv = 0;
		$('#spec_list >tr').each (function(){
			var value_data = [{"type":spec_type,"spec_value":"","spec_num":spec_num,"sv":sv}];
			var spec_value = template('spec_valueTemplate', value_data);//规格属性
			$(this).find('td').eq(all_spec_num).before(spec_value);
			sv++;
		})
	}

	//添加sku规格
	function add_spec(){
		var spec_list_value_old = $('#spec_list').find('tr').eq(0).clone();//缓存第一行的数据

		//添加删除按钮
		$('#spec_list').find('tr').eq(0).find('.spec_delete').html('<i id="delete_tr" class="Hui-iconfont">&#xe609;</i>');

		//清空第一行的数据
		$('#spec_list').find('tr').eq(0).find('img').attr('src','');//清除规格图片
		$('#spec_list').find('tr').eq(0).find('input').each(function(){
			//$(this).val('');//清空文本框的值
			if($(this).attr('name')=='sku_id[]')
			{
				$(this).val('');//设置skuid
			}
			if($(this).attr('name')=='store_nums[]')
			{
				$(this).val('100');//设置库存
			}
			if($(this).attr('name')=='sku_no[]')
			{
				$(this).val('<?php echo time();?>
-'+$("#spec_list tr").length);//设置货号
			}
			if($(this).attr('type')=='file')
			{
				$(this).attr('id',$(this).attr('id')+$("#spec_list tr").length);//设置图片id
				$(this).attr('name',$(this).attr('name')+$("#spec_list tr").length);//设置图片name
			}
		})
		$('#spec_list').append($('#spec_list').find('tr').eq(0).clone());//复制第一行的数据到最后
		$('#spec_list').find('tr').eq(0).replaceWith(spec_list_value_old);//还原第一行的数据
	}

	//添加规格图片展示
	function show_upload(spec_pic, url){
		$("#"+spec_pic).parents('td').find('img').attr('src',url);
		$("#"+spec_pic).parents('td').find('img').show();
		$("#"+spec_pic).parents('td').find('[type="hidden"]').val(url);
	}

	//模型加载
	function model_select(model_id){
		if(model_id=='')
		{
			$('#goods_model').html('');
			$('#model_select').hide();
			return false;
		}
		$.ajax({
			type:"POST",
			url: '<?php echo site_url("/manager/goods/goods/goods_model_select/");?>
',
			data: 'goods_id=<?php echo $_smarty_tpl->tpl_vars['item']->value['id'];?>
&model_id='+model_id,
			dataType:"json",
			success: function(data){
				var html = template('goods_modelTemplate',data);
				$('#goods_model').html(html);
				$('#model_select').show();
			}
		});
	}

	$(function(){
		//模型选择
		$('[name="model_id"]').change(function(){
			model_select($(this).val());
		})

		//删除sku元素
		$('#spec_list').on('click', '#delete_tr', function(){
			$(this).parents("tr").remove();
		})

		//删除规格
		var index_td = '';
		$('#spec_list_th').on('click','a', function(){
			index_td = $(this).parent().index();
			$('#spec_list_th').find('th').eq(index_td).remove();
			$('#spec_list >tr').each (function(){
			$(this).find('td').eq(index_td).remove();
		})
		})

		//删除商品图片
		$('#goods_list_pic').on('click', 'a', function(){
			$(this).parents('li').remove();
			plupload_pic_num--;
			//没有选择默认图片的时候清空默认图片地址
			goods_img_current = $('#goods_list_pic').find('.goods_img_current').attr('src');
			if (goods_img_current===undefined || goods_img_current=='') {
			$('[name="image"]').val('');
		}
		})

		//选择商品默认图片
		$('#goods_list_pic').on('click', 'img', function(){
			$('#goods_list_pic img').removeClass('goods_img_current');
			$('#goods_list_pic img').addClass('goods_img_border');
			$(this).removeClass('goods_img_border');
			$(this).addClass('goods_img_current');
			$('[name="image"]').val($(this).attr('src'));
		})

		//表单回填
		var formObj = new Form();
		formObj.init(<?php echo ch_json_encode($_smarty_tpl->tpl_vars['item']->value);?>
);

		//商品图片回填
		<?php if ($_smarty_tpl->tpl_vars['item']->value['image_list']!='') {?>
		var html = template('goods_picTemplate',<?php echo ch_json_encode($_smarty_tpl->tpl_vars['item']->value['image_list']);?>
);
		$('#goods_list_pic').append(html);
		<?php }?>

		//设置默认商品sku
		<?php if ($_smarty_tpl->tpl_vars['item']->value['sku_list']!='') {?>
			var spec_list_html = template('spec_listTemplate',<?php echo ch_json_encode($_smarty_tpl->tpl_vars['item']->value['sku_list']);?>
);
		<?php } else { ?>
			var spec_list_html = template('spec_listTemplate',[{"sku_no":"<?php echo time();?>
","store_nums":100,"value0":"","value1":"","value2":"","value3":""}]);//没有数据设置第一行默认数据
		<?php }?>
		$('#spec_list').append(spec_list_html);

		//设置默认规格标题
		<?php if ($_smarty_tpl->tpl_vars['item']->value['spec_name']!='') {?>
			var spec_th_value = template('spec_th_valueTemplate', <?php echo ch_json_encode($_smarty_tpl->tpl_vars['item']->value['spec_name']);?>
);//规格标题
			$('#spec_list_th').find('th').eq(1).before(spec_th_value);//规格标题
		<?php }?>
		spec_num = <?php echo count($_smarty_tpl->tpl_vars['item']->value['spec_name']);?>


		<?php  $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['key']->_loop = false;
 $_smarty_tpl->tpl_vars['val'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['item']->value['sku_list']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['key']->key => $_smarty_tpl->tpl_vars['key']->value) {
$_smarty_tpl->tpl_vars['key']->_loop = true;
 $_smarty_tpl->tpl_vars['val']->value = $_smarty_tpl->tpl_vars['key']->key;
?>
			var spec_value = template('spec_valueTemplate', <?php echo ch_json_encode($_smarty_tpl->tpl_vars['key']->value['value_list']);?>
);//规格属性
			$('#spec_list >tr').eq(<?php echo $_smarty_tpl->tpl_vars['val']->value;?>
).find('td').eq(1).before(spec_value)
		<?php } ?>

		//分类默认数据加载
		<?php if ($_smarty_tpl->tpl_vars['item']->value['cat_id']!='') {?>
			cat_id_select(<?php echo $_smarty_tpl->tpl_vars['item']->value['cat_id'];?>
);
		<?php }?>
	})
<?php echo '</script'; ?>
>
</body>
</html><?php }} ?>
