<?php /* Smarty version Smarty-3.1.21-dev, created on 2019-08-13 23:11:59
         compiled from "D:\www\whlives-yimeng-master\views\manager\member\user\group_add.html" */ ?>
<?php /*%%SmartyHeaderCode:266765d52d33f5d7600-41854087%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'f62cd4a1eba265b1682b2cbefe599dd0d20afa7a' => 
    array (
      0 => 'D:\\www\\whlives-yimeng-master\\views\\manager\\member\\user\\group_add.html',
      1 => 1533788760,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '266765d52d33f5d7600-41854087',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'item' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5d52d33f621998_94161181',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5d52d33f621998_94161181')) {function content_5d52d33f621998_94161181($_smarty_tpl) {?><!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<meta name="renderer" content="webkit|ie-comp|ie-stand">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
<meta http-equiv="Cache-Control" content="no-siteapp" />
    <title><?php echo config_item('manager_title');?>
</title>
    <link href="/public/H-ui/css/H-ui.min.css" rel="stylesheet" type="text/css" />
    <link href="/public/H-ui/css/H-ui.admin.css" rel="stylesheet" type="text/css" />
    <link href="/public/H-ui/hui-iconfont/iconfont.css" rel="stylesheet" type="text/css">
</head>
<body>
<div class="pd-20">
	<form action="<?php echo site_url('/manager/member/user_group/save');?>
" method="post" class="form form-horizontal" id="add">
		<div class="row cl">
			<label class="form-label col-3"><span class="c-red">*</span>会员组名称：</label>
			<div class="formControls col-5">
				<input type="text" class="input-text" value="" name="group_name" datatype="*" nullmsg="请输入会员组名称！">
			</div>
		</div>
		<div class="row cl">
			<label class="form-label col-3">折扣率0-100：</label>
			<div class="formControls col-5">
				<input type="text" class="input-text" value="100" name="discount" datatype="n1-3" nullmsg="请输入折扣率！" errormsg="折扣率只能在0-100">
			</div>
		</div>
		<div class="row cl">
			<div class="col-10 col-offset-2">
                <input type="hidden" name="id" value="">
				<button onClick="$('#add').submit();" class="btn btn-primary radius" type="submit"><i class="Hui-iconfont">&#xe632;</i> 保存</button>
				<button onClick="layer_close();" class="btn btn-default radius" type="button">&nbsp;&nbsp;取消&nbsp;&nbsp;</button>
			</div>
		</div>
	</form>
</div>
</div>
<?php echo '<script'; ?>
 type="text/javascript" src="/public/js/jquery.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="/public/js/validform.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="/public/js/layer/layer.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="/public/js/form.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="/public/H-ui/js/H-ui.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="/public/H-ui/js/H-ui.admin.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript">
    $(function(){
        //验证表单
        $.Tipmsg.r=null;
        $("#add").Validform({
            tiptype:function(msg){
                layer.msg(msg);
            },
            tipSweep:true,
            ajaxPost:true,
            callback:function(data){
                if(data.status=="y"){
                    layer_close();
                }
            }
        });
        //表单回填
        var formObj = new Form();
        formObj.init(<?php echo ch_json_encode($_smarty_tpl->tpl_vars['item']->value);?>
);
    })
<?php echo '</script'; ?>
>
</body>
</html><?php }} ?>
