<?php /* Smarty version Smarty-3.1.21-dev, created on 2019-08-13 23:03:04
         compiled from "D:\www\whlives-yimeng-master\views\manager\index\header.html" */ ?>
<?php /*%%SmartyHeaderCode:256845d52d128d5aeb8-93645701%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'c574346e68a53611b3d6060892c4720ae335e43e' => 
    array (
      0 => 'D:\\www\\whlives-yimeng-master\\views\\manager\\index\\header.html',
      1 => 1533788760,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '256845d52d128d5aeb8-93645701',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'admin_data' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5d52d129014839_76843556',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5d52d129014839_76843556')) {function content_5d52d129014839_76843556($_smarty_tpl) {?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title><?php echo config_item('manager_title');?>
</title>
    <link href="/public/H-ui/css/style.css" rel="stylesheet" type="text/css">
    <link href="/public/H-ui/hui-iconfont/iconfont.css" rel="stylesheet" type="text/css">
</head>
<body>
    <div class="top_header">
        <div class="top_left">
            <span><a href="<?php echo site_url('/manager/welcome/main');?>
" target="main"><?php echo config_item('manager_title');?>
</a></span>
        </div>
        <div class="top_menu">
            <?php if ($_smarty_tpl->tpl_vars['admin_data']->value['role_id']==0||stripos($_smarty_tpl->tpl_vars['admin_data']->value['role'],'goods')!==false) {?>
            <a href="<?php echo site_url('/manager/welcome/menu/goods');?>
" target="left"><i class="Hui-iconfont">&#xe620;</i> 商品</a>&nbsp&nbsp
            <?php }?>
            <?php if ($_smarty_tpl->tpl_vars['admin_data']->value['role_id']==0||stripos($_smarty_tpl->tpl_vars['admin_data']->value['role'],'member')!==false) {?>
            <a href="<?php echo site_url('/manager/welcome/menu/member');?>
" target="left"><i class="Hui-iconfont">&#xe60d;</i> 会员</a>&nbsp&nbsp
            <?php }?>
            <?php if ($_smarty_tpl->tpl_vars['admin_data']->value['role_id']==0||stripos($_smarty_tpl->tpl_vars['admin_data']->value['role'],'order')!==false) {?>
            <a href="<?php echo site_url('/manager/welcome/menu/order');?>
" target="left"><i class="Hui-iconfont">&#xe673;</i> 订单</a>&nbsp&nbsp
            <?php }?>
            <?php if ($_smarty_tpl->tpl_vars['admin_data']->value['role_id']==0||stripos($_smarty_tpl->tpl_vars['admin_data']->value['role'],'tool')!==false) {?>
            <a href="<?php echo site_url('/manager/welcome/menu/tool');?>
" target="left"><i class="Hui-iconfont">&#xe63c;</i> 工具</a>&nbsp&nbsp
            <?php }?>
            <?php if ($_smarty_tpl->tpl_vars['admin_data']->value['role_id']==0||stripos($_smarty_tpl->tpl_vars['admin_data']->value['role'],'market')!==false) {?>
            <a href="<?php echo site_url('/manager/welcome/menu/market');?>
" target="left"><i class="Hui-iconfont">&#xe6bb;</i> 营销</a>&nbsp&nbsp
            <?php }?>
            <?php if ($_smarty_tpl->tpl_vars['admin_data']->value['role_id']==0||stripos($_smarty_tpl->tpl_vars['admin_data']->value['role'],'report')!==false) {?>
            <a href="<?php echo site_url('/manager/welcome/menu/report');?>
" target="left"><i class="Hui-iconfont">&#xe61e;</i> 统计</a>&nbsp&nbsp
            <?php }?>
            <?php if ($_smarty_tpl->tpl_vars['admin_data']->value['role_id']==0||stripos($_smarty_tpl->tpl_vars['admin_data']->value['role'],'system')!==false) {?>
            <a href="<?php echo site_url('/manager/welcome/menu/system');?>
" target="left"><i class="Hui-iconfont">&#xe61d;</i> 系统</a>
            <?php }?>
        </div>
        <div class="top_right"><span>欢迎<?php echo $_smarty_tpl->tpl_vars['admin_data']->value['username'];?>
 <a href="<?php echo site_url('/manager/login/loginout');?>
">退出</a></span></div>
        <div class="clear"></div>
    </div>
</body>
</html><?php }} ?>
