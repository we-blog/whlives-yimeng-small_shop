<?php /* Smarty version Smarty-3.1.21-dev, created on 2019-08-13 23:11:45
         compiled from "D:\www\whlives-yimeng-master\views\manager\order\refund_doc\pending.html" */ ?>
<?php /*%%SmartyHeaderCode:70485d52d3319db8e8-16911985%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '0db2e28749cf434815078a63f0e57d2384cccbd0' => 
    array (
      0 => 'D:\\www\\whlives-yimeng-master\\views\\manager\\order\\refund_doc\\pending.html',
      1 => 1533788760,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '70485d52d3319db8e8-16911985',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'list' => 0,
    'key' => 0,
    'page_count' => 0,
    'search_where' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5d52d331ac9d91_41575484',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5d52d331ac9d91_41575484')) {function content_5d52d331ac9d91_41575484($_smarty_tpl) {?><!DOCTYPE HTML>
<html>
<head>
	<meta charset="utf-8">
	<meta name="renderer" content="webkit|ie-comp|ie-stand">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
	<meta http-equiv="Cache-Control" content="no-siteapp" />
	<title><?php echo config_item('manager_title');?>
</title>
	<link href="/public/H-ui/css/H-ui.min.css" rel="stylesheet" type="text/css" />
	<link href="/public/H-ui/css/H-ui.admin.css" rel="stylesheet" type="text/css" />
	<link href="/public/H-ui/hui-iconfont/iconfont.css" rel="stylesheet" type="text/css">
</head>
<body>
<nav class="breadcrumb">
	<i class="Hui-iconfont">&#xe67f;</i> 首页 <span class="c-gray en">&gt;</span> 订单管理 <span class="c-gray en">&gt;</span> 退款申请处理 <a class="btn btn-success radius r mr-20" style="line-height:1.6em;margin-top:3px" href="javascript:location.replace(location.href);" title="刷新" ><i class="Hui-iconfont">&#xe68f;</i></a>
</nav>
<div class="pd-20" style="padding-top: 0px;">
	<div class="mt-20">
		<table class="table table-border table-bordered table-bg table-hover table-sort">
			<thead>
			<tr class="text-c">
				<th width="30"><input name="" type="checkbox" value=""></th>
				<th width="50">ID</th>
				<th width="60">订单号</th>
				<th width="80">用户名</th>
				<th width="80">金额</th>
				<th width="120">时间</th>
				<th>备注</th>
				<th width="100">操作</th>
			</tr>
			</thead>
			<tbody>
			<?php  $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['key']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['list']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['key']->key => $_smarty_tpl->tpl_vars['key']->value) {
$_smarty_tpl->tpl_vars['key']->_loop = true;
?>
			<tr class="text-c">
				<td><input name="id[]" type="checkbox" value="<?php echo $_smarty_tpl->tpl_vars['key']->value['id'];?>
"></td>
				<td><?php echo $_smarty_tpl->tpl_vars['key']->value['id'];?>
</td>
				<td class="text-l" onclick="open_iframe('订单详情','<?php echo site_url("/manager/order/order/view/".((string)$_smarty_tpl->tpl_vars['key']->value['order_id']));?>
','100%')"><?php echo $_smarty_tpl->tpl_vars['key']->value['order_no'];?>
</td>
				<td class="text-l"><?php echo $_smarty_tpl->tpl_vars['key']->value['username'];?>
</td>
				<td class="text-l">￥<?php echo $_smarty_tpl->tpl_vars['key']->value['amount'];?>
</td>
				<td class="text-c"><?php echo date('Y-m-d H:i:s',$_smarty_tpl->tpl_vars['key']->value['addtime']);?>
</td>
				<td class="text-l"><?php echo $_smarty_tpl->tpl_vars['key']->value['note'];?>
</td>
				<td class="text-c">
					<a style="text-decoration:none" class="ml-5" onClick="open_iframe('查看处理流程','<?php echo site_url("/manager/order/refund_doc/refund_doc_log/".((string)$_smarty_tpl->tpl_vars['key']->value['id']));?>
',600,450)" href="javascript:;" title="查看处理流程"><i class="Hui-iconfont">&#xe665;</i></a>&nbsp;
					<a href="javascript:;" onClick="open_iframe('退款处理','<?php echo site_url("/manager/order/refund_doc/refund_doc_view/".((string)$_smarty_tpl->tpl_vars['key']->value['id']));?>
',600,450)" class="btn btn-danger radius">处理</a>
				</td>
			</tr>
			<?php } ?>
			</tbody>
		</table>
	</div>
</div>
<!--分页-->
<?php echo page_view('page',$_smarty_tpl->tpl_vars['page_count']->value,search_array_to_link($_smarty_tpl->tpl_vars['search_where']->value));?>

<?php echo '<script'; ?>
 type="text/javascript" src="/public/js/jquery.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="/public/js/layer/layer.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="/public/js/jquery.dataTables.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="/public/js/form.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="/public/H-ui/js/H-ui.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="/public/H-ui/js/H-ui.admin.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 language="JavaScript">
	$(function(){
		//表单回填
		var formObj = new Form();
		formObj.init(<?php echo ch_json_encode($_smarty_tpl->tpl_vars['search_where']->value);?>
);
	})
<?php echo '</script'; ?>
>
</body>
</html><?php }} ?>
