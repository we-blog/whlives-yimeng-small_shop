<?php /* Smarty version Smarty-3.1.21-dev, created on 2019-08-30 22:07:42
         compiled from "D:\www\whlives-yimeng-master\views\manager\system\express_company\tmp_edit.html" */ ?>
<?php /*%%SmartyHeaderCode:275475d692daebf7972-76040068%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'f518568b4531148fdc0438c79441b02889427bbb' => 
    array (
      0 => 'D:\\www\\whlives-yimeng-master\\views\\manager\\system\\express_company\\tmp_edit.html',
      1 => 1533788760,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '275475d692daebf7972-76040068',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'item' => 0,
    'user_type' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5d692daecc2ba6_19821487',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5d692daecc2ba6_19821487')) {function content_5d692daecc2ba6_19821487($_smarty_tpl) {?><html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo config_item('manager_title');?>
</title>
    <link rel="stylesheet" href="/public/express_templet/css/bootstrap.min.css">
    <?php echo '<script'; ?>
 language="JavaScript" src="/public/express_templet/SystemEvent.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 language="JavaScript" src="/public/express_templet/editorjs/elementProperty.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 language="JavaScript" src="/public/express_templet/editorjs/elementNode.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 language="JavaScript" src="/public/express_templet/editorjs/Canvas.1.0.1.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 language="JavaScript" src="/public/express_templet/editorjs/TemplateEdit.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 language="JavaScript" src="/public/express_templet/editorjs/DataSource.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 language="JavaScript" src="/public/express_templet/editorjs/Tools.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 language="JavaScript" src="/public/express_templet/editorjs/PublicData.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 language="JavaScript" src="/public/js/jquery.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 language="JavaScript" src="/public/express_templet/dropdown.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 language="JavaScript" src="/public/express_templet/tooltip.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 language="javascript" src="/public/express_templet/jquery.jqprint-0.3.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 language="JavaScript" src="/public/express_templet/jquery.PrintArea.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 language="JavaScript" src="/public/express_templet/transition.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 language="JavaScript" src="/public/express_templet/collapse.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 language="JavaScript" src="/public/express_templet/modal.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 language="JavaScript" src="/public/js/layer/layer.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 type="text/javascript" src="/public/js/ajaxfileupload.js"><?php echo '</script'; ?>
>
    <link href="/public/express_templet/css/ruler.css" rel="stylesheet" type="text/css"/>
    <link href="/public/express_templet/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <?php echo '<script'; ?>
 src="/public/express_templet/html5shiv.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="/public/express_templet/respond.min.js"><?php echo '</script'; ?>
>
    <![endif]-->
</head>
<body>
</BODY>
<?php echo '<script'; ?>
 language="JavaScript">
    var template = new TemplateEdit({
        jdatasource: {
            "json": {
                "买家信息": ["姓名", "电话", "收货地址", "送货时间", "城市"],
                "卖家信息": ["联系人", "电话", "联系地址", "城市"],
                "订单明细": ["订单号", "订单价格"]
            }
        },
        <?php if ($_smarty_tpl->tpl_vars['item']->value['config']=='') {?>
        jTemplates: [{
            "title": "<?php echo $_smarty_tpl->tpl_vars['item']->value['name'];?>
",
            "backgroundImage": "url('')",
            "printpc": 10,
            "container": {"left": 160, "top": 57, "right": 1600, "bottom": 557, "width": 1440, "height": 500},
        }]
        <?php } else { ?>
        jTemplates: <?php echo $_smarty_tpl->tpl_vars['item']->value['config'];?>

        <?php }?>
    });
    window.onresize = function () {
        template.ResetPosition();
    }

    var upload_url = '<?php echo config_item('upload_url');?>
';//图片上传地址
    var user_type = '<?php echo $_smarty_tpl->tpl_vars['user_type']->value;?>
';//用户类型
    //保存配置
    function save_config(json_data) {
        $.ajax({
            type:"POST",
            url: '<?php echo site_url("/manager/system/express_company/tmp_save/");?>
',
            data: 'config='+json_data+'&id=<?php echo $_smarty_tpl->tpl_vars['item']->value['id'];?>
',
            dataType:"json",
            success: function(data){
                if (data.status == 'y') {
                    layer.msg('保存成功');
                    setTimeout(function(){
                        window.location.reload();
                    }, 1000)
                } else {
                    layer.msg('保存失败');
                }
            }
        });
    }
<?php echo '</script'; ?>
>
</body>
</html>	<?php }} ?>
