<?php /* Smarty version Smarty-3.1.21-dev, created on 2019-08-13 23:19:07
         compiled from "D:\www\whlives-yimeng-master\views\manager\report\admin_log\list.html" */ ?>
<?php /*%%SmartyHeaderCode:201615d52d4eb0dbd54-78763426%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '39b28d4e88b9bea2cb3f4123299a5ae5f41b470a' => 
    array (
      0 => 'D:\\www\\whlives-yimeng-master\\views\\manager\\report\\admin_log\\list.html',
      1 => 1533788760,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '201615d52d4eb0dbd54-78763426',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'list' => 0,
    'key' => 0,
    'page_count' => 0,
    'search_where' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5d52d4eb193706_76890339',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5d52d4eb193706_76890339')) {function content_5d52d4eb193706_76890339($_smarty_tpl) {?><!DOCTYPE HTML>
<html>
<head>
	<meta charset="utf-8">
	<meta name="renderer" content="webkit|ie-comp|ie-stand">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
	<meta http-equiv="Cache-Control" content="no-siteapp" />
	<title><?php echo config_item('manager_title');?>
</title>
	<link href="/public/H-ui/css/H-ui.min.css" rel="stylesheet" type="text/css" />
	<link href="/public/H-ui/css/H-ui.admin.css" rel="stylesheet" type="text/css" />
	<link href="/public/H-ui/hui-iconfont/iconfont.css" rel="stylesheet" type="text/css">
</head>
<body>
<nav class="breadcrumb">
	<i class="Hui-iconfont">&#xe67f;</i> 首页 <span class="c-gray en">&gt;</span> 操作记录 <span class="c-gray en">&gt;</span> 操作记录 <a class="btn btn-success radius r mr-20" style="line-height:1.6em;margin-top:3px" href="javascript:location.replace(location.href);" title="刷新" ><i class="Hui-iconfont">&#xe68f;</i></a>
</nav>
<div class="pd-20" style="padding-top: 0px;">
	<div class="text-c pt-20">
		<form action="<?php echo site_url('/manager/report/admin_log/');?>
" method="post" class="form form-horizontal" id="search" name="search">
			<input type="text" class="input-text" placeholder="开始时间" name="start_time" onclick="laydate()" readonly style="width: 100px">-
			<input type="text" class="input-text" placeholder="结束时间" name="end_time" onclick="laydate()" readonly style="width: 100px">
			<input type="text" name="admin_user" placeholder="用户名" style="width:150px" class="input-text">
			<button class="btn btn-success" type="submit"><i class="Hui-iconfont">&#xe665;</i> 搜索</button>
		</form>
	</div>
	<div class="cl pd-5 bg-1 bk-gray mt-20"> <span class="l">
		<a href="javascript:;" onclick="data_del(this,'<?php echo site_url('/manager/report/admin_log/delete/');?>
','')" class="btn btn-danger radius"><i class="Hui-iconfont">&#xe60b;</i> 彻底删除</a>
	</span></div>
	<div class="mt-20">
		<table class="table table-border table-bordered table-bg table-hover table-sort">
			<thead>
			<tr class="text-c">
				<th width="40"><input name="" type="checkbox" value=""></th>
				<th width="50">ID</th>
				<th width="100">操作人</th>
				<th>备注</th>
				<th width="150">时间</th>
				<th width="100">操作</th>
			</tr>
			</thead>
			<tbody>
			<?php  $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['key']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['list']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['key']->key => $_smarty_tpl->tpl_vars['key']->value) {
$_smarty_tpl->tpl_vars['key']->_loop = true;
?>
			<tr class="text-c">
				<td><input name="id[]" type="checkbox" value="<?php echo $_smarty_tpl->tpl_vars['key']->value['id'];?>
"></td>
				<td><?php echo $_smarty_tpl->tpl_vars['key']->value['id'];?>
</td>
				<td class="text-l"><?php echo $_smarty_tpl->tpl_vars['key']->value['admin_user'];?>
</td>
				<td class="text-l"><?php echo $_smarty_tpl->tpl_vars['key']->value['note'];?>
</td>
				<td><?php echo $_smarty_tpl->tpl_vars['key']->value['addtime'];?>
</td>
				<td class="f-14 td-manage">
					<a style="text-decoration:none" class="ml-5" onClick="data_del(this,'<?php echo site_url('/manager/report/admin_log/delete/');?>
','<?php echo $_smarty_tpl->tpl_vars['key']->value['id'];?>
')" href="javascript:;" title="删除"><i class="Hui-iconfont">&#xe6e2;</i></a>
				</td>
			</tr>
			<?php } ?>
			</tbody>
		</table>
	</div>
</div>
<!--分页-->
<?php echo page_view('page',$_smarty_tpl->tpl_vars['page_count']->value,search_array_to_link($_smarty_tpl->tpl_vars['search_where']->value));?>

<?php echo '<script'; ?>
 type="text/javascript" src="/public/js/jquery.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="/public/js/layer/layer.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="/public/js/jquery.dataTables.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="/public/js/form.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="/public/H-ui/js/H-ui.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="/public/H-ui/js/H-ui.admin.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="/public/js/laydate/laydate.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 language="JavaScript">
	$(function(){
		//表单回填
		var formObj = new Form();
		formObj.init(<?php echo ch_json_encode($_smarty_tpl->tpl_vars['search_where']->value);?>
);
	})
<?php echo '</script'; ?>
>
</body>
</html><?php }} ?>
