<?php /* Smarty version Smarty-3.1.21-dev, created on 2019-08-13 23:07:58
         compiled from "D:\www\whlives-yimeng-master\views\manager\system\role\add.html" */ ?>
<?php /*%%SmartyHeaderCode:218975d52d24e42e695-00432091%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'be3b8b842362ded59a97c68910e3fc33798b1ea1' => 
    array (
      0 => 'D:\\www\\whlives-yimeng-master\\views\\manager\\system\\role\\add.html',
      1 => 1533788760,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '218975d52d24e42e695-00432091',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'right_arr' => 0,
    'val' => 0,
    'key' => 0,
    'k' => 0,
    'item' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5d52d24e510fc6_31574245',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5d52d24e510fc6_31574245')) {function content_5d52d24e510fc6_31574245($_smarty_tpl) {?><!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<meta name="renderer" content="webkit|ie-comp|ie-stand">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
<meta http-equiv="Cache-Control" content="no-siteapp" />
    <title><?php echo config_item('manager_title');?>
</title>
    <link href="/public/H-ui/css/H-ui.min.css" rel="stylesheet" type="text/css" />
    <link href="/public/H-ui/css/H-ui.admin.css" rel="stylesheet" type="text/css" />
    <link href="/public/H-ui/hui-iconfont/iconfont.css" rel="stylesheet" type="text/css">
</head>
<body>
<div class="pd-20">
	<form action="<?php echo site_url('/manager/system/role/save');?>
" method="post" class="form form-horizontal" id="add">
		<div class="row cl">
			<label class="form-label col-2"><span class="c-red">*</span>角色名称：</label>
			<div class="formControls col-5">
				<input type="text" class="input-text" value="" name="name" datatype="*" nullmsg="请输入角色名称！">
			</div>
		</div>
        <div class="row cl">
            <label class="form-label col-2">角色权限：</label>
            <div class="formControls col-10">
                <?php  $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['key']->_loop = false;
 $_smarty_tpl->tpl_vars['val'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['right_arr']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['key']->key => $_smarty_tpl->tpl_vars['key']->value) {
$_smarty_tpl->tpl_vars['key']->_loop = true;
 $_smarty_tpl->tpl_vars['val']->value = $_smarty_tpl->tpl_vars['key']->key;
?>
                <dl class="permission-list">
                    <dt>
                        <label>
                            <input type="checkbox" id='checkbox_<?php echo $_smarty_tpl->tpl_vars['val']->value;?>
' onclick='checkGroupAll(this,"<?php echo $_smarty_tpl->tpl_vars['val']->value;?>
");'>&nbsp;<?php echo $_smarty_tpl->tpl_vars['val']->value;?>

                        </label>
                    </dt>
                    <dd id='ul_<?php echo $_smarty_tpl->tpl_vars['val']->value;?>
' class='attr_list' alt="<?php echo $_smarty_tpl->tpl_vars['val']->value;?>
">
                        <dl class="cl permission-list2">
                            <dd>
                                <?php  $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['k']->_loop = false;
 $_smarty_tpl->tpl_vars['v'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['key']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['k']->key => $_smarty_tpl->tpl_vars['k']->value) {
$_smarty_tpl->tpl_vars['k']->_loop = true;
 $_smarty_tpl->tpl_vars['v']->value = $_smarty_tpl->tpl_vars['k']->key;
?>
                                <label>
                                    <input type="checkbox" value="<?php echo $_smarty_tpl->tpl_vars['k']->value['id'];?>
" name='right[]' <?php if (strpos($_smarty_tpl->tpl_vars['item']->value['rights'],$_smarty_tpl->tpl_vars['k']->value['right'])!==false) {?>checked=checked<?php }?> onclick='checkItem("<?php echo $_smarty_tpl->tpl_vars['val']->value;?>
");' >&nbsp;<?php echo $_smarty_tpl->tpl_vars['k']->value['name'];?>

                                </label>
                                <?php } ?>
                            </dd>
                        </dl>
                     </dd>
                </dl>
                <?php } ?>
            </div>
        </div>
		<div class="row cl">
			<div class="col-10 col-offset-2">
                <input type="hidden" name="id" value="">
				<button onClick="$('#add').submit();" class="btn btn-primary radius" type="submit"><i class="Hui-iconfont">&#xe632;</i> 保存</button>
				<button onClick="layer_close();" class="btn btn-default radius" type="button">&nbsp;&nbsp;取消&nbsp;&nbsp;</button>
			</div>
		</div>
	</form>
</div>
</div>
<?php echo '<script'; ?>
 type="text/javascript" src="/public/js/jquery.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="/public/js/validform.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="/public/js/layer/layer.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="/public/js/form.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="/public/js/artTemplate.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="/public/H-ui/js/H-ui.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="/public/H-ui/js/H-ui.admin.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript">
    $(function(){
        //验证表单
        $.Tipmsg.r=null;
        $("#add").Validform({
            tiptype:function(msg){
                layer.msg(msg);
            },
            tipSweep:true,
            ajaxPost:true,
            callback:function(data){
                if(data.status=="y"){
                    layer_close();
                }
            }
        });
    })

    var formObj = new Form();
    formObj.init(<?php echo ch_json_encode($_smarty_tpl->tpl_vars['item']->value);?>
);

    //分组权限全选
    function checkGroupAll(obj,nameVal)
    {
        if(obj.checked == true)
        {
            $('#ul_'+nameVal+' [name="right[]"]').prop('checked',true);
        }
        else
        {
            $('#ul_'+nameVal+' [name="right[]"]').prop('checked',false);
        }
    }

    //选择权限
    function checkItem(nameVal)
    {
        var totalNum   = $('#ul_'+nameVal+' [name="right[]"]').length;
        var checkedNum = $('#ul_'+nameVal+' [name="right[]"]:checked').length;

        if(checkedNum >= totalNum)
        {
            $('#checkbox_'+nameVal).prop('checked',true);
        }
        else
        {
            $('#checkbox_'+nameVal).prop('checked',false);
        }
    }

    //预选择复选框
    jQuery(function(){
        $('dd.attr_list[alt]').each(
            function(i)
            {
                checkItem($(this).attr('alt'));
            }
        );
    });
<?php echo '</script'; ?>
>
</body>
</html><?php }} ?>
