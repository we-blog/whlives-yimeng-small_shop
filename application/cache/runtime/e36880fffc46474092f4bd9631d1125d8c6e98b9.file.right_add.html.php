<?php /* Smarty version Smarty-3.1.21-dev, created on 2019-08-13 23:09:08
         compiled from "D:\www\whlives-yimeng-master\views\manager\system\role\right_add.html" */ ?>
<?php /*%%SmartyHeaderCode:55515d52d294791a94-12881976%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'e36880fffc46474092f4bd9631d1125d8c6e98b9' => 
    array (
      0 => 'D:\\www\\whlives-yimeng-master\\views\\manager\\system\\role\\right_add.html',
      1 => 1533788760,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '55515d52d294791a94-12881976',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'file_list' => 0,
    'key' => 0,
    'type' => 0,
    'item' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5d52d29483d8c3_50478383',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5d52d29483d8c3_50478383')) {function content_5d52d29483d8c3_50478383($_smarty_tpl) {?><!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<meta name="renderer" content="webkit|ie-comp|ie-stand">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
<meta http-equiv="Cache-Control" content="no-siteapp" />
    <title><?php echo config_item('manager_title');?>
</title>
    <link href="/public/H-ui/css/H-ui.min.css" rel="stylesheet" type="text/css" />
    <link href="/public/H-ui/css/H-ui.admin.css" rel="stylesheet" type="text/css" />
    <link href="/public/H-ui/hui-iconfont/iconfont.css" rel="stylesheet" type="text/css">
</head>
<body>
<div class="pd-20">
	<form action="<?php echo site_url('/manager/system/role_right/save');?>
" method="post" class="form form-horizontal" id="add">
		<div class="row cl">
			<label class="form-label col-2"><span class="c-red">*</span>权限名称：</label>
			<div class="formControls col-5">
				<input type="text" class="input-text" value="" name="name" datatype="*" nullmsg="请输入权限名称！">
			</div>
		</div>
        <div class="row cl">
            <label class="form-label col-2"><span class="c-red">*</span>权限码：</label>
            <div class="formControls col-7">
                <table class="table table-border table-bordered table-bg table-hover table-sort">
                    <thead>
                    <tr class="text-c">
                        <th>权限码</th>
                        <th width="50">操作</th>
                    </tr>
                    </thead>
                    <tbody id="right_list">
                    <!--权限列表-->
                    </tbody>
                    <!--权限模板-->
                    <?php echo '<script'; ?>
 type='text/html' id='rightTemplate'>
                        {{each}}
                        <tr class="text-c">
                            <td>
                                <input type="text" class="input-text" value="{{$value}}" name="right[]" datatype="*" nullmsg="权限码不能为空！">
                            </td>
                            <td class="f-14">
                                <a onclick="$(this).parent().parent().remove();"><i class="Hui-iconfont">&#xe609;</i></a>
                            </td>
                        </tr>
                        {{/each}}
                    <?php echo '</script'; ?>
>
                </table>
            </div>
        </div>
        <div class="row cl">
            <label class="form-label col-2"><span class="c-red">*</span>添加权限码：</label>
            <div class="formControls col-9">
                <span class="select-box" style="width: 230px;">
				<select name="file_name" class="select">
                    <option value="">请选择</option>
                    <?php  $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['key']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['file_list']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['key']->key => $_smarty_tpl->tpl_vars['key']->value) {
$_smarty_tpl->tpl_vars['key']->_loop = true;
?>
                    <option value="<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['key']->value;?>
</option>
                    <?php } ?>
                </select>
				</span>
                <span class="select-box" style="width: 220px;">
				<select name="action_name" class="select">
                </select>
				</span>
                <button class="btn btn-primary radius" type="button" onclick="add_right();"><i class="Hui-iconfont">&#xe600;</i> 添加</button>
            </div>
        </div>
		<div class="row cl">
			<div class="col-10 col-offset-2">
                <input type="hidden" name="id" value="">
                <input type="hidden" name="type" value="<?php echo $_smarty_tpl->tpl_vars['type']->value;?>
">
				<button onClick="$('#add').submit();" class="btn btn-primary radius" type="submit"><i class="Hui-iconfont">&#xe632;</i> 保存</button>
				<button onClick="layer_close();" class="btn btn-default radius" type="button">&nbsp;&nbsp;取消&nbsp;&nbsp;</button>
			</div>
		</div>
	</form>
</div>
</div>
<?php echo '<script'; ?>
 type="text/javascript" src="/public/js/jquery.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="/public/js/validform.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="/public/js/layer/layer.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="/public/js/form.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="/public/js/artTemplate.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="/public/H-ui/js/H-ui.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="/public/H-ui/js/H-ui.admin.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript">
    $(function(){
        //验证表单
        $.Tipmsg.r=null;
        $("#add").Validform({
            tiptype:function(msg){
                layer.msg(msg);
            },
            tipSweep:true,
            ajaxPost:true,
            callback:function(data){
                if(data.status=="y"){
                    layer_close();
                }
            }
        });

        //根据文件列出action
        $('[name="file_name"]').change(function(){
            $.ajax({
                type:"POST",
                url: "<?php echo site_url('/manager/system/role_right/list_action');?>
",
                data:"file_name="+$(this).val()+"&type=<?php echo $_smarty_tpl->tpl_vars['type']->value;?>
",
                dataType:"json",
                success: function(data){
                    var option_html = '';
                    for(action in data)
                    {
                        option_html = option_html+'<option value="'+data[action]+'">'+data[action]+'</option>';
                    }
                    $('[name="action_name"]').html(option_html);
                }
            });
        });

        //表单回填
        var formObj = new Form();
        formObj.init(<?php echo ch_json_encode($_smarty_tpl->tpl_vars['item']->value);?>
);

        <?php if ($_smarty_tpl->tpl_vars['item']->value['right']!='') {?>
            var right_data = <?php echo ch_json_encode($_smarty_tpl->tpl_vars['item']->value['right']);?>
;
            add_right_tmp(right_data);
        <?php }?>
    })

    //权限模板解析
    function add_right_tmp(data){
        var data = arguments[0] ? arguments[0] :'';
        if(data=='')
        {
            data = [];
        }
        var html = template('rightTemplate', data);
        $('#right_list').append(html);
    }

    //添加权限
    function add_right()
    {
        var html = $('[name="file_name"]').val()+'/'+$('[name="action_name"]').val();
        //var data = {"right_data":[html]};
        add_right_tmp([html]);
    }
<?php echo '</script'; ?>
>
</body>
</html><?php }} ?>
