<?php
include_once 'common.php';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>安装程序 -smallshop</title>
    <link href="css/style.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" src="/public/js/jquery.js"></script>
</head>
<body>

<div class="top">
    <div class="top-logo">
        <img src="images/top-logo.png" height="70">
    </div>
    <div class="top-link">
        <ul>
            <li><a href="http://www.smallshop.com" target="_blank">官方网站</a></li>
            <li><a href="#" target="_blank">QQ群:322257814</a></li>
        </ul>
    </div>
    <div class="top-version">
        <!-- 版本信息 -->
        <h2>smallshop</h2>
    </div>
</div>

<div class="main">
    <div class="pleft">
        <dl class="setpbox t1">
            <dt>安装步骤</dt>
            <dd>
                <ul>
                    <li class="succeed">许可协议</li>
                    <li class="succeed">环境检测</li>
                    <li class="now">参数配置</li>
                    <li>正在安装</li>
                    <li>安装完成</li>
                </ul>
            </dd>
        </dl>
    </div>
    <div class="pright">

        <!--参数配置-->
        <div class="index_mian_right_ly">
            <form id="mysql" action="ajax.php" enctype="multipart/form-data" method="post">
                <!--数据库设定-->
                <div class="index_mian_right_two_ly">
                    <div class="index_mian_right_two_one_ly"><span>数据库设定</span></div>
                    <div class="index_mian_right_two_two_ly">
                        <div class="index_mian_right_two_two_o_ly"><b>数据库主机：</b>
                            <input class="index_mian_right_two_two_text_ly" name="mysql_host" type="text" value="localhost" datatype="*" errormsg="数据库主机不能为空"/>
                            <span>一般为localhost</span>
                        </div>
                        <div class="index_mian_right_two_two_o_ly"><b>数据库用户：</b>
                            <input class="index_mian_right_two_two_text_ly" name="mysql_user" type="text" datatype="*" errormsg="数据库用户不能为空"/>
                        </div>
                        <div class="index_mian_right_two_two_o_ly"><b>数据库密码：</b>
                            <input class="index_mian_right_two_two_text_ly" name="mysql_password" type="text" datatype="*" errormsg="数据库密码不能为空"/>
                        </div>
                        <div class="index_mian_right_two_two_o_ly"><b>数据表前缀：</b>
                            <input class="index_mian_right_two_two_text_ly" name="mysql_pre" type="text" value="my_" datatype="*" errormsg="数据表前缀不能为空"/>
                            <span>如无特殊需要，请不要修改</span>
                        </div>
                        <div class="index_mian_right_two_two_o_ly"><b>数据库名称：</b>
                            <input class="index_mian_right_two_two_text_ly" name="mysql_name" type="text" value="small_shop" datatype="*" errormsg="数据库名称不能为空"/>
                        </div>
                        <div class="index_mian_right_two_two_o_ly">
                            <b>如需安装测试数据请点击这里下载数据包(注:需要安装完成了才能导入测试数据)</b>
                        </div>
                    </div>
                </div>
                <!--数据库设定结束-->

                <!--管理员初始密码-->
                <div class="index_mian_right_three_ly">
                    <div class="index_mian_right_three_one_ly"><span>管理员初始密码</span></div>
                    <div class="index_mian_right_three_two_ly">

                        <div class="index_mian_right_three_two_o_ly"><b>用户名：</b>
                            <input class="index_mian_right_two_two_text_ly" name="admin_user" type="text" datatype="*" errormsg="管理员用户名不能为空"/>
                        </div>
                        <div class="index_mian_right_three_two_n_ly"><b>密码：</b>
                            <input class="index_mian_right_two_two_text_ly" name="admin_password" type="text" datatype="*" errormsg="管理员密码不能为空"/>
                        </div>
                        <div class="index_mian_right_three_two_e_ly"><b>重复密码：</b>
                            <input class="index_mian_right_two_two_text_ly" name="admin_passwords" type="text" datatype="*" recheck="admin_password" errormsg="两次密码不一样"/>
                        </div>
                    </div>
                </div>
                <!--管理员初始密码结束-->
                <!--线-->
                <div class="index_mian_right_six_ly"></div>
                <div class="progress">
                    <div></div>
                </div>
                <!--后退,继续-->
                <div class="index_mian_right_seven_ly">
                    <a href="JavaScript:void(0)" onclick="$('#mysql').submit();">
                        <input name="" class="index_mian_right_seven_Forward_ly" type="button" value="继续"/>
                    </a>
                    <a href="javascript:history.back();">
                        <input name="" class="index_mian_right_seven_Forward_ly" type="button" value="后退"/>
                    </a>
                </div>
            </form>
            <!--后退,继续结束-->
        </div>
    </div>
</div>
<div class="foot">
</div>
<script language="JavaScript" src="/public/js/layer/layer.js"></script>
<script language="JavaScript" src="/public/js/validform.js"></script>
<script language="JavaScript">
    $(function () {
        //验证表单
        $.Tipmsg.r = null;
        $("#mysql").Validform({
            tiptype: function (msg) {
                if (msg == '正在提交数据…') {
                    layer.msg('数据安装中,请稍后...', {
                        time: 200000
                    });
                } else {
                    layer.msg(msg);
                }
            },
            tipSweep: true,
            ajaxPost: true,
            callback: function (data) {
                if (data.status == "y") {
                    layer.msg('创建完成');
                    setTimeout(function(){
                        window.location.href='install4.php';
                    }, 1000)
                } else {
                    layer.msg(data.info);
                }
            }
        });
    })
</script>
</body>
</html>
