<?php
include_once 'common.php';
$file_path     = dirname(__FILE__);
$server_config = array(
    'phpversion'          => array(0, '5.3'),
    'safe_mode'           => array(0, ''),
    'memory_limit'        => array(0, '>=128M'),
    'upload_max_filesize' => array(0, '8'),
    'mysqli'              => array(0, ''),
    'gd'                  => array(0, ''),
    'session'             => array(0, ''),
    'unlink'              => array(0, ''),
    'mkdir'               => array(0, ''),
    'filemtime'           => array(0, ''),
    'fopen'               => array(0, ''),
    'fwrite'              => array(0, ''),
    'fclose'              => array(0, ''),
    'session_start'       => array(0, ''),
    'zip'                 => array(0, ''),
    'curl'                => array(0, ''),
);

//环境配置
if (version_compare(phpversion(), '5.3') > 0) {
    $server_config['phpversion'][0] = 1;
    $server_config['phpversion'][1] = phpversion();
}
if (@ini_get('safe_mode') === false) $server_config['safe_mode'][0] = 1;
if (floatval(@ini_get('memory_limit')) >= 128) {
    $server_config['memory_limit'][0] = 1;
    $server_config['memory_limit'][1] = @ini_get('memory_limit');
}
if (floatval(@ini_get('upload_max_filesize')) >= 8) {
    $server_config['upload_max_filesize'][0] = 1;
    $server_config['upload_max_filesize'][1] = @ini_get('upload_max_filesize');
}
//扩展配置
$server_config['mysqli'][0]  = extension_loaded('mysqli');
$server_config['gd'][0]      = extension_loaded('gd');
$server_config['session'][0] = extension_loaded('session');
$server_config['zip'][0]     = extension_loaded('zip');
$server_config['curl'][0]    = extension_loaded('curl');
//启用函数
$server_config['unlink'][0]        = function_exists('unlink');
$server_config['mkdir'][0]         = function_exists('mkdir');
$server_config['filemtime'][0]     = function_exists('filemtime');
$server_config['fopen'][0]         = function_exists('fopen');
$server_config['fwrite'][0]        = function_exists('fwrite');
$server_config['fclose'][0]        = function_exists('fclose');
$server_config['session_start'][0] = function_exists('session_start');

//判断目录是否可写
$write_dir = array(
    '/application/cache/'         => $file_path . '/../application/cache/',
    '/application/cache/cache/'   => $file_path . '/../application/cache/cache/',
    '/application/cache/runtime/' => $file_path . '/../application/cache/runtime/',
    '/application/cache/session/' => $file_path . '/../application/cache/session/',
    '/application/cache/thumb/'   => $file_path . '/../application/cache/thumb/',
    '/uploads/'                   => $file_path . '/../uploads/',
);
foreach ($write_dir as $v => $k) {
    $k             = write_dir($k);
    $write_dir[$v] = $k;
}

//判断文件是否可写
$write_file = array(
    '/application/config/database.php' => $file_path . '/../application/config/database.php',
);
foreach ($write_file as $v => $k) {
    $k              = is_writeable($k);
    $write_file[$v] = $k;
}

$write_list = array_merge($write_dir, $write_file);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>安装程序 -smallshop</title>
    <link href="css/style.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" src="/public/js/jquery.js"></script>
</head>

<body>

<div class="top">
    <div class="top-logo">
        <img src="images/top-logo.png" height="70">
    </div>
    <div class="top-link">
        <ul>
            <li><a href="http://www.smallshop.com" target="_blank">官方网站</a></li>
            <li><a href="#" target="_blank">QQ群:322257814</a></li>
        </ul>
    </div>
    <div class="top-version">
        <!-- 版本信息 -->
        <h2>smallshop</h2>
    </div>
</div>

<div class="main">
    <div class="pleft">
        <dl class="setpbox t1">
            <dt>安装步骤</dt>
            <dd>
                <ul>
                    <li class="succeed">许可协议</li>
                    <li class="now">环境检测</li>
                    <li>参数配置</li>
                    <li>正在安装</li>
                    <li>安装完成</li>
                </ul>
            </dd>
        </dl>
    </div>

    <div class="pright">
        <div class="enter_lf">
            <div class="Envin_lf">
                <div class="menter_lf"><span>系统环境检测</span></div>
                <div class="menter_table_lf">
                    <table width="1000" border="0" cellspacing="0" cellpadding="0" class="tabletable">
                        <thead>
                        <tr>
                            <th align="left"><span style="margin-left: 20px;">需要开启的环境和函数</span></th>
                            <th>是否满足</th>
                            <th align="left"><span style="margin-left: 20px;">需要开启的环境和函数</span></th>
                            <th>是否满足</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <?php
                            $i = 0;
                            foreach ($server_config as $val => $key) {
                                if (($i + 1) % 2 == 1 && $i > 1) {
                                    echo "</tr><tr>";
                                }
                                $value = $status = '';
                                if (!empty($key[1])) {
                                    $value .= '(';
                                    if ($key[0] == 1) {
                                        $value .= "当前:";
                                    } else {
                                        $value .= "建议:";
                                    }
                                    $value .= $key[1] . ')';
                                }
                                if ($key[0] == 1) {
                                    $status = '<span style="color: #ff5f1e">OK</span>';
                                } else {
                                    $status = 'NO';
                                }
                                echo <<<Eof
                                    <td align="left">
                                        <span style="margin-left: 20px;">
                                            $val $value
                                        </span>
                                    </td>
                                    <td>
                                       $status
                                    </td>
Eof;
                                $i++;
                            }
                            ?>
                        </tr>
                        </tbody>
                    </table>
                    <div style="clear: both;"></div>
                </div>
            </div>
            <div class="Envin_lf">
                <div class="menter_lf"><span>目录文件权限检测</span></div>
                <div class="menter_table_lf">
                    <table width="1000" border="0" cellspacing="0" cellpadding="0" class="tabletable">
                        <thead>
                        <tr>
                            <th align="left"><span style="margin-left: 20px;">目录名/文件名</span></th>
                            <th>写入权限</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        foreach ($write_list as $val => $key) {
                            $status = '';
                            if ($key == 1) {
                                $status = '<span style="color: #ff5f1e">OK</span>';
                            } else {
                                $status = 'NO';
                            }
                            echo <<<Eof
                                <tr>
                                    <td align="left"><span style="margin-left: 20px;">$val</span></td>
                                    <td>$status</td>
                                </tr>
Eof;
                        }
                        ?>
                        </tbody>
                    </table>

                </div>
            </div>
            <div class="menter_btn_lf"></div>
            <div class="menter_btn_a_lf">
                <a href="install3.php"><input name="" type="button" class="menter_btn_a_a_lf" value="继续"></a>
                <a href="javascript:history.back();"><input name="" type="button" class="menter_btn_a_a_lf" value="后退"></a>

            </div>
        </div>
    </div>
</body>
</html>
